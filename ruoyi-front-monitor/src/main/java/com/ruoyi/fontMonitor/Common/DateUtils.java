package com.ruoyi.fontMonitor.Common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 */
public class DateUtils {

    /**
     * 计算两个日期之间相差的天数
     *
     * @param smallDate 较小的日期
     * @param bigDate   较大的日期
     * @return 相差的天数
     */
    public static Long daysBetween(Date smallDate, Date bigDate) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(smallDate);
        Long smallTime = calendar.getTimeInMillis();

        calendar.setTime(bigDate);
        Long bigTime = calendar.getTimeInMillis();

        return (bigTime - smallTime) / (1000 * 3600 * 24);
    }

    /**
     * 时间Date转String
     *
     * @param date String格式的时间
     * @return Date格式的时间
     */
    public static Date stringToDate(String date) throws ParseException {
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.parse(date);
    }

    /**
     * 时间String转Date
     *
     * @param date Date格式的时间
     * @return String格式的时间
     */
    public static String dateToString(Date date) {
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(date);
    }
}
