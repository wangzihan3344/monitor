package com.ruoyi.fontMonitor.Common;

import com.alibaba.fastjson2.JSONObject;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.core.io.ClassPathResource;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

public class IPAddressSearchUtils {
    /**
     * description 地址库查询
     *
     * @param ip 地址
     * @return java.lang.String
     * @version 1.0
     */
    public static JSONObject getRealAddress(String ip) {
        JSONObject result =  new JSONObject();
        try {
            // 加载地址库
            ClassPathResource resource = new ClassPathResource("GeoLite2-City.mmdb");
            DatabaseReader reader = new DatabaseReader.Builder(resource.getInputStream()).build();
            InetAddress inetAddress = InetAddress.getByName(ip);
            CityResponse response = reader.city(inetAddress);
            // 获取所在国家
            String country = response.getCountry().getNames().get("ja");
            // 库内获取不到的IP，访问ali的地域查询
            if (response.getSubdivisions().size() > 0) {
                // 获取所在省份
                String province = response.getSubdivisions().get(0).getNames().get("zh-CN");
                // 获取所在城市
                String city = response.getCity().getNames().get("zh-CN");
                city = city == null ? "" : city;
                result.put("country", country);
                result.put("province", province);
                result.put("city", city);
            } else {
                result = getAlibaba(ip);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * description ali地域查询
     * <p>
     * 错误返回格式
     * {
     * "data": null,
     * "msg": "The error occurred while execute outGetIpInfo,parameter =541.94.1.111",
     * "code": 2
     * }
     * <p>
     * 正确返回格式
     * {
     * "data": {
     * "area": "",
     * "country": "中国",
     * "isp_id": "100017",
     * "queryIp": "60.177.107.164",
     * "city": "杭州",
     * "ip": "60.177.107.164",
     * "isp": "电信",
     * "county": "",
     * "region_id": "330000",
     * "area_id": "",
     * "county_id": null,
     * "region": "浙江",
     * "country_id": "CN",
     * "city_id": "330100"
     * },
     * "msg": "query success",
     * "code": 0
     * }
     *
     * @param ip ip地址
     * @return java.lang.String
     * @version 1.0
     */
    public static JSONObject getAlibaba(String ip) {
        JSONObject result = new JSONObject();

        String host = "http://ip.taobao.com";
        String path = "/outGetIpInfo";
        String method = "POST";
        String charset = "UTF-8";

        Map<String, String> headers = new HashMap<String, String>();

        Map<String, String> querys = new HashMap<String, String>();

        Map map = new HashMap();
        map.put("ip", ip);
        map.put("accessKey", "alibaba-inc");

        try {
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, map);
            // 请求成功，解析响应数据
            String str = EntityUtils.toString(response.getEntity()).toString();
            Map valueMap = JSONObject.parseObject(str, Map.class);
            if (valueMap.get("data") != null) {
                Map<String, String> dataMap = (Map<String, String>) valueMap.get("data");
                String country = dataMap.get("country");
                String region = dataMap.get("region");
                String city = dataMap.get("city");
                result.put("country", country);
                if (country.equals("中国"))
                    result.put("province", region + "省");
                else
                    result.put("province", region);
                result.put("city", city);
                return result;
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    public static void main(String[] args) {
        System.out.println(getRealAddress("60.177.107.164"));
    }

}
