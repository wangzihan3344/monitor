package com.ruoyi.fontMonitor.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.fontMonitor.service.IEnhanceErrorService;
import com.ruoyi.fontMonitor.service.IEnhanceMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@SuppressWarnings("all")
@RequestMapping("/enhance/error")
public class EnhanceErrorController {
    @Autowired
    private IEnhanceErrorService errorService;

    @Autowired
    private IEnhanceMainService enhanceMainService;

    /**
     * 获取Js和Promise错误数量图表
     */
    @GetMapping("/numberOfJsAndPromiseError")
    public AjaxResult numberOfJsAndPromiseError(@RequestParam(value = "projectId", required = false) String projectId,
                                                @RequestParam(value = "timestamp", required = false) String reportTimestamp,
                                                @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(errorService.numberOfJsAndPromiseError(projectId, reportTimestamp, env));
    }

    /**
     * 获取js/promise/custom错误列表
     */
    @GetMapping("/getJsAndPromiseAndCustomErrorByDesignation")
    public AjaxResult getJsAndPromiseAndCustomErrorByDesignation(@RequestParam(value = "projectId", required = false) String projectId,
                                                                 @RequestParam(value = "env", required = false) String env,
                                                                 @RequestParam(value = "timestamp", required = false) String timestamp,
                                                                 @RequestParam(value = "timeType", required = false) String timeType,
                                                                 @RequestParam(value = "range", required = false) String range,
                                                                 @RequestParam(value = "type", required = false) String type) {
        return AjaxResult.success(errorService.getJsAndPromiseAndCustomError(projectId, env, timestamp, timeType, range, type));
    }

    /**
     * 获取api错误数量图表
     */
    @GetMapping("/numberOfApiError")
    public AjaxResult numberOfApiError(@RequestParam(value = "projectId", required = false) String projectId,
                                       @RequestParam(value = "timestamp", required = false) String reportTimestamp,
                                       @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(errorService.numberOfApiError(projectId, reportTimestamp, env));
    }

    /**
     * 获取api错误列表(只有主表数据)
     */
    @GetMapping("/searchApiErrorDataByDesignation")
    public AjaxResult searchApiErrorDataByDesignation(@RequestParam(value = "projectId", required = false) String projectId,
                                                      @RequestParam(value = "env", required = false) String env,
                                                      @RequestParam(value = "timestamp", required = false) String timestamp,
                                                      @RequestParam(value = "timeType", required = false) String timeType,
                                                      @RequestParam(value = "range", required = false) String range) {
        return AjaxResult.success(errorService.searchApiErrorDataByDesignation(projectId, env, timestamp, timeType, range));
    }

    /**
     * 获取api错误列表(包含子表数据)
     */
    @GetMapping("/getApiErrorByDesignation")
    public AjaxResult getApiErrorByDesignation(@RequestParam(value = "projectId", required = false) String projectId,
                                               @RequestParam(value = "env", required = false) String env,
                                               @RequestParam(value = "timestamp", required = false) String timestamp,
                                               @RequestParam(value = "timeType", required = false) String timeType,
                                               @RequestParam(value = "range", required = false) String range) {
        return AjaxResult.success(errorService.getApiErrorByDesignation(projectId, env, timestamp, timeType, range));
    }

    /**
     * 获取resource错误数量图表
     */
    @GetMapping("/numberOfResourceError")
    public AjaxResult numberOfResourceError(@RequestParam(value = "projectId", required = false) String projectId,
                                            @RequestParam(value = "timestamp", required = false) String reportTimestamp,
                                            @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(errorService.numberOfResourceError(projectId, reportTimestamp, env));
    }

    /**
     * 获取Resource错误列表(包含子表数据)
     */
    @GetMapping("/getResourceErrorByDesignation")
    public AjaxResult getResourceErrorByDesignation(@RequestParam(value = "projectId", required = false) String projectId,
                                                    @RequestParam(value = "env", required = false) String env,
                                                    @RequestParam(value = "timestamp", required = false) String timestamp,
                                                    @RequestParam(value = "timeType", required = false) String timeType,
                                                    @RequestParam(value = "range", required = false) String range) {
        return AjaxResult.success(errorService.getResourceErrorByDesignation(projectId, env, timestamp, timeType, range));
    }

    /**
     * 获取指定时间静态资源概况数据：总静态资源错误数、影响的页面数、影响的用户数
     */
    @GetMapping("/numberOfResourceErrorTodayData")
    public AjaxResult numberOfResourceErrorTodayData(@RequestParam(value = "projectId", required = false) String projectId,
                                                     @RequestParam(value = "timestamp", required = false) String reportTimestamp,
                                                     @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(enhanceMainService.listMainByTimeAndEnvANdGroupByUuid(projectId, reportTimestamp, env, String.valueOf(2)));
    }

    /**
     * 获取当天项目健康评分、js报错量、promise异常量、自定义错误量、静态资源异常率、接口异常率
     */
    @GetMapping("/getProjectScore")
    public AjaxResult getProjectScore(@RequestParam(value = "projectId", required = false) String projectId,
                                      @RequestParam(value = "env", required = false) String env,
                                      @RequestParam(value = "timestamp", required = false) String reportTimestamp) {
        return AjaxResult.success(errorService.getProjectScore(projectId, env, reportTimestamp));
    }

    /**
     * 获取指定时间24小时及一周前同一天24小时每小时内报错数量图表数据
     */
    @GetMapping("/getTwentyForeHourError")
    public AjaxResult getTwentyForeHourError(@RequestParam(value = "projectId", required = false) String projectId,
                                             @RequestParam(value = "env", required = false) String env,
                                             @RequestParam(value = "errorType", required = false) String errorType,
                                             @RequestParam(value = "timestamp", required = false) String reportTimestamp) {
        return AjaxResult.success(errorService.getTwentyFourHourError(projectId, env, errorType, reportTimestamp));
    }
}
