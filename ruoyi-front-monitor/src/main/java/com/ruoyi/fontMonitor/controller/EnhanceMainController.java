package com.ruoyi.fontMonitor.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.fontMonitor.domain.FontMonitorMain;
import com.ruoyi.fontMonitor.service.IEnhanceMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@SuppressWarnings("all")
@RequestMapping("/enhance/main")
public class EnhanceMainController extends BaseController {
    @Autowired
    private IEnhanceMainService enhanceMainService;

    /**
     * 获取指定时间范围内产生的数据列表
     */
    @GetMapping("/searchDataByDesignation")
    public AjaxResult searchDataByDesignation(@RequestParam(value = "projectId", required = false) String projectId,
                                              @RequestParam(value = "type", required = false) String type,
                                              @RequestParam(value = "env", required = false) String env,
                                              @RequestParam(value = "timestamp", required = false) String timestamp,
                                              @RequestParam(value = "timeType", required = false) String timeType,
                                              @RequestParam(value = "range", required = false) String range) {
        return AjaxResult.success(enhanceMainService.searchDataByDesignation(projectId, type, env, timestamp, timeType, range));
    }
}
