package com.ruoyi.fontMonitor.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.fontMonitor.service.IEnhancePerformanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("/enhance/performance")
public class EnhancePerformanceController {
    @Autowired
    private IEnhancePerformanceService enhancePerformanceService;

    /**
     * 获取传入时间到当天的api数量（根据耗时分段归类）和占据百分比
     */
    @GetMapping("/getApiNumberAndPercentage")
    public AjaxResult getApiNumberAndPercentage(@RequestParam(value = "projectId", required = false) String projectId,
                                                @RequestParam(value = "sub", required = false) Integer sub,
                                                @RequestParam(value = "timestamp", required = false) String timestamp,
                                                @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(enhancePerformanceService.getApiNumberAndPercentage(projectId, sub, timestamp, env));
    }

    /**
     * 根据传入的时间分割条件获取指定日期指定耗时分段内的接口请求量
     */
    @GetMapping("/getThirtyDaysApiNumber")
    public AjaxResult getThirtyDaysApiNumber(@RequestParam(value = "projectId", required = false) String projectId,
                                             @RequestParam(value = "sub", required = false) Integer sub,
                                             @RequestParam(value = "timestamp", required = false) String timestamp,
                                             @RequestParam(value = "env", required = false) String env,
                                             @RequestParam(value = "timeType", required = false) String timeType,
                                             @RequestParam(value = "range", required = false) Integer range) {
        return AjaxResult.success(enhancePerformanceService.getThirtyDaysApiNumber(projectId, sub, timestamp, env, timeType, range));
    }

    /**
     * 获取指定日期指定耗时分段1小时内的接口信息平均网络耗时、影响用户数、发生页面数、发生页面列表
     */
    @GetMapping("/getApiMessage")
    public AjaxResult getApiMessage(@RequestParam(value = "projectId", required = false) String projectId,
                                    @RequestParam(value = "sub", required = false) Integer sub,
                                    @RequestParam(value = "timestamp", required = false) String timestamp,
                                    @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(enhancePerformanceService.getApiMessage(projectId, sub, timestamp, env));
    }

    @GetMapping("/getApiInfo")
    public AjaxResult getApiInfo(@RequestParam(value = "address", required = false) String address,
                                    @RequestParam(value = "projectId", required = false) String projectId,
                                    @RequestParam(value = "sub", required = false) Integer sub,
                                    @RequestParam(value = "timestamp", required = false) String timestamp,
                                    @RequestParam(value = "env", required = false) String env) throws UnsupportedEncodingException {
        return AjaxResult.success(enhancePerformanceService.getApiInfo(address, projectId, sub, timestamp, env));
    }

    /**
     * 获取传入时间到前一段时间的page数量（根据耗时分段归类）和占据百分比
     */
    @GetMapping("/getPageNumberAndPercentage")
    public AjaxResult getPageNumberAndPercentage(@RequestParam(value = "projectId", required = false) String projectId,
                                                 @RequestParam(value = "sub", required = false) Integer sub,
                                                 @RequestParam(value = "timestamp", required = false) String timestamp,
                                                 @RequestParam(value = "env", required = false) String env,
                                                 @RequestParam(value = "timeType", required = false) String timeType,
                                                 @RequestParam(value = "range", required = false) Integer range) {
        return AjaxResult.success(enhancePerformanceService.getPageNumberAndPercentage(projectId, sub, timestamp, env, timeType, range));
    }

    /**
     * 根据传入的时间分割条件获取指定日期指定耗时分段内的页面请求量
     */
    @GetMapping("/getPageNumberByTimeRange")
    public AjaxResult getPageNumberByTimeRange(@RequestParam(value = "projectId", required = false) String projectId,
                                               @RequestParam(value = "sub", required = false) Integer sub,
                                               @RequestParam(value = "timestamp", required = false) String timestamp,
                                               @RequestParam(value = "env", required = false) String env,
                                               @RequestParam(value = "timeType", required = false) String timeType,
                                               @RequestParam(value = "range", required = false) Integer range) {
        return AjaxResult.success(enhancePerformanceService.getPageNumberByTimeRange(projectId, sub, timestamp, env, timeType, range));
    }

    /**
     * 获取指定日期指定耗时分段1小时内的接口信息平均网络耗时、影响用户数、发生页面数、发生页面列表
     */
    @GetMapping("/getPageMessage")
    public AjaxResult getPageMessage(@RequestParam(value = "projectId", required = false) String projectId,
                                     @RequestParam(value = "sub", required = false) Integer sub,
                                     @RequestParam(value = "timestamp", required = false) String timestamp,
                                     @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(enhancePerformanceService.getPageMessage(projectId, sub, timestamp, env));
    }

    @GetMapping("/getPageInfo")
    public AjaxResult getPageInfo(@RequestParam(value = "url", required = false) String url,
                                 @RequestParam(value = "projectId", required = false) String projectId,
                                 @RequestParam(value = "sub", required = false) Integer sub,
                                 @RequestParam(value = "timestamp", required = false) String timestamp,
                                 @RequestParam(value = "env", required = false) String env) throws UnsupportedEncodingException {
        return AjaxResult.success(enhancePerformanceService.getPageInfo(url, projectId, sub, timestamp, env));
    }

    /**
     * 获取指定时间7天内TTFB、Dom解析、页面加载平均时间
     */
    @GetMapping("/getPageAVGTime")
    public AjaxResult getPageAVGTime(@RequestParam(value = "projectId", required = false) String projectId,
                                     @RequestParam(value = "timestamp", required = false) String timestamp,
                                     @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(enhancePerformanceService.getPageAVGTime(projectId, timestamp, env));
    }

    /**
     * 获取指定时间7天内总接口请求量、接口请求平均耗时、接口请求成功率
     */
    @GetMapping("/getApiAVGTime")
    public AjaxResult getApiAVGTime(@RequestParam(value = "projectId", required = false) String projectId,
                                    @RequestParam(value = "timestamp", required = false) String timestamp,
                                    @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(enhancePerformanceService.getApiAVGTime(projectId, timestamp, env));
    }

    /**
     * 获取指定时间7天内每个时间段访问的数量
     */
    @GetMapping("/getPageNumberInSevenDaysBySub")
    public AjaxResult getPageNumberInSevenDaysBySub(@RequestParam(value = "projectId", required = false) String projectId,
                                                    @RequestParam(value = "timestamp", required = false) String timestamp,
                                                    @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(enhancePerformanceService.getPageNumberInSevenDaysBySub(projectId, timestamp, env));
    }

    /**
     * 获取指定时间7天内每个时间段访问的数量
     */
    @GetMapping("/getApiNumberInSevenDaysBySub")
    public AjaxResult getApiNumberInSevenDaysBySub(@RequestParam(value = "projectId", required = false) String projectId,
                                                   @RequestParam(value = "timestamp", required = false) String timestamp,
                                                   @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(enhancePerformanceService.getApiNumberInSevenDaysBySub(projectId, timestamp, env));
    }

    /**
     * 页面加载耗时路由Top
     */
    @GetMapping("/getTopByPage")
    public AjaxResult getTopByPage(@RequestParam(value = "projectId", required = false) String projectId,
                                   @RequestParam(value = "timestamp", required = false) String timestamp,
                                   @RequestParam(value = "env", required = false) String env,
                                   @RequestParam(value = "number", required = false) Long number) {
        return AjaxResult.success(enhancePerformanceService.getTopByPage(projectId, timestamp, env, number));
    }

    /**
     * Api请求耗时路由Top
     */
    @GetMapping("/getTopByApi")
    public AjaxResult getTopByApi(@RequestParam(value = "projectId", required = false) String projectId,
                                  @RequestParam(value = "timestamp", required = false) String timestamp,
                                  @RequestParam(value = "env", required = false) String env,
                                  @RequestParam(value = "number", required = false) Long number) {
        return AjaxResult.success(enhancePerformanceService.getTopByApi(projectId, timestamp, env, number));
    }
}
