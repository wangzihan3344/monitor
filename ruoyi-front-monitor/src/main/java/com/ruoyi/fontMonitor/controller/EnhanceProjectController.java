package com.ruoyi.fontMonitor.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.fontMonitor.domain.FontMonitorMain;
import com.ruoyi.fontMonitor.domain.FontMonitorUserTeamProject;
import com.ruoyi.fontMonitor.service.IEnhanceProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@SuppressWarnings("all")
@RequestMapping("/enhance/project")
public class EnhanceProjectController {
    @Autowired
    private IEnhanceProjectService enhanceProjectService;

    /**
     * 创建项目
     */
    @PostMapping("/createProject")
    public AjaxResult createProject(@RequestBody FontMonitorUserTeamProject userTeamProject){
        enhanceProjectService.createProject(userTeamProject);
        return AjaxResult.success(userTeamProject);
    }

    /**
     * 获取指定时间应用流量数据（pv数、uv数、新访客数、ip数、人均访问次数、跳出率及各自较昨日增长率）
     */
    @GetMapping("/applicationTraffic")
    public AjaxResult applicationTraffic(@RequestBody FontMonitorMain monitorMain) {
        return AjaxResult.success(enhanceProjectService.applicationTraffic(monitorMain));
    }
}
