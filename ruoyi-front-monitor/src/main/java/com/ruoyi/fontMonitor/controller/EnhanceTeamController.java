package com.ruoyi.fontMonitor.controller;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.fontMonitor.service.IEnhanceTeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SuppressWarnings("all")
@RequestMapping("/enhance/team")
public class EnhanceTeamController {
    @Autowired
    private IEnhanceTeamService teamService;

    @PostMapping("/createTeam")
    public AjaxResult createTeam(@RequestBody JSONObject teamMessage) {
        return AjaxResult.success(teamService.createTeam(teamMessage));
    }

    @PostMapping("/addTeamMember")
    public AjaxResult addTeamMember(@RequestBody JSONObject teamAndMember) {
        return AjaxResult.success(teamService.addTeamMember(teamAndMember));
    }
}
