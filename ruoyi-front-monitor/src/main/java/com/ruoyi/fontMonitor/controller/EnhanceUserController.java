package com.ruoyi.fontMonitor.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.fontMonitor.service.IEnhanceUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/enhance/user")
@SuppressWarnings("all")
public class EnhanceUserController {
    @Autowired
    private IEnhanceUserService userService;

    /**
     * 获取指定时间30天每日访问新、老用户数
     */
    @GetMapping("/statisticsThirtyDaysInUser")
    public AjaxResult statisticsThirtyDaysInUser(@RequestParam(value = "projectId", required = false) String projectId,
                                                 @RequestParam(value = "timestamp", required = false) String timestamp,
                                                 @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(userService.statisticsThirtyDaysInUser(projectId, timestamp, env));
    }

    /**
     * 获取项目指定日期当天和一周前24小时内每小时页面访问量（pv量）
     */
    @GetMapping("/getTwentyFourHourPv")
    public AjaxResult getTwentyFourHourPv(@RequestParam(value = "projectId", required = false) String projectId,
                                          @RequestParam(value = "timestamp", required = false) String timestamp,
                                          @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(userService.getTwentyFourHourPv(projectId, timestamp, env));
    }

    /**
     * 获取项目指定日期当天和一周前24小时内每小时页面访问量（uv量）
     */
    @GetMapping("/getTwentyFourHourUv")
    public AjaxResult getTwentyFourHourUv(@RequestParam(value = "projectId", required = false) String projectId,
                                          @RequestParam(value = "timestamp", required = false) String timestamp,
                                          @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(userService.getTwentyFourHourUv(projectId, timestamp, env));
    }

    /**
     * 获取项目指定日期当天和一周前24小时内每小时页面访问量（newUser）
     */
    @GetMapping("/getTwentyFourHourNewUser")
    public AjaxResult getTwentyFourHourNewUser(@RequestParam(value = "projectId", required = false) String projectId,
                                               @RequestParam(value = "timestamp", required = false) String timestamp,
                                               @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(userService.getTwentyFourHourNewUser(projectId, timestamp, env));
    }

    /**
     * 获取项目指定日期10天内每天用户平均在线时长
     */
    @GetMapping("/getTenDaysUserAvgStay")
    public AjaxResult getTenDaysUserAvgStay(@RequestParam(value = "projectId", required = false) String projectId,
                                            @RequestParam(value = "timestamp", required = false) String timestamp,
                                            @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(userService.getTenDaysUserAvgStay(projectId, timestamp, env));
    }

    /**
     * 获取项目指定日期当天10天内新用户次日留存率
     */
    @GetMapping("/getTenDaysNewUserInNextDay")
    public AjaxResult getTenDaysNewUserInNextDay(@RequestParam(value = "projectId", required = false) String projectId,
                                                 @RequestParam(value = "timestamp", required = false) String timestamp,
                                                 @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(userService.getTenDaysNewUserInNextDay(projectId, timestamp, env));
    }

    /**
     * 获取访问量最高的前n个网站地址
     */
    @GetMapping("/getTopByUrl")
    public AjaxResult getTopByUrl(@RequestParam(value = "projectId", required = false) String projectId,
                                  @RequestParam(value = "appointNumber", required = false) Long appointNumber,
                                  @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(userService.getTopByUrl(projectId, appointNumber, env));
    }

    /**
     * 获取访问量最高的前n个浏览器
     */
    @GetMapping("/getTopByBrowser")
    public AjaxResult getTopByBrowser(@RequestParam(value = "projectId", required = false) String projectId,
                                      @RequestParam(value = "appointNumber", required = false) Long appointNumber,
                                      @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(userService.getTopByBrowser(projectId, appointNumber, env));
    }

    /**
     * 获取访问量最高的前n个系统版本
     */
    @GetMapping("/getTopByOS")
    public AjaxResult getTopByOS(@RequestParam(value = "projectId", required = false) String projectId,
                                 @RequestParam(value = "appointNumber", required = false) Long appointNumber,
                                 @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(userService.getTopByOS(projectId, appointNumber, env));
    }

    /**
     * 获取访问量最高的前n个屏幕分辨率
     */
    @GetMapping("/getTopByScreen")
    public AjaxResult getTopByScreen(@RequestParam(value = "projectId", required = false) String projectId,
                                     @RequestParam(value = "appointNumber", required = false) Long appointNumber,
                                     @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(userService.getTopByScreen(projectId, appointNumber, env));
    }

    /**
     * 获取访问量最高的前n个城市
     */
    @GetMapping("/getTopByCity")
    public AjaxResult getTopByCity(@RequestParam(value = "projectId", required = false) String projectId,
                                   @RequestParam(value = "appointNumber", required = false) Long appointNumber,
                                   @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(userService.getTopByCity(projectId, appointNumber, env));
    }

    /**
     * 获取访问量最高的前n个国家
     */
    @GetMapping("/getTopByCountry")
    public AjaxResult getTopByCountry(@RequestParam(value = "projectId", required = false) String projectId,
                                      @RequestParam(value = "isWorld", required = false) Integer isWorld,
                                      @RequestParam(value = "appointNumber", required = false) Long appointNumber,
                                      @RequestParam(value = "env", required = false) String env,
                                      @RequestParam(value = "timestamp", required = false) String timestamp) {
        if (isWorld == 1)
            return AjaxResult.success(userService.getTopByCountry(projectId, appointNumber, env, timestamp));
        else if (isWorld == 0)
            return AjaxResult.success(userService.getTopByProvince(projectId, appointNumber, env, timestamp));
        else
            return AjaxResult.error("没有这个类型");
    }

    /**
     * 获取访问量最高的前n个城市
     */
    @GetMapping("/getTopByResourceUrl")
    public AjaxResult getTopByResourceUrl(@RequestParam(value = "projectId", required = false) String projectId,
                                          @RequestParam(value = "appointNumber", required = false) Long appointNumber,
                                          @RequestParam(value = "env", required = false) String env) {
        return AjaxResult.success(userService.getTopByResourceUrl(projectId, appointNumber, env));
    }


    /**
     * 获取项目用户信息列表
     */
    @GetMapping("/getMainListGroupByFirstTimestamp")
    public AjaxResult getMainListGroupByFirstTimestamp(@RequestParam(value = "projectId", required = false) String projectId,
                                                       @RequestParam(value = "ip", required = false) String ip,
                                                       @RequestParam(value = "timestamp", required = false) String timestamp,
                                                       @RequestParam(value = "env", required = false) String env,
                                                       @RequestParam(value = "uuid", required = false) String uuid) {
        return AjaxResult.success(userService.getMainListGroupByFirstTimestamp(projectId, ip, timestamp, env, uuid));
    }

    /**
     * 获取用户行为记录
     */
    @GetMapping("/getUserAction")
    public AjaxResult getUserAction(@RequestParam(value = "mainId") String mainId,
                                    @RequestParam(value = "timestamp", required = false) String timestamp,
                                    @RequestParam(value = "type", required = false) String type) {
        return AjaxResult.success(userService.getUserAction(mainId, timestamp, type));
    }

    /**
     * 获取用户访问页面平均加载耗时
     */
    @GetMapping("/getUserPageAvgLoadTime")
    public AjaxResult getUserPageAvgLoadTime(@RequestParam(value = "mainId") String mainId) {
        return AjaxResult.success(userService.getUserPageAvgLoadTime(mainId));
    }

    /**
     * 获取用户访问接口耗时区间分布
     */
    @GetMapping("/getUserInterfaceAvgTime")
    public AjaxResult getUserInterfaceAvgTime(@RequestParam(value = "mainId") String mainId) {
        return AjaxResult.success(userService.getUserInterfaceAvgTime(mainId));
    }
}
