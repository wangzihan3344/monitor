package com.ruoyi.fontMonitor.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.fontMonitor.domain.FontMonitorUserMessage;
import com.ruoyi.fontMonitor.service.IEnhanceUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SuppressWarnings("all")
@RequestMapping("/enhance/userInfo")
public class EnhanceUserInfoController {
    @Autowired
    private IEnhanceUserInfoService userInfoService;

    @PostMapping("/userLogin")
    public AjaxResult userLogin(@RequestBody FontMonitorUserMessage userMessage) {
        userInfoService.userLogin(userMessage);
        return AjaxResult.success("登录成功");
    }

    @PostMapping("/userRegister")
    public AjaxResult userRegister(@RequestBody FontMonitorUserMessage userMessage) {
        return AjaxResult.success("注册成功", userInfoService.userRegister(userMessage));
    }
}
