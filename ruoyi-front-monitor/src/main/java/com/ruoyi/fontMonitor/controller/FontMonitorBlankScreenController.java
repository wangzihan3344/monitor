package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorBlankScreen;
import com.ruoyi.fontMonitor.service.IFontMonitorBlankScreenService;

/**
 * 监控白屏Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("All")
@RequestMapping("/fontMonitor/fontMonitorBlankScreen")
public class FontMonitorBlankScreenController extends BaseController {
    @Autowired
    private IFontMonitorBlankScreenService fontMonitorBlankScreenService;

    /**
     * 查询监控白屏列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorBlankScreen:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorBlankScreen:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorBlankScreen fontMonitorBlankScreen) {
        startPage();
        List<FontMonitorBlankScreen> list = fontMonitorBlankScreenService.selectFontMonitorBlankScreenList(fontMonitorBlankScreen);
        return getDataTable(list);
    }

    /**
     * 导出监控白屏列表
     */
    //@PreAuthorize("fontMonitor:fontMonitorBlankScreen:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorBlankScreen:export')")
    @Log(title = "监控白屏", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorBlankScreen fontMonitorBlankScreen) {
        List<FontMonitorBlankScreen> list = fontMonitorBlankScreenService.selectFontMonitorBlankScreenList(fontMonitorBlankScreen);
        ExcelUtil<FontMonitorBlankScreen> util = new ExcelUtil<FontMonitorBlankScreen>(FontMonitorBlankScreen.class);
        util.exportExcel(response, list, "监控白屏数据");
    }

    /**
     * 获取监控白屏详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorBlankScreen:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorBlankScreen:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorBlankScreenService.selectFontMonitorBlankScreenById(id));
    }

    /**
     * 新增监控白屏
     */
    //@RequiresPermissions("fontMonitor:fontMonitorBlankScreen:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorBlankScreen:add')")
    @Log(title = "监控白屏", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorBlankScreen fontMonitorBlankScreen) {
        return toAjax(fontMonitorBlankScreenService.insertFontMonitorBlankScreen(fontMonitorBlankScreen));
    }

    /**
     * 修改监控白屏
     */
    //@RequiresPermissions("fontMonitor:fontMonitorBlankScreen:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorBlankScreen:edit')")
    @Log(title = "监控白屏", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorBlankScreen fontMonitorBlankScreen) {
        return toAjax(fontMonitorBlankScreenService.updateFontMonitorBlankScreen(fontMonitorBlankScreen));
    }

    /**
     * 删除监控白屏
     */
    //@RequiresPermissions("fontMonitor:fontMonitorBlankScreen:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorBlankScreen:remove')")
    @Log(title = "监控白屏", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorBlankScreenService.deleteFontMonitorBlankScreenByIds(ids));
    }
}
