package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorCustomMessage;
import com.ruoyi.fontMonitor.service.IFontMonitorCustomMessageService;

/**
 * custom_error 的 message 数据Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorCustomMessage")
public class FontMonitorCustomMessageController extends BaseController {
    @Autowired
    private IFontMonitorCustomMessageService fontMonitorCustomMessageService;

    /**
     * 查询custom_error 的 message 数据列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorCustomMessage:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorCustomMessage:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorCustomMessage fontMonitorCustomMessage) {
        startPage();
        List<FontMonitorCustomMessage> list = fontMonitorCustomMessageService.selectFontMonitorCustomMessageList(fontMonitorCustomMessage);
        return getDataTable(list);
    }

    /**
     * 导出custom_error 的 message 数据列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorCustomMessage:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorCustomMessage:export')")
    @Log(title = "custom_error 的 message 数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorCustomMessage fontMonitorCustomMessage) {
        List<FontMonitorCustomMessage> list = fontMonitorCustomMessageService.selectFontMonitorCustomMessageList(fontMonitorCustomMessage);
        ExcelUtil<FontMonitorCustomMessage> util = new ExcelUtil<FontMonitorCustomMessage>(FontMonitorCustomMessage.class);
        util.exportExcel(response, list, "custom_error 的 message 数据数据");
    }

    /**
     * 获取custom_error 的 message 数据详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorCustomMessage:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorCustomMessage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorCustomMessageService.selectFontMonitorCustomMessageById(id));
    }

    /**
     * 新增custom_error 的 message 数据
     */
    //@RequiresPermissions("fontMonitor:fontMonitorCustomMessage:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorCustomMessage:add')")
    @Log(title = "custom_error 的 message 数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorCustomMessage fontMonitorCustomMessage) {
        return toAjax(fontMonitorCustomMessageService.insertFontMonitorCustomMessage(fontMonitorCustomMessage));
    }

    /**
     * 修改custom_error 的 message 数据
     */
    //@RequiresPermissions("fontMonitor:fontMonitorCustomMessage:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorCustomMessage:edit')")
    @Log(title = "custom_error 的 message 数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorCustomMessage fontMonitorCustomMessage) {
        return toAjax(fontMonitorCustomMessageService.updateFontMonitorCustomMessage(fontMonitorCustomMessage));
    }

    /**
     * 删除custom_error 的 message 数据
     */
    //@RequiresPermissions("fontMonitor:fontMonitorCustomMessage:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorCustomMessage:remove')")
    @Log(title = "custom_error 的 message 数据", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorCustomMessageService.deleteFontMonitorCustomMessageByIds(ids));
    }
}
