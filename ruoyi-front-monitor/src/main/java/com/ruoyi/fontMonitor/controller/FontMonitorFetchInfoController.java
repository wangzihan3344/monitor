package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorFetchInfo;
import com.ruoyi.fontMonitor.service.IFontMonitorFetchInfoService;

/**
 * 监控 fetch 接口Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorFetchInfo")
public class FontMonitorFetchInfoController extends BaseController {
    @Autowired
    private IFontMonitorFetchInfoService fontMonitorFetchInfoService;

    /**
     * 查询监控 fetch 接口列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorFetchInfo:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorFetchInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorFetchInfo fontMonitorFetchInfo) {
        startPage();
        List<FontMonitorFetchInfo> list = fontMonitorFetchInfoService.selectFontMonitorFetchInfoList(fontMonitorFetchInfo);
        return getDataTable(list);
    }

    /**
     * 导出监控 fetch 接口列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorFetchInfo:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorFetchInfo:export')")
    @Log(title = "监控 fetch 接口", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorFetchInfo fontMonitorFetchInfo) {
        List<FontMonitorFetchInfo> list = fontMonitorFetchInfoService.selectFontMonitorFetchInfoList(fontMonitorFetchInfo);
        ExcelUtil<FontMonitorFetchInfo> util = new ExcelUtil<FontMonitorFetchInfo>(FontMonitorFetchInfo.class);
        util.exportExcel(response, list, "监控 fetch 接口数据");
    }

    /**
     * 获取监控 fetch 接口详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorFetchInfo:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorFetchInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorFetchInfoService.selectFontMonitorFetchInfoById(id));
    }

    /**
     * 新增监控 fetch 接口
     */
    //@RequiresPermissions("fontMonitor:fontMonitorFetchInfo:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorFetchInfo:add')")
    @Log(title = "监控 fetch 接口", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorFetchInfo fontMonitorFetchInfo) {
        return toAjax(fontMonitorFetchInfoService.insertFontMonitorFetchInfo(fontMonitorFetchInfo));
    }

    /**
     * 修改监控 fetch 接口
     */
    //@RequiresPermissions("fontMonitor:fontMonitorFetchInfo:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorFetchInfo:edit')")
    @Log(title = "监控 fetch 接口", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorFetchInfo fontMonitorFetchInfo) {
        return toAjax(fontMonitorFetchInfoService.updateFontMonitorFetchInfo(fontMonitorFetchInfo));
    }

    /**
     * 删除监控 fetch 接口
     */
    //@RequiresPermissions("fontMonitor:fontMonitorFetchInfo:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorFetchInfo:remove')")
    @Log(title = "监控 fetch 接口", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorFetchInfoService.deleteFontMonitorFetchInfoByIds(ids));
    }
}
