package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorFirstInputDelay;
import com.ruoyi.fontMonitor.service.IFontMonitorFirstInputDelayService;

/**
 * 监控首次输入响应延迟Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorFirstInputDelay")
public class FontMonitorFirstInputDelayController extends BaseController {
    @Autowired
    private IFontMonitorFirstInputDelayService fontMonitorFirstInputDelayService;

    /**
     * 查询监控首次输入响应延迟列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorFirstInputDelay:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorFirstInputDelay:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorFirstInputDelay fontMonitorFirstInputDelay) {
        startPage();
        List<FontMonitorFirstInputDelay> list = fontMonitorFirstInputDelayService.selectFontMonitorFirstInputDelayList(fontMonitorFirstInputDelay);
        return getDataTable(list);
    }

    /**
     * 导出监控首次输入响应延迟列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorFirstInputDelay:export")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorFirstInputDelay:export')")
    @Log(title = "监控首次输入响应延迟", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorFirstInputDelay fontMonitorFirstInputDelay) {
        List<FontMonitorFirstInputDelay> list = fontMonitorFirstInputDelayService.selectFontMonitorFirstInputDelayList(fontMonitorFirstInputDelay);
        ExcelUtil<FontMonitorFirstInputDelay> util = new ExcelUtil<FontMonitorFirstInputDelay>(FontMonitorFirstInputDelay.class);
        util.exportExcel(response, list, "监控首次输入响应延迟数据");
    }

    /**
     * 获取监控首次输入响应延迟详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorFirstInputDelay:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorFirstInputDelay:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorFirstInputDelayService.selectFontMonitorFirstInputDelayById(id));
    }

    /**
     * 新增监控首次输入响应延迟
     */
    //@RequiresPermissions("fontMonitor:fontMonitorFirstInputDelay:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorFirstInputDelay:add')")
    @Log(title = "监控首次输入响应延迟", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorFirstInputDelay fontMonitorFirstInputDelay) {
        return toAjax(fontMonitorFirstInputDelayService.insertFontMonitorFirstInputDelay(fontMonitorFirstInputDelay));
    }

    /**
     * 修改监控首次输入响应延迟
     */
    //@RequiresPermissions("fontMonitor:fontMonitorFirstInputDelay:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorFirstInputDelay:edit')")
    @Log(title = "监控首次输入响应延迟", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorFirstInputDelay fontMonitorFirstInputDelay) {
        return toAjax(fontMonitorFirstInputDelayService.updateFontMonitorFirstInputDelay(fontMonitorFirstInputDelay));
    }

    /**
     * 删除监控首次输入响应延迟
     */
    //@RequiresPermissions("fontMonitor:fontMonitorFirstInputDelay:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorFirstInputDelay:remove')")
    @Log(title = "监控首次输入响应延迟", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorFirstInputDelayService.deleteFontMonitorFirstInputDelayByIds(ids));
    }
}
