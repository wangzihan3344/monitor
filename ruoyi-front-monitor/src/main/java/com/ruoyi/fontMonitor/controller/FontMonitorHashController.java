package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorHash;
import com.ruoyi.fontMonitor.service.IFontMonitorHashService;

/**
 * url变动Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorHash")
public class FontMonitorHashController extends BaseController {
    @Autowired
    private IFontMonitorHashService fontMonitorHashService;

    /**
     * 查询url变动列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorHash:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorHash:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorHash fontMonitorHash) {
        startPage();
        List<FontMonitorHash> list = fontMonitorHashService.selectFontMonitorHashList(fontMonitorHash);
        return getDataTable(list);
    }

    /**
     * 导出url变动列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorHash:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorHash:export')")
    @Log(title = "url变动", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorHash fontMonitorHash) {
        List<FontMonitorHash> list = fontMonitorHashService.selectFontMonitorHashList(fontMonitorHash);
        ExcelUtil<FontMonitorHash> util = new ExcelUtil<FontMonitorHash>(FontMonitorHash.class);
        util.exportExcel(response, list, "url变动数据");
    }

    /**
     * 获取url变动详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorHash:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorHash:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorHashService.selectFontMonitorHashById(id));
    }

    /**
     * 新增url变动
     */
    //@RequiresPermissions("fontMonitor:fontMonitorHash:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorHash:add')")
    @Log(title = "url变动", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorHash fontMonitorHash) {
        return toAjax(fontMonitorHashService.insertFontMonitorHash(fontMonitorHash));
    }

    /**
     * 修改url变动
     */
    //@RequiresPermissions("fontMonitor:fontMonitorHash:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorHash:edit')")
    @Log(title = "url变动", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorHash fontMonitorHash) {
        return toAjax(fontMonitorHashService.updateFontMonitorHash(fontMonitorHash));
    }

    /**
     * 删除url变动
     */
    //@RequiresPermissions("fontMonitor:fontMonitorHash:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorHash:remove')")
    @Log(title = "url变动", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorHashService.deleteFontMonitorHashByIds(ids));
    }
}
