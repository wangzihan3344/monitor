package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorHistory;
import com.ruoyi.fontMonitor.service.IFontMonitorHistoryService;

/**
 * 路由记录Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorHistory")
public class FontMonitorHistoryController extends BaseController {
    @Autowired
    private IFontMonitorHistoryService fontMonitorHistoryService;

    /**
     * 查询路由记录列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorHistory:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorHistory:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorHistory fontMonitorHistory) {
        startPage();
        List<FontMonitorHistory> list = fontMonitorHistoryService.selectFontMonitorHistoryList(fontMonitorHistory);
        return getDataTable(list);
    }

    /**
     * 导出路由记录列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorHistory:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorHistory:export')")
    @Log(title = "路由记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorHistory fontMonitorHistory) {
        List<FontMonitorHistory> list = fontMonitorHistoryService.selectFontMonitorHistoryList(fontMonitorHistory);
        ExcelUtil<FontMonitorHistory> util = new ExcelUtil<FontMonitorHistory>(FontMonitorHistory.class);
        util.exportExcel(response, list, "路由记录数据");
    }

    /**
     * 获取路由记录详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorHistory:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorHistory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorHistoryService.selectFontMonitorHistoryById(id));
    }

    /**
     * 新增路由记录
     */
    //@RequiresPermissions("fontMonitor:fontMonitorHistory:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorHistory:add')")
    @Log(title = "路由记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorHistory fontMonitorHistory) {
        return toAjax(fontMonitorHistoryService.insertFontMonitorHistory(fontMonitorHistory));
    }

    /**
     * 修改路由记录
     */
    //@RequiresPermissions("fontMonitor:fontMonitorHistory:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorHistory:edit')")
    @Log(title = "路由记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorHistory fontMonitorHistory) {
        return toAjax(fontMonitorHistoryService.updateFontMonitorHistory(fontMonitorHistory));
    }

    /**
     * 删除路由记录
     */
    //@RequiresPermissions("fontMonitor:fontMonitorHistory:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorHistory:remove')")
    @Log(title = "路由记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorHistoryService.deleteFontMonitorHistoryByIds(ids));
    }
}
