package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorJsError;
import com.ruoyi.fontMonitor.service.IFontMonitorJsErrorService;

/**
 * 监控 jsErrorController
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorJsError")
public class FontMonitorJsErrorController extends BaseController {
    @Autowired
    private IFontMonitorJsErrorService fontMonitorJsErrorService;

    /**
     * 查询监控 jsError列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorJsError:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorJsError:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorJsError fontMonitorJsError) {
        startPage();
        List<FontMonitorJsError> list = fontMonitorJsErrorService.selectFontMonitorJsErrorList(fontMonitorJsError);
        return getDataTable(list);
    }

    /**
     * 导出监控 jsError列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorJsError:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorJsError:export')")
    @Log(title = "监控 jsError", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorJsError fontMonitorJsError) {
        List<FontMonitorJsError> list = fontMonitorJsErrorService.selectFontMonitorJsErrorList(fontMonitorJsError);
        ExcelUtil<FontMonitorJsError> util = new ExcelUtil<FontMonitorJsError>(FontMonitorJsError.class);
        util.exportExcel(response, list, "监控 jsError数据");
    }

    /**
     * 获取监控 jsError详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorJsError:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorJsError:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorJsErrorService.selectFontMonitorJsErrorById(id));
    }

    /**
     * 新增监控 jsError
     */
    //@RequiresPermissions("fontMonitor:fontMonitorJsError:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorJsError:add')")
    @Log(title = "监控 jsError", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorJsError fontMonitorJsError) {
        return toAjax(fontMonitorJsErrorService.insertFontMonitorJsError(fontMonitorJsError));
    }

    /**
     * 修改监控 jsError
     */
    //@RequiresPermissions("fontMonitor:fontMonitorJsError:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorJsError:edit')")
    @Log(title = "监控 jsError", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorJsError fontMonitorJsError) {
        return toAjax(fontMonitorJsErrorService.updateFontMonitorJsError(fontMonitorJsError));
    }

    /**
     * 删除监控 jsError
     */
    //@RequiresPermissions("fontMonitor:fontMonitorJsError:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorJsError:remove')")
    @Log(title = "监控 jsError", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorJsErrorService.deleteFontMonitorJsErrorByIds(ids));
    }
}
