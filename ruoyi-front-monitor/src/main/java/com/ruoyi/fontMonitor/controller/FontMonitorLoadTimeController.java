package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorLoadTime;
import com.ruoyi.fontMonitor.service.IFontMonitorLoadTimeService;

/**
 * 监控加载时间Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorLoadTime")
public class FontMonitorLoadTimeController extends BaseController {
    @Autowired
    private IFontMonitorLoadTimeService fontMonitorLoadTimeService;

    /**
     * 查询监控加载时间列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorLoadTime:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorLoadTime:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorLoadTime fontMonitorLoadTime) {
        startPage();
        List<FontMonitorLoadTime> list = fontMonitorLoadTimeService.selectFontMonitorLoadTimeList(fontMonitorLoadTime);
        return getDataTable(list);
    }

    /**
     * 导出监控加载时间列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorLoadTime:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorLoadTime:list')")
    @Log(title = "监控加载时间", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorLoadTime fontMonitorLoadTime) {
        List<FontMonitorLoadTime> list = fontMonitorLoadTimeService.selectFontMonitorLoadTimeList(fontMonitorLoadTime);
        ExcelUtil<FontMonitorLoadTime> util = new ExcelUtil<FontMonitorLoadTime>(FontMonitorLoadTime.class);
        util.exportExcel(response, list, "监控加载时间数据");
    }

    /**
     * 获取监控加载时间详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorLoadTime:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorLoadTime:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorLoadTimeService.selectFontMonitorLoadTimeById(id));
    }

    /**
     * 新增监控加载时间
     */
    //@RequiresPermissions("fontMonitor:fontMonitorLoadTime:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorLoadTime:add')")
    @Log(title = "监控加载时间", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorLoadTime fontMonitorLoadTime) {
        return toAjax(fontMonitorLoadTimeService.insertFontMonitorLoadTime(fontMonitorLoadTime));
    }

    /**
     * 修改监控加载时间
     */
    //@RequiresPermissions("fontMonitor:fontMonitorLoadTime:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorLoadTime:edit')")
    @Log(title = "监控加载时间", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorLoadTime fontMonitorLoadTime) {
        return toAjax(fontMonitorLoadTimeService.updateFontMonitorLoadTime(fontMonitorLoadTime));
    }

    /**
     * 删除监控加载时间
     */
    //@RequiresPermissions("fontMonitor:fontMonitorLoadTime:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorLoadTime:remove')")
    @Log(title = "监控加载时间", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorLoadTimeService.deleteFontMonitorLoadTimeByIds(ids));
    }
}
