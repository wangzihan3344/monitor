package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorLongTask;
import com.ruoyi.fontMonitor.service.IFontMonitorLongTaskService;

/**
 * 监控卡顿Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorLongTask")
public class FontMonitorLongTaskController extends BaseController {
    @Autowired
    private IFontMonitorLongTaskService fontMonitorLongTaskService;

    /**
     * 查询监控卡顿列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorLongTask:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorLongTask:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorLongTask fontMonitorLongTask) {
        startPage();
        List<FontMonitorLongTask> list = fontMonitorLongTaskService.selectFontMonitorLongTaskList(fontMonitorLongTask);
        return getDataTable(list);
    }

    /**
     * 导出监控卡顿列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorLongTask:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorLongTask:export')")
    @Log(title = "监控卡顿", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorLongTask fontMonitorLongTask) {
        List<FontMonitorLongTask> list = fontMonitorLongTaskService.selectFontMonitorLongTaskList(fontMonitorLongTask);
        ExcelUtil<FontMonitorLongTask> util = new ExcelUtil<FontMonitorLongTask>(FontMonitorLongTask.class);
        util.exportExcel(response, list, "监控卡顿数据");
    }

    /**
     * 获取监控卡顿详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorLongTask:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorLongTask:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorLongTaskService.selectFontMonitorLongTaskById(id));
    }

    /**
     * 新增监控卡顿
     */
    //@RequiresPermissions("fontMonitor:fontMonitorLongTask:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorLongTask:add')")
    @Log(title = "监控卡顿", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorLongTask fontMonitorLongTask) {
        return toAjax(fontMonitorLongTaskService.insertFontMonitorLongTask(fontMonitorLongTask));
    }

    /**
     * 修改监控卡顿
     */
    //@RequiresPermissions("fontMonitor:fontMonitorLongTask:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorLongTask:edit')")
    @Log(title = "监控卡顿", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorLongTask fontMonitorLongTask) {
        return toAjax(fontMonitorLongTaskService.updateFontMonitorLongTask(fontMonitorLongTask));
    }

    /**
     * 删除监控卡顿
     */
    //@RequiresPermissions("fontMonitor:fontMonitorLongTask:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorLongTask:remove')")
    @Log(title = "监控卡顿", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorLongTaskService.deleteFontMonitorLongTaskByIds(ids));
    }
}
