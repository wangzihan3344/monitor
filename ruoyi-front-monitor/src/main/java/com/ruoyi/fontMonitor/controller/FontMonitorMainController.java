package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorMain;
import com.ruoyi.fontMonitor.service.IFontMonitorMainService;

/**
 * 公共字段Controller
 *
 * @author ruoyi
 * @date 2022-08-30
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorMain")
public class FontMonitorMainController extends BaseController {
    @Autowired
    private IFontMonitorMainService fontMonitorMainService;

    /**
     * 查询公共字段列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorMain:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorMain:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorMain fontMonitorMain) {
        startPage();
        List<FontMonitorMain> list = fontMonitorMainService.selectFontMonitorMainList(fontMonitorMain);
        return getDataTable(list);
    }

    /**
     * 导出公共字段列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorMain:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorMain:export')")
    @Log(title = "公共字段", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorMain fontMonitorMain) {
        List<FontMonitorMain> list = fontMonitorMainService.selectFontMonitorMainList(fontMonitorMain);
        ExcelUtil<FontMonitorMain> util = new ExcelUtil<FontMonitorMain>(FontMonitorMain.class);
        util.exportExcel(response, list, "公共字段数据");
    }

    /**
     * 获取公共字段详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorMain:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorMain:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorMainService.selectFontMonitorMainById(id));
    }

    /**
     * 新增公共字段
     */
    //@RequiresPermissions("fontMonitor:fontMonitorMain:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorMain:add')")
    @Log(title = "公共字段", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorMain fontMonitorMain) {
        return toAjax(fontMonitorMainService.insertFontMonitorMain(fontMonitorMain));
    }

    /**
     * 修改公共字段
     */
    //@RequiresPermissions("fontMonitor:fontMonitorMain:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorMain:edit')")
    @Log(title = "公共字段", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorMain fontMonitorMain) {
        return toAjax(fontMonitorMainService.updateFontMonitorMain(fontMonitorMain));
    }

    /**
     * 删除公共字段
     */
    //@RequiresPermissions("fontMonitor:fontMonitorMain:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorMain:remove')")
    @Log(title = "公共字段", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorMainService.deleteFontMonitorMainByIds(ids));
    }
}
