package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorPaintTime;
import com.ruoyi.fontMonitor.service.IFontMonitorPaintTimeService;

/**
 * 监控渲染时间Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorPaintTime")
public class FontMonitorPaintTimeController extends BaseController {
    @Autowired
    private IFontMonitorPaintTimeService fontMonitorPaintTimeService;

    /**
     * 查询监控渲染时间列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorPaintTime:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorPaintTime:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorPaintTime fontMonitorPaintTime) {
        startPage();
        List<FontMonitorPaintTime> list = fontMonitorPaintTimeService.selectFontMonitorPaintTimeList(fontMonitorPaintTime);
        return getDataTable(list);
    }

    /**
     * 导出监控渲染时间列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorPaintTime:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorPaintTime:export')")
    @Log(title = "监控渲染时间", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorPaintTime fontMonitorPaintTime) {
        List<FontMonitorPaintTime> list = fontMonitorPaintTimeService.selectFontMonitorPaintTimeList(fontMonitorPaintTime);
        ExcelUtil<FontMonitorPaintTime> util = new ExcelUtil<FontMonitorPaintTime>(FontMonitorPaintTime.class);
        util.exportExcel(response, list, "监控渲染时间数据");
    }

    /**
     * 获取监控渲染时间详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorPaintTime:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorPaintTime:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorPaintTimeService.selectFontMonitorPaintTimeById(id));
    }

    /**
     * 新增监控渲染时间
     */
    //@RequiresPermissions("fontMonitor:fontMonitorPaintTime:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorPaintTime:add')")
    @Log(title = "监控渲染时间", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorPaintTime fontMonitorPaintTime) {
        return toAjax(fontMonitorPaintTimeService.insertFontMonitorPaintTime(fontMonitorPaintTime));
    }

    /**
     * 修改监控渲染时间
     */
    //@RequiresPermissions("fontMonitor:fontMonitorPaintTime:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorPaintTime:edit')")
    @Log(title = "监控渲染时间", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorPaintTime fontMonitorPaintTime) {
        return toAjax(fontMonitorPaintTimeService.updateFontMonitorPaintTime(fontMonitorPaintTime));
    }

    /**
     * 删除监控渲染时间
     */
    //@RequiresPermissions("fontMonitor:fontMonitorPaintTime:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorPaintTime:remove')")
    @Log(title = "监控渲染时间", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorPaintTimeService.deleteFontMonitorPaintTimeByIds(ids));
    }
}
