package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorPromiseError;
import com.ruoyi.fontMonitor.service.IFontMonitorPromiseErrorService;

/**
 * 监控 promiseErrorController
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorPromiseError")
public class FontMonitorPromiseErrorController extends BaseController {
    @Autowired
    private IFontMonitorPromiseErrorService fontMonitorPromiseErrorService;

    /**
     * 查询监控 promiseError列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorPromiseError:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorPromiseError:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorPromiseError fontMonitorPromiseError) {
        startPage();
        List<FontMonitorPromiseError> list = fontMonitorPromiseErrorService.selectFontMonitorPromiseErrorList(fontMonitorPromiseError);
        return getDataTable(list);
    }

    /**
     * 导出监控 promiseError列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorPromiseError:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorPromiseError:export')")
    @Log(title = "监控 promiseError", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorPromiseError fontMonitorPromiseError) {
        List<FontMonitorPromiseError> list = fontMonitorPromiseErrorService.selectFontMonitorPromiseErrorList(fontMonitorPromiseError);
        ExcelUtil<FontMonitorPromiseError> util = new ExcelUtil<FontMonitorPromiseError>(FontMonitorPromiseError.class);
        util.exportExcel(response, list, "监控 promiseError数据");
    }

    /**
     * 获取监控 promiseError详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorPromiseError:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorPromiseError:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorPromiseErrorService.selectFontMonitorPromiseErrorById(id));
    }

    /**
     * 新增监控 promiseError
     */
    //@RequiresPermissions("fontMonitor:fontMonitorPromiseError:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorPromiseError:add')")
    @Log(title = "监控 promiseError", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorPromiseError fontMonitorPromiseError) {
        return toAjax(fontMonitorPromiseErrorService.insertFontMonitorPromiseError(fontMonitorPromiseError));
    }

    /**
     * 修改监控 promiseError
     */
    //@RequiresPermissions("fontMonitor:fontMonitorPromiseError:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorPromiseError:edit')")
    @Log(title = "监控 promiseError", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorPromiseError fontMonitorPromiseError) {
        return toAjax(fontMonitorPromiseErrorService.updateFontMonitorPromiseError(fontMonitorPromiseError));
    }

    /**
     * 删除监控 promiseError
     */
    //@RequiresPermissions("fontMonitor:fontMonitorPromiseError:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorPromiseError:remove')")
    @Log(title = "监控 promiseError", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorPromiseErrorService.deleteFontMonitorPromiseErrorByIds(ids));
    }
}
