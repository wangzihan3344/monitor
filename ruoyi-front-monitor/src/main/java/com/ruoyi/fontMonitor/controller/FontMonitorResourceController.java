package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorResource;
import com.ruoyi.fontMonitor.service.IFontMonitorResourceService;

/**
 * 资源监听Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorResource")
public class FontMonitorResourceController extends BaseController {
    @Autowired
    private IFontMonitorResourceService fontMonitorResourceService;

    /**
     * 查询资源监听列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorResource:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorResource:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorResource fontMonitorResource) {
        startPage();
        List<FontMonitorResource> list = fontMonitorResourceService.selectFontMonitorResourceList(fontMonitorResource);
        return getDataTable(list);
    }

    /**
     * 导出资源监听列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorResource:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorResource:export')")
    @Log(title = "资源监听", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorResource fontMonitorResource) {
        List<FontMonitorResource> list = fontMonitorResourceService.selectFontMonitorResourceList(fontMonitorResource);
        ExcelUtil<FontMonitorResource> util = new ExcelUtil<FontMonitorResource>(FontMonitorResource.class);
        util.exportExcel(response, list, "资源监听数据");
    }

    /**
     * 获取资源监听详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorResource:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorResource:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorResourceService.selectFontMonitorResourceById(id));
    }

    /**
     * 新增资源监听
     */
    //@RequiresPermissions("fontMonitor:fontMonitorResource:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorResource:add')")
    @Log(title = "资源监听", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorResource fontMonitorResource) {
        return toAjax(fontMonitorResourceService.insertFontMonitorResource(fontMonitorResource));
    }

    /**
     * 修改资源监听
     */
    //@RequiresPermissions("fontMonitor:fontMonitorResource:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorResource:edit')")
    @Log(title = "资源监听", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorResource fontMonitorResource) {
        return toAjax(fontMonitorResourceService.updateFontMonitorResource(fontMonitorResource));
    }

    /**
     * 删除资源监听
     */
    //@RequiresPermissions("fontMonitor:fontMonitorResource:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorResource:remove')")
    @Log(title = "资源监听", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorResourceService.deleteFontMonitorResourceByIds(ids));
    }
}
