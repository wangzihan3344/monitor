package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorResourceError;
import com.ruoyi.fontMonitor.service.IFontMonitorResourceErrorService;

/**
 * 监控 resourceErrorController
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorResourceError")
public class FontMonitorResourceErrorController extends BaseController {
    @Autowired
    private IFontMonitorResourceErrorService fontMonitorResourceErrorService;

    /**
     * 查询监控 resourceError列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorResourceError:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorResourceError:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorResourceError fontMonitorResourceError) {
        startPage();
        List<FontMonitorResourceError> list = fontMonitorResourceErrorService.selectFontMonitorResourceErrorList(fontMonitorResourceError);
        return getDataTable(list);
    }

    /**
     * 导出监控 resourceError列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorResourceError:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorResourceError:export')")
    @Log(title = "监控 resourceError", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorResourceError fontMonitorResourceError) {
        List<FontMonitorResourceError> list = fontMonitorResourceErrorService.selectFontMonitorResourceErrorList(fontMonitorResourceError);
        ExcelUtil<FontMonitorResourceError> util = new ExcelUtil<FontMonitorResourceError>(FontMonitorResourceError.class);
        util.exportExcel(response, list, "监控 resourceError数据");
    }

    /**
     * 获取监控 resourceError详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorResourceError:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorResourceError:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorResourceErrorService.selectFontMonitorResourceErrorById(id));
    }

    /**
     * 新增监控 resourceError
     */
    //@RequiresPermissions("fontMonitor:fontMonitorResourceError:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorResourceError:add')")
    @Log(title = "监控 resourceError", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorResourceError fontMonitorResourceError) {
        return toAjax(fontMonitorResourceErrorService.insertFontMonitorResourceError(fontMonitorResourceError));
    }

    /**
     * 修改监控 resourceError
     */
    //@RequiresPermissions("fontMonitor:fontMonitorResourceError:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorResourceError:edit')")
    @Log(title = "监控 resourceError", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorResourceError fontMonitorResourceError) {
        return toAjax(fontMonitorResourceErrorService.updateFontMonitorResourceError(fontMonitorResourceError));
    }

    /**
     * 删除监控 resourceError
     */
    //@RequiresPermissions("fontMonitor:fontMonitorResourceError:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorResourceError:remove')")
    @Log(title = "监控 resourceError", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorResourceErrorService.deleteFontMonitorResourceErrorByIds(ids));
    }
}
