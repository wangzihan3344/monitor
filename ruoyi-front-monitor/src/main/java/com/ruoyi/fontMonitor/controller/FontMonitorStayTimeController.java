package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorStayTime;
import com.ruoyi.fontMonitor.service.IFontMonitorStayTimeService;

/**
 * 记录停留时间Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorStayTime")
public class FontMonitorStayTimeController extends BaseController {
    @Autowired
    private IFontMonitorStayTimeService fontMonitorStayTimeService;

    /**
     * 查询记录停留时间列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorStayTime:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorStayTime:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorStayTime fontMonitorStayTime) {
        startPage();
        List<FontMonitorStayTime> list = fontMonitorStayTimeService.selectFontMonitorStayTimeList(fontMonitorStayTime);
        return getDataTable(list);
    }

    /**
     * 导出记录停留时间列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorStayTime:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorStayTime:export')")
    @Log(title = "记录停留时间", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorStayTime fontMonitorStayTime) {
        List<FontMonitorStayTime> list = fontMonitorStayTimeService.selectFontMonitorStayTimeList(fontMonitorStayTime);
        ExcelUtil<FontMonitorStayTime> util = new ExcelUtil<FontMonitorStayTime>(FontMonitorStayTime.class);
        util.exportExcel(response, list, "记录停留时间数据");
    }

    /**
     * 获取记录停留时间详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorStayTime:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorStayTime:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorStayTimeService.selectFontMonitorStayTimeById(id));
    }

    /**
     * 新增记录停留时间
     */
    //@RequiresPermissions("fontMonitor:fontMonitorStayTime:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorStayTime:add')")
    @Log(title = "记录停留时间", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorStayTime fontMonitorStayTime) {
        return toAjax(fontMonitorStayTimeService.insertFontMonitorStayTime(fontMonitorStayTime));
    }

    /**
     * 修改记录停留时间
     */
    //@RequiresPermissions("fontMonitor:fontMonitorStayTime:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorStayTime:edit')")
    @Log(title = "记录停留时间", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorStayTime fontMonitorStayTime) {
        return toAjax(fontMonitorStayTimeService.updateFontMonitorStayTime(fontMonitorStayTime));
    }

    /**
     * 删除记录停留时间
     */
    //@RequiresPermissions("fontMonitor:fontMonitorStayTime:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorStayTime:remove')")
    @Log(title = "记录停留时间", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorStayTimeService.deleteFontMonitorStayTimeByIds(ids));
    }
}
