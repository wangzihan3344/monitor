package com.ruoyi.fontMonitor.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorUserConnectTeam;
import com.ruoyi.fontMonitor.service.IFontMonitorUserConnectTeamService;

/**
 * 用户和团队的关系Controller
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorUserConnectTeam")
public class FontMonitorUserConnectTeamController extends BaseController
{
    @Autowired
    private IFontMonitorUserConnectTeamService fontMonitorUserConnectTeamService;

    /**
     * 查询用户和团队的关系列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorUserConnectTeam:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorUserConnectTeam:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorUserConnectTeam fontMonitorUserConnectTeam)
    {
        startPage();
        List<FontMonitorUserConnectTeam> list = fontMonitorUserConnectTeamService.selectFontMonitorUserConnectTeamList(fontMonitorUserConnectTeam);
        return getDataTable(list);
    }

    /**
     * 导出用户和团队的关系列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorUserConnectTeam:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorUserConnectTeam:export')")
    @Log(title = "用户和团队的关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorUserConnectTeam fontMonitorUserConnectTeam)
    {
        List<FontMonitorUserConnectTeam> list = fontMonitorUserConnectTeamService.selectFontMonitorUserConnectTeamList(fontMonitorUserConnectTeam);
        ExcelUtil<FontMonitorUserConnectTeam> util = new ExcelUtil<FontMonitorUserConnectTeam>(FontMonitorUserConnectTeam.class);
        util.exportExcel(response, list, "用户和团队的关系数据");
    }

    /**
     * 获取用户和团队的关系详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorUserConnectTeam:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorUserConnectTeam:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fontMonitorUserConnectTeamService.selectFontMonitorUserConnectTeamById(id));
    }

    /**
     * 新增用户和团队的关系
     */
    //@RequiresPermissions("fontMonitor:fontMonitorUserConnectTeam:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorUserConnectTeam:add')")
    @Log(title = "用户和团队的关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorUserConnectTeam fontMonitorUserConnectTeam)
    {
        return toAjax(fontMonitorUserConnectTeamService.insertFontMonitorUserConnectTeam(fontMonitorUserConnectTeam));
    }

    /**
     * 修改用户和团队的关系
     */
    //@RequiresPermissions("fontMonitor:fontMonitorUserConnectTeam:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorUserConnectTeam:edit')")
    @Log(title = "用户和团队的关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorUserConnectTeam fontMonitorUserConnectTeam)
    {
        return toAjax(fontMonitorUserConnectTeamService.updateFontMonitorUserConnectTeam(fontMonitorUserConnectTeam));
    }

    /**
     * 删除用户和团队的关系
     */
    //@RequiresPermissions("fontMonitor:fontMonitorUserConnectTeam:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorUserConnectTeam:remove')")
    @Log(title = "用户和团队的关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fontMonitorUserConnectTeamService.deleteFontMonitorUserConnectTeamByIds(ids));
    }
}
