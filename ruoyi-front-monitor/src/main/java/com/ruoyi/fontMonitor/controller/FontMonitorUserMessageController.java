package com.ruoyi.fontMonitor.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorUserMessage;
import com.ruoyi.fontMonitor.service.IFontMonitorUserMessageService;

/**
 * 用户信息Controller
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorMessage")
public class FontMonitorUserMessageController extends BaseController
{
    @Autowired
    private IFontMonitorUserMessageService fontMonitorUserMessageService;

    /**
     * 查询用户信息列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorMessage:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorMessage:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorUserMessage fontMonitorUserMessage)
    {
        startPage();
        List<FontMonitorUserMessage> list = fontMonitorUserMessageService.selectFontMonitorUserMessageList(fontMonitorUserMessage);
        return getDataTable(list);
    }

    /**
     * 导出用户信息列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorMessage:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorMessage:export')")
    @Log(title = "用户信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorUserMessage fontMonitorUserMessage)
    {
        List<FontMonitorUserMessage> list = fontMonitorUserMessageService.selectFontMonitorUserMessageList(fontMonitorUserMessage);
        ExcelUtil<FontMonitorUserMessage> util = new ExcelUtil<FontMonitorUserMessage>(FontMonitorUserMessage.class);
        util.exportExcel(response, list, "用户信息数据");
    }

    /**
     * 获取用户信息详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorMessage:query")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorMessage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fontMonitorUserMessageService.selectFontMonitorUserMessageById(id));
    }

    /**
     * 新增用户信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorMessage:add")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorMessage:add')")
    @Log(title = "用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorUserMessage fontMonitorUserMessage)
    {
        return toAjax(fontMonitorUserMessageService.insertFontMonitorUserMessage(fontMonitorUserMessage));
    }

    /**
     * 修改用户信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorMessage:edit")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorMessage:edit')")
    @Log(title = "用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorUserMessage fontMonitorUserMessage)
    {
        return toAjax(fontMonitorUserMessageService.updateFontMonitorUserMessage(fontMonitorUserMessage));
    }

    /**
     * 删除用户信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorMessage:remove")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorMessage:remove')")
    @Log(title = "用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fontMonitorUserMessageService.deleteFontMonitorUserMessageByIds(ids));
    }
}
