package com.ruoyi.fontMonitor.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorUserTeam;
import com.ruoyi.fontMonitor.service.IFontMonitorUserTeamService;

/**
 * 团队信息Controller
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorUserTeam")
public class FontMonitorUserTeamController extends BaseController
{
    @Autowired
    private IFontMonitorUserTeamService fontMonitorUserTeamService;

    /**
     * 查询团队信息列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorUserTeam:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorUserTeam:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorUserTeam fontMonitorUserTeam)
    {
        startPage();
        List<FontMonitorUserTeam> list = fontMonitorUserTeamService.selectFontMonitorUserTeamList(fontMonitorUserTeam);
        return getDataTable(list);
    }

    /**
     * 导出团队信息列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorUserTeam:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorUserTeam:export')")
    @Log(title = "团队信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorUserTeam fontMonitorUserTeam)
    {
        List<FontMonitorUserTeam> list = fontMonitorUserTeamService.selectFontMonitorUserTeamList(fontMonitorUserTeam);
        ExcelUtil<FontMonitorUserTeam> util = new ExcelUtil<FontMonitorUserTeam>(FontMonitorUserTeam.class);
        util.exportExcel(response, list, "团队信息数据");
    }

    /**
     * 获取团队信息详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorUserTeam:query")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorUserTeam:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fontMonitorUserTeamService.selectFontMonitorUserTeamById(id));
    }

    /**
     * 新增团队信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorUserTeam:add")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorUserTeam:add')")
    @Log(title = "团队信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorUserTeam fontMonitorUserTeam)
    {
        return toAjax(fontMonitorUserTeamService.insertFontMonitorUserTeam(fontMonitorUserTeam));
    }

    /**
     * 修改团队信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorUserTeam:edit")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorUserTeam:edit')")
    @Log(title = "团队信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorUserTeam fontMonitorUserTeam)
    {
        return toAjax(fontMonitorUserTeamService.updateFontMonitorUserTeam(fontMonitorUserTeam));
    }

    /**
     * 删除团队信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorUserTeam:remove")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorUserTeam:remove')")
    @Log(title = "团队信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fontMonitorUserTeamService.deleteFontMonitorUserTeamByIds(ids));
    }
}
