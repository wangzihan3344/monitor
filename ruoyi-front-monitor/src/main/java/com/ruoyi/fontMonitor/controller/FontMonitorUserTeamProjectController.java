package com.ruoyi.fontMonitor.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorUserTeamProject;
import com.ruoyi.fontMonitor.service.IFontMonitorUserTeamProjectService;

/**
 * 团队负责的项目Controller
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorProject")
public class FontMonitorUserTeamProjectController extends BaseController
{
    @Autowired
    private IFontMonitorUserTeamProjectService fontMonitorUserTeamProjectService;

    /**
     * 查询团队负责的项目列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorProject:list")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorProject:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorUserTeamProject fontMonitorUserTeamProject)
    {
        startPage();
        List<FontMonitorUserTeamProject> list = fontMonitorUserTeamProjectService.selectFontMonitorUserTeamProjectList(fontMonitorUserTeamProject);
        return getDataTable(list);
    }

    /**
     * 导出团队负责的项目列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorProject:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorProject:export')")
    @Log(title = "团队负责的项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorUserTeamProject fontMonitorUserTeamProject)
    {
        List<FontMonitorUserTeamProject> list = fontMonitorUserTeamProjectService.selectFontMonitorUserTeamProjectList(fontMonitorUserTeamProject);
        ExcelUtil<FontMonitorUserTeamProject> util = new ExcelUtil<FontMonitorUserTeamProject>(FontMonitorUserTeamProject.class);
        util.exportExcel(response, list, "团队负责的项目数据");
    }

    /**
     * 获取团队负责的项目详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorProject:query")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorProject:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fontMonitorUserTeamProjectService.selectFontMonitorUserTeamProjectById(id));
    }

    /**
     * 新增团队负责的项目
     */
    //@RequiresPermissions("fontMonitor:fontMonitorProject:add")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorProject:add')")
    @Log(title = "团队负责的项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorUserTeamProject fontMonitorUserTeamProject)
    {
        return toAjax(fontMonitorUserTeamProjectService.insertFontMonitorUserTeamProject(fontMonitorUserTeamProject));
    }

    /**
     * 修改团队负责的项目
     */
    //@RequiresPermissions("fontMonitor:fontMonitorProject:edit")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorProject:edit')")
    @Log(title = "团队负责的项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorUserTeamProject fontMonitorUserTeamProject)
    {
        return toAjax(fontMonitorUserTeamProjectService.updateFontMonitorUserTeamProject(fontMonitorUserTeamProject));
    }

    /**
     * 删除团队负责的项目
     */
    //@RequiresPermissions("fontMonitor:fontMonitorProject:remove")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorProject:remove')")
    @Log(title = "团队负责的项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fontMonitorUserTeamProjectService.deleteFontMonitorUserTeamProjectByIds(ids));
    }
}
