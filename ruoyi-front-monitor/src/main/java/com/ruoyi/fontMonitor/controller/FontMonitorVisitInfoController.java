package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorVisitInfo;
import com.ruoyi.fontMonitor.service.IFontMonitorVisitInfoService;

/**
 * 记录访问信息Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorVisitInfo")
public class FontMonitorVisitInfoController extends BaseController {
    @Autowired
    private IFontMonitorVisitInfoService fontMonitorVisitInfoService;

    /**
     * 查询记录访问信息列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorVisitInfo:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorVisitInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorVisitInfo fontMonitorVisitInfo) {
        startPage();
        List<FontMonitorVisitInfo> list = fontMonitorVisitInfoService.selectFontMonitorVisitInfoList(fontMonitorVisitInfo);
        return getDataTable(list);
    }

    /**
     * 导出记录访问信息列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorVisitInfo:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorVisitInfo:export')")
    @Log(title = "记录访问信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorVisitInfo fontMonitorVisitInfo) {
        List<FontMonitorVisitInfo> list = fontMonitorVisitInfoService.selectFontMonitorVisitInfoList(fontMonitorVisitInfo);
        ExcelUtil<FontMonitorVisitInfo> util = new ExcelUtil<FontMonitorVisitInfo>(FontMonitorVisitInfo.class);
        util.exportExcel(response, list, "记录访问信息数据");
    }

    /**
     * 获取记录访问信息详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorVisitInfo:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorVisitInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorVisitInfoService.selectFontMonitorVisitInfoById(id));
    }

    /**
     * 新增记录访问信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorVisitInfo:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorVisitInfo:add')")
    @Log(title = "记录访问信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorVisitInfo fontMonitorVisitInfo) {
        return toAjax(fontMonitorVisitInfoService.insertFontMonitorVisitInfo(fontMonitorVisitInfo));
    }

    /**
     * 修改记录访问信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorVisitInfo:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorVisitInfo:edit')")
    @Log(title = "记录访问信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorVisitInfo fontMonitorVisitInfo) {
        return toAjax(fontMonitorVisitInfoService.updateFontMonitorVisitInfo(fontMonitorVisitInfo));
    }

    /**
     * 删除记录访问信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorVisitInfo:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorVisitInfo:remove')")
    @Log(title = "记录访问信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorVisitInfoService.deleteFontMonitorVisitInfoByIds(ids));
    }
}
