package com.ruoyi.fontMonitor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.fontMonitor.domain.FontMonitorXhrInfo;
import com.ruoyi.fontMonitor.service.IFontMonitorXhrInfoService;

/**
 * 监控 xhr 接口Controller
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@RestController
@SuppressWarnings("all")
@RequestMapping("/fontMonitor/fontMonitorXhrInfo")
public class FontMonitorXhrInfoController extends BaseController {
    @Autowired
    private IFontMonitorXhrInfoService fontMonitorXhrInfoService;

    /**
     * 查询监控 xhr 接口列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorXhrInfo:list")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorXhrInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(FontMonitorXhrInfo fontMonitorXhrInfo) {
        startPage();
        List<FontMonitorXhrInfo> list = fontMonitorXhrInfoService.selectFontMonitorXhrInfoList(fontMonitorXhrInfo);
        return getDataTable(list);
    }

    /**
     * 导出监控 xhr 接口列表
     */
    //@RequiresPermissions("fontMonitor:fontMonitorXhrInfo:export")
    @PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorXhrInfo:export')")
    @Log(title = "监控 xhr 接口", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FontMonitorXhrInfo fontMonitorXhrInfo) {
        List<FontMonitorXhrInfo> list = fontMonitorXhrInfoService.selectFontMonitorXhrInfoList(fontMonitorXhrInfo);
        ExcelUtil<FontMonitorXhrInfo> util = new ExcelUtil<FontMonitorXhrInfo>(FontMonitorXhrInfo.class);
        util.exportExcel(response, list, "监控 xhr 接口数据");
    }

    /**
     * 获取监控 xhr 接口详细信息
     */
    //@RequiresPermissions("fontMonitor:fontMonitorXhrInfo:query")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorXhrInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fontMonitorXhrInfoService.selectFontMonitorXhrInfoById(id));
    }

    /**
     * 新增监控 xhr 接口
     */
    //@RequiresPermissions("fontMonitor:fontMonitorXhrInfo:add")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorXhrInfo:add')")
    @Log(title = "监控 xhr 接口", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FontMonitorXhrInfo fontMonitorXhrInfo) {
        return toAjax(fontMonitorXhrInfoService.insertFontMonitorXhrInfo(fontMonitorXhrInfo));
    }

    /**
     * 修改监控 xhr 接口
     */
    //@RequiresPermissions("fontMonitor:fontMonitorXhrInfo:edit")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorXhrInfo:edit')")
    @Log(title = "监控 xhr 接口", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FontMonitorXhrInfo fontMonitorXhrInfo) {
        return toAjax(fontMonitorXhrInfoService.updateFontMonitorXhrInfo(fontMonitorXhrInfo));
    }

    /**
     * 删除监控 xhr 接口
     */
    //@RequiresPermissions("fontMonitor:fontMonitorXhrInfo:remove")
    //@PreAuthorize("@ss.hasPermi('fontMonitor:fontMonitorXhrInfo:remove')")
    @Log(title = "监控 xhr 接口", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fontMonitorXhrInfoService.deleteFontMonitorXhrInfoByIds(ids));
    }
}
