package com.ruoyi.fontMonitor.controller;


import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.fontMonitor.service.IToolReadLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SuppressWarnings("all")
@RestController
@RequestMapping("/readLog")
@EnableScheduling
public class ToolReadLogController {
    @Autowired
    private IToolReadLogService toolReadLogService;

    @GetMapping("/readLogTest")
    public AjaxResult readLogTest() {
        ArrayList logs = toolReadLogService.readLog(null);
        ArrayList requirementsLogs = toolReadLogService.screenLogs(logs);
        ArrayList jsonLogs = toolReadLogService.logsStringConversionJSON(requirementsLogs);
        toolReadLogService.dataRecordedInDatabase(jsonLogs);
        return AjaxResult.success();
    }

    @Scheduled(cron = "0 10 * * * *")
    public void readLogTestScheduled() {
        ArrayList logs = toolReadLogService.readLog(toolReadLogService.getLogsName());
        ArrayList requirementsLogs = toolReadLogService.screenLogs(logs);
        ArrayList jsonLogs = toolReadLogService.logsStringConversionJSON(requirementsLogs);
        toolReadLogService.dataRecordedInDatabase(jsonLogs);
        return;
    }
}
