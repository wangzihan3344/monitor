package com.ruoyi.fontMonitor.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@SuppressWarnings("all")
public class EnhanceDataStatistics {
    /**
     * 时间
     */
    private String timeType;

    /**
     * 数量
     */
    private Long dataNumber;
}
