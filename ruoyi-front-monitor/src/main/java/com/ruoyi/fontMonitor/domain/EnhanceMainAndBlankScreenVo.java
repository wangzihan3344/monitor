package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class EnhanceMainAndBlankScreenVo extends FontMonitorMain{
    /** 空白点集 */
    @Excel(name = "空白点集")
    private String emptyPoints;

    /** 屏幕分辨率 */
    @Excel(name = "屏幕分辨率")
    private String screen;

    /** 屏幕分辨率 */
    @Excel(name = "屏幕分辨率")
    private String viewPoint;

    /** 选择器 */
    @Excel(name = "选择器")
    private String selector;
}
