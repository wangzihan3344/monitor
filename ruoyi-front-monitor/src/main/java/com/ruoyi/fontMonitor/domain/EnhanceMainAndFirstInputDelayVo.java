package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class EnhanceMainAndFirstInputDelayVo extends FontMonitorMain {
    /** 延迟时间 */
    @Excel(name = "延迟时间")
    private String inputDelay;

    /** 处理的耗时, */
    @Excel(name = "处理的耗时,")
    private String duration;

    /** 开始处理的事件 */
    @Excel(name = "开始处理的事件")
    private String startTime;

    /** 选择器 */
    @Excel(name = "选择器")
    private String selector;
}
