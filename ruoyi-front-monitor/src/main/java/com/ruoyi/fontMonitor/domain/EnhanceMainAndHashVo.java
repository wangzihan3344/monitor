package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class EnhanceMainAndHashVo extends FontMonitorMain {
    /**
     * 旧地址
     */
    @Excel(name = "旧地址")
    private String oldUrl;

    /**
     * 新地址
     */
    @Excel(name = "新地址")
    private String newUrl;
}
