package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class EnhanceMainAndHistoryVo extends FontMonitorMain {
    /** 当前路由 */
    @Excel(name = "当前路由")
    private String current;

    /** 向后路由 */
    @Excel(name = "向后路由")
    private String back;

    /** 向前路由 */
    @Excel(name = "向前路由")
    private String forward;
}
