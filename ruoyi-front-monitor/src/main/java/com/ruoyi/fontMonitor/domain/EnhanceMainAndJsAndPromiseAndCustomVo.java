package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class EnhanceMainAndJsAndPromiseAndCustomVo extends FontMonitorMain{
    /** 错误描述 */
    @Excel(name = "错误描述")
    private String message;

    /** 发生错误的文件 */
    @Excel(name = "发生错误的文件")
    private String fileName;

    /** 发生错误的行列信息 */
    @Excel(name = "发生错误的行列信息")
    private String position;

    /** 堆栈信息 */
    @Excel(name = "堆栈信息")
    private String stack;

    /** 选择器 */
    @Excel(name = "选择器")
    private String selector;
}
