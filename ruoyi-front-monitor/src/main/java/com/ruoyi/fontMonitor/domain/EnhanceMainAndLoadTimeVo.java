package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 主表和时间加载表联查，以主表为主，时间加载表数据填充
 */
@Data
@EqualsAndHashCode
public class EnhanceMainAndLoadTimeVo extends FontMonitorMain{
    /** TCP 连接耗时 */
    @Excel(name = "TCP 连接耗时")
    private String connectTime;

    /** TCP 连接耗时 */
    @Excel(name = "TCP 连接耗时")
    private String ttfbTime;

    /** 响应时间 */
    @Excel(name = "响应时间")
    private String responseTime;

    /** DOM 解析渲染耗时 */
    @Excel(name = "DOM 解析渲染耗时")
    private String parseDomTime;

    /** DOMContentLoaded 回调耗时 */
    @Excel(name = "DOMContentLoaded 回调耗时")
    private String domContentLoadedTime;

    /** 首次交互耗时 */
    @Excel(name = "首次交互耗时")
    private String timeToInteractive;

    /** 完整的加载时间,注意，为了方便比较，这里改成了Double */
    @Excel(name = "完整的加载时间")
    private Double loadTime;
}
