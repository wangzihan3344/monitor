package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class EnhanceMainAndLongTaskVo extends FontMonitorMain {
    /** 时间类型 */
    @Excel(name = "时间类型")
    private String eventType;

    /** 开始时间 */
    @Excel(name = "开始时间")
    private String startTime;

    /** 处理的耗时, */
    @Excel(name = "处理的耗时,")
    private String duration;

    /** 选择器 */
    @Excel(name = "选择器")
    private String selector;
}
