package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class EnhanceMainAndPaintTimeVo extends FontMonitorMain{
    /** 请求页面到开始渲染耗时 */
    @Excel(name = "请求页面到开始渲染耗时")
    private String firstPaint;

    /** 请求页面到渲染首页完整内容耗时 */
    @Excel(name = "请求页面到渲染首页完整内容耗时")
    private String firstContentPaint;

    /** 请求页面到渲染绘制有意义内容的时间 */
    @Excel(name = "请求页面到渲染绘制有意义内容的时间")
    private String firstMeaningfulPaint;

    /** 请求页面到开始渲染网页可视区内最大的元素时间 */
    @Excel(name = "请求页面到开始渲染网页可视区内最大的元素时间")
    private String largestContentfulPaint;
}
