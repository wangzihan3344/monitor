package com.ruoyi.fontMonitor.domain;

import lombok.Data;

@Data
public class EnhanceMainAndPvVo {
    private Long mainId;

    private String url;

    private String resourceUrl;
}
