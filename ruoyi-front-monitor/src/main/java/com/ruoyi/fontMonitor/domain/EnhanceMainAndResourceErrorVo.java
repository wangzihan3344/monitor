package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class EnhanceMainAndResourceErrorVo extends FontMonitorMain {
    /** 请求错误的文件 */
    @Excel(name = "请求错误的文件")
    private String filename;

    /** 发生错误的元素名称 */
    @Excel(name = "发生错误的元素名称")
    private String tagName;

    /** 触发时间 */
    @Excel(name = "触发时间")
    private String triggerTimestamp;

    /** 发生错误最后操作的元素名称 */
    @Excel(name = "发生错误最后操作的元素名称")
    private String selector;
}
