package com.ruoyi.fontMonitor.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class EnhanceMainAndResourceVo extends FontMonitorMain {
    /** 资源名称(链接) */
    @Excel(name = "资源名称(链接)")
    private String name;

    /** 请求耗时 */
    @Excel(name = "请求耗时")
    private Double duration;

    /** 资源大小 */
    @Excel(name = "资源大小")
    private String encodedBodySize;

    /** 域名解析时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "域名解析时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Double parseDnsTime;

    /** 连接建立时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "连接建立时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Double connectTime;

    /** 首字节到达时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "首字节到达时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Double ttfbTime;

    /** 响应耗时 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "响应耗时", width = 30, dateFormat = "yyyy-MM-dd")
    private Double responseTime;
}
