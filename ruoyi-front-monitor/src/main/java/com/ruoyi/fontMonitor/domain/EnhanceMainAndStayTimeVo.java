package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class EnhanceMainAndStayTimeVo extends FontMonitorMain{
    /** 停留事件 */
    @Excel(name = "停留事件")
    private Long stayTime;
}
