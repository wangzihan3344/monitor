package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class EnhanceMainAndVisitInfoVo extends FontMonitorMain{
    /**
     * 网络类型
     */
    @Excel(name = "网络类型")
    private String effectiveType;

    /**
     * 网络延迟
     */
    @Excel(name = "网络延迟")
    private String rtt;

    /**
     * 可视区域尺寸
     */
    @Excel(name = "可视区域尺寸")
    private String screen;

    /**
     * 来源网站
     */
    @Excel(name = "来源网站")
    private String sourceUrl;

    /**
     * 是否为外部网站
     */
    private Boolean externalWebsiteFlag;
}
