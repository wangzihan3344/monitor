package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class EnhanceMainAndXhrAndFetchVo extends FontMonitorMain{

    /** 路径 */
    @Excel(name = "地址")
    private String address;


    /** 路径 */
    @Excel(name = "路径")
    private String pathName;

    /** 状态码 */
    @Excel(name = "状态码")
    private String status;

    /** 持续事件,注意：为了方便比较，String改为Double */
    @Excel(name = "持续事件")
    private Double duration;

    /** 请求参数 */
    @Excel(name = "请求参数")
    private String requestParams;

    /** 事件类型 */
    @Excel(name = "事件类型")
    private String eventType;

    /** 响应内容 */
    @Excel(name = "响应内容")
    private String responseData;

}
