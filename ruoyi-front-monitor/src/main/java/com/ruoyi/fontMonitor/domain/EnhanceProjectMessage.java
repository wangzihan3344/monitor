package com.ruoyi.fontMonitor.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class EnhanceProjectMessage extends FontMonitorMain{
    /**
     * 开始时间
     */
    private String preDate;

    /**
     * 结束时间
     */
    private String endDate;

    /**
     * 归类字段
     */
    private String groupBy;
}
