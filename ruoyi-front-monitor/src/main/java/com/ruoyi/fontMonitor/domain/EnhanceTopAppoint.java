package com.ruoyi.fontMonitor.domain;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class EnhanceTopAppoint {
    /** 次数 */
    private Integer number;
    /** 指定内容 */
    private String appointName;
}
