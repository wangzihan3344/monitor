package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 监控白屏对象 font_monitor_blank_screen
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorBlankScreen extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增的唯一标识 */
    private Long id;

    /** 主表id */
    @Excel(name = "主表id")
    private String mainId;

    /** 空白点集 */
    @Excel(name = "空白点集")
    private String emptyPoints;

    /** 屏幕分辨率 */
    @Excel(name = "屏幕分辨率")
    private String screen;

    /** 屏幕分辨率 */
    @Excel(name = "屏幕分辨率")
    private String viewPoint;

    /** 选择器 */
    @Excel(name = "选择器")
    private String selector;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMainId(String mainId) 
    {
        this.mainId = mainId;
    }

    public String getMainId() 
    {
        return mainId;
    }
    public void setEmptyPoints(String emptyPoints) 
    {
        this.emptyPoints = emptyPoints;
    }

    public String getEmptyPoints() 
    {
        return emptyPoints;
    }
    public void setScreen(String screen) 
    {
        this.screen = screen;
    }

    public String getScreen() 
    {
        return screen;
    }
    public void setViewPoint(String viewPoint) 
    {
        this.viewPoint = viewPoint;
    }

    public String getViewPoint() 
    {
        return viewPoint;
    }
    public void setSelector(String selector) 
    {
        this.selector = selector;
    }

    public String getSelector() 
    {
        return selector;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mainId", getMainId())
            .append("emptyPoints", getEmptyPoints())
            .append("screen", getScreen())
            .append("viewPoint", getViewPoint())
            .append("selector", getSelector())
            .toString();
    }
}
