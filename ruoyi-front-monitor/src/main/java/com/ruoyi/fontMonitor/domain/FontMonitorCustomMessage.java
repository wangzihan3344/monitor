package com.ruoyi.fontMonitor.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * custom_error 的 message 数据对象 font_monitor_custom_message
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorCustomMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增的唯一标识 */
    private Long id;

    /** 主表id */
    @Excel(name = "主表id")
    private String mainId;

    /** 消息内容（数组形式） */
    @Excel(name = "消息内容", readConverterExp = "数=组形式")
    private String message;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMainId(String mainId) 
    {
        this.mainId = mainId;
    }

    public String getMainId() 
    {
        return mainId;
    }
    public void setMessage(String message) 
    {
        this.message = message;
    }

    public String getMessage() 
    {
        return message;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mainId", getMainId())
            .append("message", getMessage())
            .toString();
    }
}
