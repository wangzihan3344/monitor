package com.ruoyi.fontMonitor.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 监控 fetch 接口对象 font_monitor_fetch_info
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorFetchInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增的唯一标识 */
    private Long id;

    /** 主表id */
    @Excel(name = "主表id")
    private String mainId;

    /** 路径 */
    @Excel(name = "路径")
    private String pathName;

    /** 状态码 */
    @Excel(name = "状态码")
    private String status;

    /** 持续事件 */
    @Excel(name = "持续事件")
    private String duration;

    /** 请求参数 */
    @Excel(name = "请求参数")
    private String requestParams;

    /** 响应数据 */
    @Excel(name = "响应数据")
    private String responseData;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMainId(String mainId) 
    {
        this.mainId = mainId;
    }

    public String getMainId() 
    {
        return mainId;
    }
    public void setPathName(String pathName) 
    {
        this.pathName = pathName;
    }

    public String getPathName() 
    {
        return pathName;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDuration(String duration) 
    {
        this.duration = duration;
    }

    public String getDuration() 
    {
        return duration;
    }
    public void setRequestParams(String requestParams) 
    {
        this.requestParams = requestParams;
    }

    public String getRequestParams() 
    {
        return requestParams;
    }
    public void setResponseData(String responseData) 
    {
        this.responseData = responseData;
    }

    public String getResponseData() 
    {
        return responseData;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mainId", getMainId())
            .append("pathName", getPathName())
            .append("status", getStatus())
            .append("duration", getDuration())
            .append("requestParams", getRequestParams())
            .append("responseData", getResponseData())
            .toString();
    }
}
