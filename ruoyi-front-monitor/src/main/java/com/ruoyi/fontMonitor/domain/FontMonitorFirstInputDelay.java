package com.ruoyi.fontMonitor.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 监控首次输入响应延迟对象 font_monitor_first_input_delay
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorFirstInputDelay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增的唯一标识 */
    private Long id;

    /** 主表id */
    @Excel(name = "主表id")
    private String mainId;

    /** 延迟时间 */
    @Excel(name = "延迟时间")
    private String inputDelay;

    /** 处理的耗时, */
    @Excel(name = "处理的耗时,")
    private String duration;

    /** 开始处理的事件 */
    @Excel(name = "开始处理的事件")
    private String startTime;

    /** 选择器 */
    @Excel(name = "选择器")
    private String selector;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMainId(String mainId) 
    {
        this.mainId = mainId;
    }

    public String getMainId() 
    {
        return mainId;
    }
    public void setInputDelay(String inputDelay) 
    {
        this.inputDelay = inputDelay;
    }

    public String getInputDelay() 
    {
        return inputDelay;
    }
    public void setDuration(String duration) 
    {
        this.duration = duration;
    }

    public String getDuration() 
    {
        return duration;
    }
    public void setStartTime(String startTime) 
    {
        this.startTime = startTime;
    }

    public String getStartTime() 
    {
        return startTime;
    }
    public void setSelector(String selector) 
    {
        this.selector = selector;
    }

    public String getSelector() 
    {
        return selector;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mainId", getMainId())
            .append("inputDelay", getInputDelay())
            .append("duration", getDuration())
            .append("startTime", getStartTime())
            .append("selector", getSelector())
            .toString();
    }
}
