package com.ruoyi.fontMonitor.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * url变动对象 font_monitor_hash
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorHash extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 主表id */
    @Excel(name = "主表id")
    private String mianId;

    /** 旧地址 */
    @Excel(name = "旧地址")
    private String oldUrl;

    /** 新地址 */
    @Excel(name = "新地址")
    private String newUrl;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setMianId(String mianId)
    {
        this.mianId = mianId;
    }

    public String getMianId()
    {
        return mianId;
    }
    public void setOldUrl(String oldUrl) 
    {
        this.oldUrl = oldUrl;
    }

    public String getOldUrl() 
    {
        return oldUrl;
    }
    public void setNewUrl(String newUrl) 
    {
        this.newUrl = newUrl;
    }

    public String getNewUrl() 
    {
        return newUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mianId", getMianId())
            .append("oldUrl", getOldUrl())
            .append("newUrl", getNewUrl())
            .toString();
    }
}
