package com.ruoyi.fontMonitor.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 路由记录对象 font_monitor_history
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FontMonitorHistory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 主表id */
    @Excel(name = "主表id")
    private String mianId;

    /** 当前路由 */
    @Excel(name = "当前路由")
    private String current;

    /** 向后路由 */
    @Excel(name = "向后路由")
    private String back;

    /** 向前路由 */
    @Excel(name = "向前路由")
    private String forward;
}
