package com.ruoyi.fontMonitor.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 监控 jsError对象 font_monitor_js_error
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorJsError extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增的唯一标识 */
    private Long id;

    /** 主表id */
    @Excel(name = "主表id")
    private String mainId;

    /** 错误描述 */
    @Excel(name = "错误描述")
    private String message;

    /** 发生错误的文件 */
    @Excel(name = "发生错误的文件")
    private String fileName;

    /** 发生错误的行列信息 */
    @Excel(name = "发生错误的行列信息")
    private String position;

    /** 堆栈信息 */
    @Excel(name = "堆栈信息")
    private String stack;

    /** 选择器 */
    @Excel(name = "选择器")
    private String selector;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMainId(String mainId) 
    {
        this.mainId = mainId;
    }

    public String getMainId() 
    {
        return mainId;
    }
    public void setMessage(String message) 
    {
        this.message = message;
    }

    public String getMessage() 
    {
        return message;
    }
    public void setFileName(String fileName) 
    {
        this.fileName = fileName;
    }

    public String getFileName() 
    {
        return fileName;
    }
    public void setPosition(String position) 
    {
        this.position = position;
    }

    public String getPosition() 
    {
        return position;
    }
    public void setStack(String stack) 
    {
        this.stack = stack;
    }

    public String getStack() 
    {
        return stack;
    }
    public void setSelector(String selector) 
    {
        this.selector = selector;
    }

    public String getSelector() 
    {
        return selector;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mainId", getMainId())
            .append("message", getMessage())
            .append("fileName", getFileName())
            .append("position", getPosition())
            .append("stack", getStack())
            .append("selector", getSelector())
            .toString();
    }
}
