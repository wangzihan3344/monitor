package com.ruoyi.fontMonitor.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 监控加载时间对象 font_monitor_load_time
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorLoadTime extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增的唯一标识 */
    private Long id;

    /** TCP 连接耗时 */
    @Excel(name = "TCP 连接耗时")
    private String connectTime;

    /** TCP 连接耗时 */
    @Excel(name = "TCP 连接耗时")
    private String ttfbTime;

    /** 响应时间 */
    @Excel(name = "响应时间")
    private String responseTime;

    /** DOM 解析渲染耗时 */
    @Excel(name = "DOM 解析渲染耗时")
    private String parseDomTime;

    /** DOMContentLoaded 回调耗时 */
    @Excel(name = "DOMContentLoaded 回调耗时")
    private String domContentLoadedTime;

    /** 首次交互耗时 */
    @Excel(name = "首次交互耗时")
    private String timeToInteractive;

    /** 完整的加载时间 */
    @Excel(name = "完整的加载时间")
    private String loadTime;

    /** 主表id */
    @Excel(name = "主表id")
    private String mainId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setConnectTime(String connectTime) 
    {
        this.connectTime = connectTime;
    }

    public String getConnectTime() 
    {
        return connectTime;
    }
    public void setTtfbTime(String ttfbTime) 
    {
        this.ttfbTime = ttfbTime;
    }

    public String getTtfbTime() 
    {
        return ttfbTime;
    }
    public void setResponseTime(String responseTime) 
    {
        this.responseTime = responseTime;
    }

    public String getResponseTime() 
    {
        return responseTime;
    }
    public void setParseDomTime(String parseDomTime) 
    {
        this.parseDomTime = parseDomTime;
    }

    public String getParseDomTime() 
    {
        return parseDomTime;
    }
    public void setDomContentLoadedTime(String domContentLoadedTime) 
    {
        this.domContentLoadedTime = domContentLoadedTime;
    }

    public String getDomContentLoadedTime() 
    {
        return domContentLoadedTime;
    }
    public void setTimeToInteractive(String timeToInteractive) 
    {
        this.timeToInteractive = timeToInteractive;
    }

    public String getTimeToInteractive() 
    {
        return timeToInteractive;
    }
    public void setLoadTime(String loadTime) 
    {
        this.loadTime = loadTime;
    }

    public String getLoadTime() 
    {
        return loadTime;
    }
    public void setMainId(String mainId) 
    {
        this.mainId = mainId;
    }

    public String getMainId() 
    {
        return mainId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("connectTime", getConnectTime())
            .append("ttfbTime", getTtfbTime())
            .append("responseTime", getResponseTime())
            .append("parseDomTime", getParseDomTime())
            .append("domContentLoadedTime", getDomContentLoadedTime())
            .append("timeToInteractive", getTimeToInteractive())
            .append("loadTime", getLoadTime())
            .append("mainId", getMainId())
            .toString();
    }
}
