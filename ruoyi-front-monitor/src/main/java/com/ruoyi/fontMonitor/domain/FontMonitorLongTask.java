package com.ruoyi.fontMonitor.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 监控卡顿对象 font_monitor_long_task
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorLongTask extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增的唯一标识 */
    private Long id;

    /** 时间类型 */
    @Excel(name = "时间类型")
    private String eventType;

    /** 开始时间 */
    @Excel(name = "开始时间")
    private String startTime;

    /** 处理的耗时, */
    @Excel(name = "处理的耗时,")
    private String duration;

    /** 选择器 */
    @Excel(name = "选择器")
    private String selector;

    /** 主表id */
    @Excel(name = "主表id")
    private String mianId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setEventType(String eventType) 
    {
        this.eventType = eventType;
    }

    public String getEventType() 
    {
        return eventType;
    }
    public void setStartTime(String startTime) 
    {
        this.startTime = startTime;
    }

    public String getStartTime() 
    {
        return startTime;
    }
    public void setDuration(String duration) 
    {
        this.duration = duration;
    }

    public String getDuration() 
    {
        return duration;
    }
    public void setSelector(String selector) 
    {
        this.selector = selector;
    }

    public String getSelector() 
    {
        return selector;
    }
    public void setMianId(String mianId) 
    {
        this.mianId = mianId;
    }

    public String getMianId() 
    {
        return mianId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("eventType", getEventType())
            .append("startTime", getStartTime())
            .append("duration", getDuration())
            .append("selector", getSelector())
            .append("mianId", getMianId())
            .toString();
    }
}
