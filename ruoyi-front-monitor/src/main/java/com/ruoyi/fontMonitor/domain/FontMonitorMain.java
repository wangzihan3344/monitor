package com.ruoyi.fontMonitor.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 公共字段对象 font_monitor_main
 *
 * @author ruoyi
 * @date 2022-08-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FontMonitorMain extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 主id
     */
    @Excel(name = "主id")
    private String mainId;

    /**
     * 项目id
     */
    @Excel(name = "项目id")
    private String pid;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private String uuid;

    /**
     * 网页标题
     */
    @Excel(name = "网页标题")
    private String title;

    /**
     * 网页地址
     */
    @Excel(name = "网页地址")
    private String url;

    /**
     * 上报时间戳
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上报时间戳", width = 30, dateFormat = "yyyy-MM-dd")
    private Date reportTimestamp;

    /**
     * 浏览器
     */
    @Excel(name = "浏览器")
    private String browser;

    /**
     * 系统
     */
    @Excel(name = "系统")
    private String os;

    /**
     * 请求的ip地址
     */
    @Excel(name = "请求的ip地址")
    private String ip;

    /**
     * 项目环境（'dev'：生产环境；'sit'：测试环境；'stag'：预发布环境；'prod'：生产环境）
     */
    @Excel(name = "项目环境", readConverterExp = "'=dev'：生产环境；'sit'：测试环境；'stag'：预发布环境；'prod'：生产环境")
    private String production;

    /**
     * 监控类型字典id
     */
    @Excel(name = "监控类型字典id")
    private String typeId;

    /**
     * 判断是否为老用户（0为新用户，1为老用户）
     */
    @Excel(name = "判断是否为老用户")
    private Integer oldUserFlag;

    /**
     * 国家
     */
    @Excel(name = "国家")
    private String ipCountry;

    /**
     * 省份
     */
    @Excel(name = "省份")
    private String ipProvince;

    /**
     * 城市
     */
    @Excel(name = "城市")
    private String ipCity;

    /**
     * 第一次上报时间戳
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "第一次上报时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date firstTimestamp;
}
