package com.ruoyi.fontMonitor.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 监控渲染时间对象 font_monitor_paint_time
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorPaintTime extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增的唯一标识 */
    private Long id;

    /** 请求页面到开始渲染耗时 */
    @Excel(name = "请求页面到开始渲染耗时")
    private String firstPaint;

    /** 请求页面到渲染首页完整内容耗时 */
    @Excel(name = "请求页面到渲染首页完整内容耗时")
    private String firstContentPaint;

    /** 请求页面到渲染绘制有意义内容的时间 */
    @Excel(name = "请求页面到渲染绘制有意义内容的时间")
    private String firstMeaningfulPaint;

    /** 请求页面到开始渲染网页可视区内最大的元素时间 */
    @Excel(name = "请求页面到开始渲染网页可视区内最大的元素时间")
    private String largestContentfulPaint;

    /** 主表id */
    @Excel(name = "主表id")
    private String mainId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFirstPaint(String firstPaint) 
    {
        this.firstPaint = firstPaint;
    }

    public String getFirstPaint() 
    {
        return firstPaint;
    }
    public void setFirstContentPaint(String firstContentPaint) 
    {
        this.firstContentPaint = firstContentPaint;
    }

    public String getFirstContentPaint() 
    {
        return firstContentPaint;
    }
    public void setFirstMeaningfulPaint(String firstMeaningfulPaint) 
    {
        this.firstMeaningfulPaint = firstMeaningfulPaint;
    }

    public String getFirstMeaningfulPaint() 
    {
        return firstMeaningfulPaint;
    }
    public void setLargestContentfulPaint(String largestContentfulPaint) 
    {
        this.largestContentfulPaint = largestContentfulPaint;
    }

    public String getLargestContentfulPaint() 
    {
        return largestContentfulPaint;
    }
    public void setMainId(String mainId) 
    {
        this.mainId = mainId;
    }

    public String getMainId() 
    {
        return mainId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("firstPaint", getFirstPaint())
            .append("firstContentPaint", getFirstContentPaint())
            .append("firstMeaningfulPaint", getFirstMeaningfulPaint())
            .append("largestContentfulPaint", getLargestContentfulPaint())
            .append("mainId", getMainId())
            .toString();
    }
}
