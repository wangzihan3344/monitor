package com.ruoyi.fontMonitor.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资源监听对象 font_monitor_resource
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorResource extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增的唯一标识 */
    private Long id;

    /** 资源名称(链接) */
    @Excel(name = "资源名称(链接)")
    private String name;

    /** 请求耗时 */
    @Excel(name = "请求耗时")
    private Double duration;

    /** 资源大小 */
    @Excel(name = "资源大小")
    private String encodedBodySize;

    /** 域名解析时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "域名解析时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Double parseDnsTime;

    /** 连接建立时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "连接建立时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Double connectTime;

    /** 首字节到达时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "首字节到达时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Double ttfbTime;

    /** 响应耗时 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "响应耗时", width = 30, dateFormat = "yyyy-MM-dd")
    private Double responseTime;

    /** 主表id */
    @Excel(name = "主表id")
    private String mainId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDuration(Double duration)
    {
        this.duration = duration;
    }

    public Double getDuration()
    {
        return duration;
    }
    public void setEncodedBodySize(String encodedBodySize) 
    {
        this.encodedBodySize = encodedBodySize;
    }

    public String getEncodedBodySize() 
    {
        return encodedBodySize;
    }
    public void setParseDnsTime(Double parseDnsTime)
    {
        this.parseDnsTime = parseDnsTime;
    }

    public Double getParseDnsTime()
    {
        return parseDnsTime;
    }
    public void setConnectTime(Double connectTime)
    {
        this.connectTime = connectTime;
    }

    public Double getConnectTime()
    {
        return connectTime;
    }
    public void setTtfbTime(Double ttfbTime)
    {
        this.ttfbTime = ttfbTime;
    }

    public Double getTtfbTime()
    {
        return ttfbTime;
    }
    public void setResponseTime(Double responseTime)
    {
        this.responseTime = responseTime;
    }

    public Double getResponseTime()
    {
        return responseTime;
    }
    public void setMainId(String mainId) 
    {
        this.mainId = mainId;
    }

    public String getMainId() 
    {
        return mainId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("duration", getDuration())
            .append("encodedBodySize", getEncodedBodySize())
            .append("parseDnsTime", getParseDnsTime())
            .append("connectTime", getConnectTime())
            .append("ttfbTime", getTtfbTime())
            .append("responseTime", getResponseTime())
            .append("mainId", getMainId())
            .toString();
    }
}
