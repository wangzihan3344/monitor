package com.ruoyi.fontMonitor.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 监控 resourceError对象 font_monitor_resource_error
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorResourceError extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增的唯一标识 */
    private Long id;

    /** 请求错误的文件 */
    @Excel(name = "请求错误的文件")
    private String filename;

    /** 发生错误的元素名称 */
    @Excel(name = "发生错误的元素名称")
    private String tagName;

    /** 触发时间 */
    @Excel(name = "触发时间")
    private String triggerTimestamp;

    /** 发生错误最后操作的元素名称 */
    @Excel(name = "发生错误最后操作的元素名称")
    private String selector;

    /** 主表id */
    @Excel(name = "主表id")
    private String mianId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFilename(String filename) 
    {
        this.filename = filename;
    }

    public String getFilename() 
    {
        return filename;
    }
    public void setTagName(String tagName) 
    {
        this.tagName = tagName;
    }

    public String getTagName() 
    {
        return tagName;
    }
    public void setTriggerTimestamp(String triggerTimestamp) 
    {
        this.triggerTimestamp = triggerTimestamp;
    }

    public String getTriggerTimestamp() 
    {
        return triggerTimestamp;
    }
    public void setSelector(String selector) 
    {
        this.selector = selector;
    }

    public String getSelector() 
    {
        return selector;
    }
    public void setMianId(String mianId) 
    {
        this.mianId = mianId;
    }

    public String getMianId() 
    {
        return mianId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("filename", getFilename())
            .append("tagName", getTagName())
            .append("triggerTimestamp", getTriggerTimestamp())
            .append("selector", getSelector())
            .append("mianId", getMianId())
            .toString();
    }
}
