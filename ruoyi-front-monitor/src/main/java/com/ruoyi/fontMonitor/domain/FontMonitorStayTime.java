package com.ruoyi.fontMonitor.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 记录停留时间对象 font_monitor_stay_time
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorStayTime extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增的唯一标识 */
    private Long id;

    /** 停留事件 */
    @Excel(name = "停留事件")
    private Long stayTime;

    /** 主表id */
    @Excel(name = "主表id")
    private String mainId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStayTime(Long stayTime) 
    {
        this.stayTime = stayTime;
    }

    public Long getStayTime() 
    {
        return stayTime;
    }
    public void setMainId(String mainId) 
    {
        this.mainId = mainId;
    }

    public String getMainId() 
    {
        return mainId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("stayTime", getStayTime())
            .append("mainId", getMainId())
            .toString();
    }
}
