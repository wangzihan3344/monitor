package com.ruoyi.fontMonitor.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户和团队的关系对象 font_monitor_user_connect_team
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
public class FontMonitorUserConnectTeam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** Id */
    private Long id;

    /** 团队Id */
    @Excel(name = "团队Id")
    private Long teamId;

    /** 用户Id */
    @Excel(name = "用户Id")
    private Long userId;

    /** 拥有权限 */
    @Excel(name = "拥有权限")
    private String permission;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTeamId(Long teamId) 
    {
        this.teamId = teamId;
    }

    public Long getTeamId() 
    {
        return teamId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setPermission(String permission) 
    {
        this.permission = permission;
    }

    public String getPermission() 
    {
        return permission;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("teamId", getTeamId())
            .append("userId", getUserId())
            .append("permission", getPermission())
            .append("createTime", getCreateTime())
            .append("status", getStatus())
            .append("remark", getRemark())
            .toString();
    }
}
