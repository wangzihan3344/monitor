package com.ruoyi.fontMonitor.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户信息对象 font_monitor_user_message
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
public class FontMonitorUserMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** Id */
    private Long id;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    private String userName;

    /** 注册邮箱 */
    @Excel(name = "注册邮箱")
    private String email;

    /** 登入密码 */
    @Excel(name = "登入密码")
    private String password;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String phoneNumber;

    /** 账号状态 */
    @Excel(name = "账号状态")
    private String status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setPhoneNumber(String phoneNumber) 
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() 
    {
        return phoneNumber;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userName", getUserName())
            .append("email", getEmail())
            .append("password", getPassword())
            .append("phoneNumber", getPhoneNumber())
            .append("createTime", getCreateTime())
            .append("status", getStatus())
            .toString();
    }
}
