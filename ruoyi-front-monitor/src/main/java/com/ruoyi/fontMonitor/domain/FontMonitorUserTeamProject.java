package com.ruoyi.fontMonitor.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 团队负责的项目对象 font_monitor_user_team_project
 *
 * @author ruoyi
 * @date 2022-10-27
 */
@Data
public class FontMonitorUserTeamProject extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * Id
     */
    private Long id;

    /**
     * 项目名称
     */
    @Excel(name = "项目名称")
    private String projectName;

    /**
     * 项目Id（作为项目的pid，创建项目时自动生成，用于main表）
     */
    @Excel(name = "项目Id", readConverterExp = "先=放着")
    private String projectId;

    /**
     * 所属团队id
     */
    @Excel(name = "所属团队id")
    private Long teamId;

    /**
     * 项目状态
     */
    @Excel(name = "项目状态")
    private String status;

}
