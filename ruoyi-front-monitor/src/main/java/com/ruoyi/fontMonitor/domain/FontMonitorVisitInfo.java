package com.ruoyi.fontMonitor.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 记录访问信息对象 font_monitor_visit_info
 *
 * @author ruoyi
 * @date 2022-08-31
 */
@Data
@EqualsAndHashCode
public class FontMonitorVisitInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 自增的唯一标识
     */
    private Long id;

    /**
     * 网络类型
     */
    @Excel(name = "网络类型")
    private String effectiveType;

    /**
     * 网络延迟
     */
    @Excel(name = "网络延迟")
    private String rtt;

    /**
     * 可视区域尺寸
     */
    @Excel(name = "可视区域尺寸")
    private String screen;

    /**
     * 来源网站
     */
    @Excel(name = "来源网站")
    private String sourceUrl;

    /**
     * 主表id
     */
    @Excel(name = "主表id")
    private String mainId;

    /**
     * 是否为外部网站
     */
    private Boolean externalWebsiteFlag;
}
