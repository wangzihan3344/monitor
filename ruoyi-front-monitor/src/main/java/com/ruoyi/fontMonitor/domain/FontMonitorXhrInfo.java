package com.ruoyi.fontMonitor.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 监控 xhr 接口对象 font_monitor_xhr_info
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public class FontMonitorXhrInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增的唯一标识 */
    private Long id;

    /** 路径 */
    @Excel(name = "路径")
    private String pathName;

    /** 状态码 */
    @Excel(name = "状态码")
    private String status;

    /** 持续事件 */
    @Excel(name = "持续事件")
    private String duration;

    /** 请求参数 */
    @Excel(name = "请求参数")
    private String requestParams;

    /** 事件类型 */
    @Excel(name = "事件类型")
    private String eventType;

    /** 响应内容 */
    @Excel(name = "响应内容")
    private String responseData;

    /** 主表id */
    @Excel(name = "主表id")
    private String mianId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPathName(String pathName) 
    {
        this.pathName = pathName;
    }

    public String getPathName() 
    {
        return pathName;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDuration(String duration) 
    {
        this.duration = duration;
    }

    public String getDuration() 
    {
        return duration;
    }
    public void setRequestParams(String requestParams) 
    {
        this.requestParams = requestParams;
    }

    public String getRequestParams() 
    {
        return requestParams;
    }
    public void setEventType(String eventType) 
    {
        this.eventType = eventType;
    }

    public String getEventType() 
    {
        return eventType;
    }
    public void setResponseData(String responseData) 
    {
        this.responseData = responseData;
    }

    public String getResponseData() 
    {
        return responseData;
    }
    public void setMianId(String mianId) 
    {
        this.mianId = mianId;
    }

    public String getMianId() 
    {
        return mianId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pathName", getPathName())
            .append("status", getStatus())
            .append("duration", getDuration())
            .append("requestParams", getRequestParams())
            .append("eventType", getEventType())
            .append("responseData", getResponseData())
            .append("mianId", getMianId())
            .toString();
    }
}
