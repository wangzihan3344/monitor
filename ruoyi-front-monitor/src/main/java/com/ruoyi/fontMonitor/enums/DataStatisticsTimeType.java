package com.ruoyi.fontMonitor.enums;

/**
 * 数据统计用的时间类型
 */
public enum DataStatisticsTimeType {
    /**
     * 按日查询数据
     */
    EVERYDAY("0", "%Y-%m-%d"),

    /**
     * 按时查询数据
     */
    EVERYHOUR("1", "%Y-%m-%d %H"),

    /**
     * 按时查询数据
     */
    EVERYMINUTES("2", "%Y-%m-%d %H:%i");

    private final String type;
    private final String info;

    DataStatisticsTimeType(String type, String info) {
        this.type = type;
        this.info = info;
    }

    public String getType() {
        return type;
    }

    public String getInfo() {
        return info;
    }
}
