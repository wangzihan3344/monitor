package com.ruoyi.fontMonitor.mapper;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorBlankScreen;

/**
 * 监控白屏Mapper接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface FontMonitorBlankScreenMapper 
{
    /**
     * 查询监控白屏
     * 
     * @param id 监控白屏主键
     * @return 监控白屏
     */
    public FontMonitorBlankScreen selectFontMonitorBlankScreenById(Long id);

    /**
     * 查询监控白屏列表
     * 
     * @param fontMonitorBlankScreen 监控白屏
     * @return 监控白屏集合
     */
    public List<FontMonitorBlankScreen> selectFontMonitorBlankScreenList(FontMonitorBlankScreen fontMonitorBlankScreen);

    /**
     * 新增监控白屏
     * 
     * @param fontMonitorBlankScreen 监控白屏
     * @return 结果
     */
    public int insertFontMonitorBlankScreen(FontMonitorBlankScreen fontMonitorBlankScreen);

    /**
     * 修改监控白屏
     * 
     * @param fontMonitorBlankScreen 监控白屏
     * @return 结果
     */
    public int updateFontMonitorBlankScreen(FontMonitorBlankScreen fontMonitorBlankScreen);

    /**
     * 删除监控白屏
     * 
     * @param id 监控白屏主键
     * @return 结果
     */
    public int deleteFontMonitorBlankScreenById(Long id);

    /**
     * 批量删除监控白屏
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorBlankScreenByIds(Long[] ids);
}
