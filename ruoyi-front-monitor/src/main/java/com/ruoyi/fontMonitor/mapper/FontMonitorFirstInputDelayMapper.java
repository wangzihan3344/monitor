package com.ruoyi.fontMonitor.mapper;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorFirstInputDelay;

/**
 * 监控首次输入响应延迟Mapper接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface FontMonitorFirstInputDelayMapper 
{
    /**
     * 查询监控首次输入响应延迟
     * 
     * @param id 监控首次输入响应延迟主键
     * @return 监控首次输入响应延迟
     */
    public FontMonitorFirstInputDelay selectFontMonitorFirstInputDelayById(Long id);

    /**
     * 查询监控首次输入响应延迟列表
     * 
     * @param fontMonitorFirstInputDelay 监控首次输入响应延迟
     * @return 监控首次输入响应延迟集合
     */
    public List<FontMonitorFirstInputDelay> selectFontMonitorFirstInputDelayList(FontMonitorFirstInputDelay fontMonitorFirstInputDelay);

    /**
     * 新增监控首次输入响应延迟
     * 
     * @param fontMonitorFirstInputDelay 监控首次输入响应延迟
     * @return 结果
     */
    public int insertFontMonitorFirstInputDelay(FontMonitorFirstInputDelay fontMonitorFirstInputDelay);

    /**
     * 修改监控首次输入响应延迟
     * 
     * @param fontMonitorFirstInputDelay 监控首次输入响应延迟
     * @return 结果
     */
    public int updateFontMonitorFirstInputDelay(FontMonitorFirstInputDelay fontMonitorFirstInputDelay);

    /**
     * 删除监控首次输入响应延迟
     * 
     * @param id 监控首次输入响应延迟主键
     * @return 结果
     */
    public int deleteFontMonitorFirstInputDelayById(Long id);

    /**
     * 批量删除监控首次输入响应延迟
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorFirstInputDelayByIds(Long[] ids);
}
