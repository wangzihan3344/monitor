package com.ruoyi.fontMonitor.mapper;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorLoadTime;

/**
 * 监控加载时间Mapper接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface FontMonitorLoadTimeMapper 
{
    /**
     * 查询监控加载时间
     * 
     * @param id 监控加载时间主键
     * @return 监控加载时间
     */
    public FontMonitorLoadTime selectFontMonitorLoadTimeById(Long id);

    /**
     * 查询监控加载时间列表
     * 
     * @param fontMonitorLoadTime 监控加载时间
     * @return 监控加载时间集合
     */
    public List<FontMonitorLoadTime> selectFontMonitorLoadTimeList(FontMonitorLoadTime fontMonitorLoadTime);

    /**
     * 新增监控加载时间
     * 
     * @param fontMonitorLoadTime 监控加载时间
     * @return 结果
     */
    public int insertFontMonitorLoadTime(FontMonitorLoadTime fontMonitorLoadTime);

    /**
     * 修改监控加载时间
     * 
     * @param fontMonitorLoadTime 监控加载时间
     * @return 结果
     */
    public int updateFontMonitorLoadTime(FontMonitorLoadTime fontMonitorLoadTime);

    /**
     * 删除监控加载时间
     * 
     * @param id 监控加载时间主键
     * @return 结果
     */
    public int deleteFontMonitorLoadTimeById(Long id);

    /**
     * 批量删除监控加载时间
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorLoadTimeByIds(Long[] ids);
}
