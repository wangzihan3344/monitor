package com.ruoyi.fontMonitor.mapper;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorLongTask;

/**
 * 监控卡顿Mapper接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface FontMonitorLongTaskMapper 
{
    /**
     * 查询监控卡顿
     * 
     * @param id 监控卡顿主键
     * @return 监控卡顿
     */
    public FontMonitorLongTask selectFontMonitorLongTaskById(Long id);

    /**
     * 查询监控卡顿列表
     * 
     * @param fontMonitorLongTask 监控卡顿
     * @return 监控卡顿集合
     */
    public List<FontMonitorLongTask> selectFontMonitorLongTaskList(FontMonitorLongTask fontMonitorLongTask);

    /**
     * 新增监控卡顿
     * 
     * @param fontMonitorLongTask 监控卡顿
     * @return 结果
     */
    public int insertFontMonitorLongTask(FontMonitorLongTask fontMonitorLongTask);

    /**
     * 修改监控卡顿
     * 
     * @param fontMonitorLongTask 监控卡顿
     * @return 结果
     */
    public int updateFontMonitorLongTask(FontMonitorLongTask fontMonitorLongTask);

    /**
     * 删除监控卡顿
     * 
     * @param id 监控卡顿主键
     * @return 结果
     */
    public int deleteFontMonitorLongTaskById(Long id);

    /**
     * 批量删除监控卡顿
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorLongTaskByIds(Long[] ids);
}
