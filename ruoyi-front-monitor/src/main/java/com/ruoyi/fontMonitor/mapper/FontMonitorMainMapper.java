package com.ruoyi.fontMonitor.mapper;

import java.util.Date;
import java.util.List;

import com.ruoyi.fontMonitor.domain.*;
import org.apache.ibatis.annotations.Param;

/**
 * 公共字段Mapper接口
 *
 * @author ruoyi
 * @date 2022-08-30
 */
public interface FontMonitorMainMapper {
    /**
     * 查询公共字段
     *
     * @param id 公共字段主键
     * @return 公共字段
     */
    public FontMonitorMain selectFontMonitorMainById(Long id);

    /**
     * 查询公共字段列表
     *
     * @param fontMonitorMain 公共字段
     * @return 公共字段集合
     */
    public List<FontMonitorMain> selectFontMonitorMainList(FontMonitorMain fontMonitorMain);

    /**
     * 查询公共字段列表
     *
     * @param projectMessage 公共字段
     * @return 公共字段集合
     */
    public List<FontMonitorMain> selectFontMonitorMainListGroupBy(EnhanceProjectMessage projectMessage);

    /**
     * 新增公共字段
     *
     * @param fontMonitorMain 公共字段
     * @return 结果
     */
    public int insertFontMonitorMain(FontMonitorMain fontMonitorMain);

    /**
     * 修改公共字段
     *
     * @param fontMonitorMain 公共字段
     * @return 结果
     */
    public int updateFontMonitorMain(FontMonitorMain fontMonitorMain);

    /**
     * 删除公共字段
     *
     * @param id 公共字段主键
     * @return 结果
     */
    public int deleteFontMonitorMainById(Long id);

    /**
     * 批量删除公共字段
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorMainByIds(Long[] ids);

    /**
     * 数据统计查询
     *
     * @param timeType    不能为空，时间段类型，像是%Y-%m-%d之类的，详情见DataStatisticsTimeType
     * @param preDate     不能为空，开始统计时间
     * @param endDate     不能为空，结束统计时间
     * @param projectId   可以为null，项目pid
     * @param typeId      可以为null，类型
     * @param env         可以为null，项目环境
     * @param oldUserFlag 是否为老用户，是填1，不是填0
     * @param groupBy     归类字段,根据用户填uuid,根据页面填url,不归类填null
     * @return 统计期间每个时间段的数量
     */
    public List<EnhanceDataStatistics> dataStatistics(
            @Param("timeType") String timeType,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("projectId") String projectId,
            @Param("typeId") Long typeId,
            @Param("env") String env,
            @Param("oldUserFlag") Integer oldUserFlag,
            @Param("groupBy") String groupBy);

    /**
     * 数据统计apiError查询
     *
     * @param timeType  不能为空，时间段类型，像是%Y-%m-%d之类的，详情见DataStatisticsTimeType
     * @param preDate   不能为空，开始统计时间
     * @param endDate   不能为空，结束统计时间
     * @param projectId 可以为null，项目pid
     * @param typeId    可以为null，类型
     * @param apiStatus 可以为null，api状态，不为填入的数据
     * @return 统计期间每个时间段的数量
     */
    public List<EnhanceDataStatistics> dataStatisticsNumberOfApiErrorInXhr(
            @Param("timeType") String timeType,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("projectId") String projectId,
            @Param("typeId") Long typeId,
            @Param("apiStatus") String apiStatus);

    /**
     * 数据统计apiError查询
     *
     * @param timeType  不能为空，时间段类型，像是%Y-%m-%d之类的，详情见DataStatisticsTimeType
     * @param preDate   不能为空，开始统计时间
     * @param endDate   不能为空，结束统计时间
     * @param projectId 可以为null，项目pid
     * @param typeId    可以为null，类型
     * @param apiStatus 可以为null，api状态，不为填入的数据
     * @return 统计期间每个时间段的数量
     */
    public List<EnhanceDataStatistics> dataStatisticsNumberOfApiErrorInFetch(
            @Param("timeType") String timeType,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("projectId") String projectId,
            @Param("typeId") Long typeId,
            @Param("apiStatus") String apiStatus);

    /**
     * 数据统计apiError查询
     *
     * @param timeType    不能为空，时间段类型，像是%Y-%m-%d之类的，详情见 DataStatisticsTimeType
     * @param preDate     不能为空，开始统计时间
     * @param endDate     不能为空，结束统计时间
     * @param projectId   可以为null，项目pid
     * @param env         可以为null，环境
     * @param apiStatus   需不需要检索xhr表和fetch表,如果使用以下参数，这个必须为true
     * @param apiSuccess  apiError填false,apiSuccess填true
     * @param durationMin 最小持续时间,包括
     * @param durationMax 最大持续时间,不包括
     * @param groupBy     归类字段,根据用户填uuid,根据页面填url,根据api天pathName,不归类填null
     * @return 统计期间每个时间段的数量
     */
    public List<EnhanceDataStatistics> dataStatisticsNumberOfApi(
            @Param("timeType") String timeType,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("projectId") String projectId,
            @Param("env") String env,
            @Param("apiStatus") Boolean apiStatus,
            @Param("apiSuccess") Boolean apiSuccess,
            @Param("durationMin") Integer durationMin,
            @Param("durationMax") Integer durationMax,
            @Param("groupBy") String groupBy);

    /**
     * 数据统计apiError查询 规定时间内的总数
     *
     * @param preDate   不能为空，开始统计时间
     * @param endDate   不能为空，结束统计时间
     * @param projectId 可以为null，项目pid
     * @param env       可以为null，环境
     * @param apiStatus 可以为null，api状态，不为填入的数据
     * @return 统计期间每个时间段的数量
     */
    public List<FontMonitorMain> dataStatisticsNumberOfApiErrorAll(
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("projectId") String projectId,
            @Param("env") String env,
            @Param("apiStatus") Boolean apiStatus);

    /**
     * 查询某一段时间内的数据列表
     *
     * @param projectId 项目pid，可以为null
     * @param typeId    数据类型，可以为null
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @param evn       环境，可以为null
     * @return 数据列表
     */
    public List<FontMonitorMain> selectFontMonitorMainListByTimeRange(
            @Param("projectId") String projectId,
            @Param("typeId") String typeId,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("evn") String evn);

    /**
     * 查询某一段时间内的数据列表（api）
     *
     * @param projectId   项目pid，可以为null
     * @param preDate     开始时间，可以为null
     * @param endDate     结束时间，可以为null
     * @param evn         环境，可以为null
     * @param apiStatus   需不需要检索xhr表和fetch表,如果使用以下参数，这个必须为true
     * @param apiSuccess  apiError填false,apiSuccess填true
     * @param durationMin 最小持续时间,包括
     * @param durationMax 最大持续时间,不包括
     * @param pathName    指定某个接口，不需要的话填null
     * @param groupBy     归类字段,根据用户填uuid,根据页面填url,不归类填null
     * @return 数据列表
     */
    public List<FontMonitorMain> selectFontMonitorApiErrorMainListByTimeRange(
            @Param("projectId") String projectId,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("evn") String evn,
            @Param("apiStatus") Boolean apiStatus,
            @Param("apiSuccess") Boolean apiSuccess,
            @Param("durationMin") Integer durationMin,
            @Param("durationMax") Integer durationMax,
            @Param("pathName") String pathName,
            @Param("groupBy") String groupBy
    );

    /**
     * 查询某一段时间内的xhr数据列表
     *
     * @param projectId   项目pid，可以为null
     * @param preDate     开始时间，可以为null
     * @param endDate     结束时间，可以为null
     * @param evn         环境，可以为null
     * @param apiStatus   需不需要检索xhr表和fetch表,如果使用以下参数，这个必须为true
     * @param apiSuccess  apiError填false,apiSuccess填true
     * @param durationMin 最小持续时间,包括
     * @param durationMax 最大持续时间,不包括
     * @param pathName    指定某个接口，不需要的话填null
     * @param groupBy     归类字段,根据用户填uuid,根据页面填url,不归类填null
     * @return 数据列表
     */
    public List<EnhanceMainAndXhrAndFetchVo> selectFontMonitorXhrJoinMainListByTimeRange(
            @Param("projectId") String projectId,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("evn") String evn,
            @Param("apiStatus") Boolean apiStatus,
            @Param("apiSuccess") Boolean apiSuccess,
            @Param("durationMin") Integer durationMin,
            @Param("durationMax") Integer durationMax,
            @Param("pathName") String pathName,
            @Param("mainId") String mainId,
            @Param("uuid") String uuid,
            @Param("firstTimestamp") Date firstTimestamp,
            @Param("address") String address,
            @Param("groupBy") String groupBy);

    /**
     * 查询xhr表的duration和
     *
     * @param projectId  项目pid，可以为null
     * @param preDate    开始时间，可以为null
     * @param endDate    结束时间，可以为null
     * @param evn        环境，可以为null
     * @param apiSuccess apiError填false,apiSuccess填true
     * @param pathName   指定某个接口，不需要的话填null
     * @return 符合规定的xhr表的duration和
     */
    public Integer selectDurationSumFromXhr(
            @Param("projectId") String projectId,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("evn") String evn,
            @Param("apiSuccess") Boolean apiSuccess,
            @Param("pathName") String pathName
    );

    /**
     * 查询fetch表的duration和
     *
     * @param projectId  项目pid，可以为null
     * @param preDate    开始时间，可以为null
     * @param endDate    结束时间，可以为null
     * @param evn        环境，可以为null
     * @param apiSuccess apiError填false,apiSuccess填true
     * @param pathName   指定某个接口，不需要的话填null
     * @return 符合规定的fetch表的duration和
     */
    public Integer selectDurationSumFromFetch(
            @Param("projectId") String projectId,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("evn") String evn,
            @Param("apiSuccess") Boolean apiSuccess,
            @Param("pathName") String pathName
    );

    /**
     * 查询某一段时间内的数据列表（loadTime）
     *
     * @param projectId      项目pid，可以为null
     * @param preDate        开始时间，可以为null
     * @param endDate        结束时间，可以为null
     * @param evn            环境，可以为null
     * @param groupBy        归类字段,根据用户填uuid,根据页面填url,不归类填null
     * @param loadTimeStatus 需不需要检索load_time表,如果使用以下参数，这个必须为true
     * @param durationMin    最小持续时间,包括
     * @param durationMax    最大持续时间,不包括
     * @return 数据列表
     */
    public List<FontMonitorMain> selectFontMonitorMainListWithLoadTime(
            @Param("projectId") String projectId,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("evn") String evn,
            @Param("groupBy") String groupBy,
            @Param("loadTimeStatus") Boolean loadTimeStatus,
            @Param("durationMin") Integer durationMin,
            @Param("durationMax") Integer durationMax);

    /**
     * 数据统计查询 规定时间内的总数（loadTime）
     *
     * @param timeType       不能为空，时间段类型，像是%Y-%m-%d之类的，详情见 DataStatisticsTimeType
     * @param projectId      项目pid，可以为null
     * @param preDate        开始时间，可以为null
     * @param endDate        结束时间，可以为null
     * @param evn            环境，可以为null
     * @param groupBy        归类字段,根据用户填uuid,根据页面填url,不归类填null
     * @param loadTimeStatus 需不需要检索load_time表,如果使用以下参数，这个必须为true
     * @param durationMin    最小持续时间,包括
     * @param durationMax    最大持续时间,不包括
     * @return 统计期间每个时间段的数量
     */
    public List<EnhanceDataStatistics> selectFontMonitorMainWithLoadTimeDataStatisticsNumber(
            @Param("timeType") String timeType,
            @Param("projectId") String projectId,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("evn") String evn,
            @Param("groupBy") String groupBy,
            @Param("loadTimeStatus") Boolean loadTimeStatus,
            @Param("durationMin") Integer durationMin,
            @Param("durationMax") Integer durationMax
    );

    /**
     * 查询LoadTime表的duration和
     *
     * @param projectId   项目pid，可以为null
     * @param preDate     开始时间，可以为null
     * @param endDate     结束时间，可以为null
     * @param evn         环境，可以为null
     * @param durationMin 最小持续时间,包括
     * @param durationMax 最大持续时间,不包括
     * @return 符合规定的LoadTime表的duration和
     */
    public Integer selectDurationSumFromLoadTime(
            @Param("projectId") String projectId,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("evn") String evn,
            @Param("durationMin") Integer durationMin,
            @Param("durationMax") Integer durationMax
    );

    /**
     * 查询LoadTime表的指定项的平均值
     *
     * @param AVGFlag     需要求平均值的指定项,有ttfb_time/parse_dom_time/load_time,不需要填null
     * @param projectId   项目pid，可以为null
     * @param preDate     开始时间，可以为null
     * @param endDate     结束时间，可以为null
     * @param evn         环境，可以为null
     * @param durationMin 最小持续时间,包括
     * @param durationMax 最大持续时间,不包括
     * @return 符合规定的表的duration和
     */
    public Integer selectDataFromLoadTimeGetAVG(
            @Param("AVGFlag") String AVGFlag,
            @Param("projectId") String projectId,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("evn") String evn,
            @Param("durationMin") Integer durationMin,
            @Param("durationMax") Integer durationMax
    );

    public List<EnhanceMainAndLoadTimeVo> selectFontMonitorMainListWithLoadTimeAndUrl(
            @Param("url") String url,
            @Param("projectId") String projectId,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("evn") String evn,
            @Param("durationMin") Integer durationMin,
            @Param("durationMax") Integer durationMax
    );

    /**
     * 查询某一段时间内的数据列表（stayTime）
     *
     * @param projectId      项目pid，可以为null
     * @param preDate        开始时间，可以为null
     * @param endDate        结束时间，可以为null
     * @param evn            环境，可以为null
     * @param groupBy        归类字段,根据用户填uuid,根据页面填url,不归类填null
     * @param loadTimeStatus 需不需要检索load_time表,如果使用以下参数，这个必须为true
     * @param durationMin    最小持续时间,包括
     * @param durationMax    最大持续时间,不包括
     * @return 数据列表
     */
    public List<FontMonitorMain> selectFontMonitorMainListWithStayTime(
            @Param("projectId") String projectId,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("evn") String evn,
            @Param("groupBy") String groupBy,
            @Param("loadTimeStatus") Boolean loadTimeStatus,
            @Param("durationMin") Integer durationMin,
            @Param("durationMax") Integer durationMax);

    /**
     * 查询某一段时间内的数据列表（stayTime）
     *
     * @param projectId   项目pid，可以为null
     * @param preDate     开始时间，可以为null
     * @param endDate     结束时间，可以为null
     * @param evn         环境，可以为null
     * @param durationMin 最小持续时间,包括
     * @param durationMax 最大持续时间,不包括
     * @return 数据列表
     */
    public Integer statisticTenDaysUserAvgStay(
            @Param("projectId") String projectId,
            @Param("preDate") String preDate,
            @Param("endDate") String endDate,
            @Param("evn") String evn,
            @Param("durationMin") Integer durationMin,
            @Param("durationMax") Integer durationMax);

    /**
     * 查询某一段时间内的数据列表（stayTime）
     *
     * @param projectId     项目pid，可以为null
     * @param preDate       开始时间，可以为null
     * @param endDate       结束时间，可以为null
     * @param evn           环境，可以为null
     * @param preDateDayAgo 开始时间，可以为null
     * @param endDateDayAgo 结束时间，可以为null
     * @return 数据列表
     */
    public List<FontMonitorMain> selectMainListOnTenDaysNewUserInNextDay(@Param("projectId") String projectId,
                                                                         @Param("preDate") String preDate,
                                                                         @Param("endDate") String endDate,
                                                                         @Param("evn") String evn,
                                                                         @Param("preDateDayAgo") String preDateDayAgo,
                                                                         @Param("endDateDayAgo") String endDateDayAgo);

    /**
     * 根据url指定量查询main数据
     *
     * @param projectId     项目pid，可以为null
     * @param evn           环境，可以为null
     * @param appointNumber 指定量
     * @return 满足条件的指定变量及其个数的集合
     */
    public List<EnhanceTopAppoint> statisticTopByUrlAppointInMain(@Param("projectId") String projectId,
                                                                  @Param("evn") String evn,
                                                                  @Param("appointNumber") Long appointNumber);

    /**
     * 根据浏览器指定量查询main数据
     *
     * @param projectId     项目pid，可以为null
     * @param evn           环境，可以为null
     * @param appointNumber 指定量
     * @return 满足条件的指定变量及其个数的集合
     */
    public List<EnhanceTopAppoint> statisticTopByBrowserAppointInMain(@Param("projectId") String projectId,
                                                                      @Param("evn") String evn,
                                                                      @Param("appointNumber") Long appointNumber);

    /**
     * 根据OS指定量查询main数据
     *
     * @param projectId     项目pid，可以为null
     * @param evn           环境，可以为null
     * @param appointNumber 指定量
     * @return 满足条件的指定变量及其个数的集合
     */
    public List<EnhanceTopAppoint> statisticTopByOSAppointInMain(@Param("projectId") String projectId,
                                                                 @Param("evn") String evn,
                                                                 @Param("appointNumber") Long appointNumber);

    /**
     * 根据屏幕分辨率指定量查询main数据
     *
     * @param projectId     项目pid，可以为null
     * @param evn           环境，可以为null
     * @param appointNumber 指定量
     * @return 满足条件的指定变量及其个数的集合
     */
    public List<EnhanceTopAppoint> statisticTopByScreenAppointInMainAndBlankScreen(@Param("projectId") String projectId,
                                                                                   @Param("evn") String evn,
                                                                                   @Param("appointNumber") Long appointNumber);

    /**
     * 根据城市指定量查询main数据
     *
     * @param projectId     项目pid，可以为null
     * @param evn           环境，可以为null
     * @param appointNumber 指定量
     * @return 满足条件的指定变量及其个数的集合
     */
    public List<EnhanceTopAppoint> statisticTopByIpCityAppointInMain(@Param("projectId") String projectId,
                                                                     @Param("evn") String evn,
                                                                     @Param("appointNumber") Long appointNumber);

    /**
     * 根据国家指定量查询main数据
     *
     * @param projectId     项目pid，可以为null
     * @param evn           环境，可以为null
     * @param appointNumber 指定量
     * @return 满足条件的指定变量及其个数的集合
     */
    public List<EnhanceTopAppoint> statisticTopByIpCountryAppointInMain(@Param("projectId") String projectId,
                                                                        @Param("evn") String evn,
                                                                        @Param("appointNumber") Long appointNumber,
                                                                        @Param("preDate") String preDate,
                                                                        @Param("endDate") String endDate);

    /**
     * 根据省份指定量查询main数据
     *
     * @param projectId     项目pid，可以为null
     * @param evn           环境，可以为null
     * @param appointNumber 指定量
     * @return 满足条件的指定变量及其个数的集合
     */
    public List<EnhanceTopAppoint> statisticTopByIpProvinceAppointInMain(@Param("projectId") String projectId,
                                                                         @Param("evn") String evn,
                                                                         @Param("appointNumber") Long appointNumber,
                                                                         @Param("preDate") String preDate,
                                                                         @Param("endDate") String endDate);

    /**
     * main表左联visitInfo表，只返回url、来源url和mainId
     *
     * @param evn 环境，可以为null
     * @return 满足条件的指定变量及其个数的集合
     */
    public List<EnhanceTopAppoint> selectMainLeftJoinVisitInfo(@Param("projectId") String projectId,
                                                               @Param("evn") String evn);

    /**
     * 根据省份指定量查询main数据
     *
     * @param projectId     项目pid，可以为null
     * @param evn           环境，可以为null
     * @param appointNumber 指定量
     * @return 满足条件的指定变量及其个数的集合
     */
    public List<EnhanceTopAppoint> statisticTopByResourceOutWebAppointInMainAndVisitInfo(@Param("projectId") String projectId,
                                                                                         @Param("evn") String evn,
                                                                                         @Param("appointNumber") Long appointNumber);

    /**
     * 根据首次时间戳进行groudBy获取用户的总体访问记录
     *
     * @return 主表信息
     */
    public List<FontMonitorMain> selectMainListGroupByFirstTimestamp(@Param("projectId") String projectId,
                                                                     @Param("evn") String evn,
                                                                     @Param("preDate") String preDate,
                                                                     @Param("endDate") String endDate,
                                                                     @Param("ip") String ip,
                                                                     @Param("uuid") String uuid);

    /**
     * 页面加载耗时路由Top
     * 主表内联页面加载耗时表
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 页面加载信息
     */
    public List<EnhanceMainAndLoadTimeVo> selectMainLeftJoinLoadTime(@Param("projectId") String projectId,
                                                                     @Param("evn") String evn,
                                                                     @Param("preDate") String preDate,
                                                                     @Param("endDate") String endDate,
                                                                     @Param("mainId") String mainId,
                                                                     @Param("uuid") String uuid,
                                                                     @Param("firstTimestamp") Date firstTimestamp);

    /**
     * 主表内联Xhr联查
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联Xhr联查
     */
    public List<EnhanceMainAndXhrAndFetchVo> selectMainInnerJoinXhr(@Param("projectId") String projectId,
                                                                    @Param("evn") String evn,
                                                                    @Param("preDate") String preDate,
                                                                    @Param("endDate") String endDate,
                                                                    @Param("apiSuccess") Boolean apiSuccess,
                                                                    @Param("mainId") String mainId,
                                                                    @Param("uuid") String uuid,
                                                                    @Param("firstTimestamp") Date firstTimestamp);

    /**
     * 主表内联Fetch联查
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联Fetch联查
     */
    public List<EnhanceMainAndXhrAndFetchVo> selectMainInnerJoinFetch(@Param("projectId") String projectId,
                                                                      @Param("evn") String evn,
                                                                      @Param("preDate") String preDate,
                                                                      @Param("endDate") String endDate,
                                                                      @Param("apiSuccess") Boolean apiSuccess,
                                                                      @Param("mainId") String mainId,
                                                                      @Param("uuid") String uuid,
                                                                      @Param("firstTimestamp") Date firstTimestamp);

    /**
     * 主表内联JsError
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联JsError
     */
    public List<EnhanceMainAndJsAndPromiseAndCustomVo> selectMainInnerJoinJsError(@Param("projectId") String projectId,
                                                                                  @Param("evn") String evn,
                                                                                  @Param("preDate") String preDate,
                                                                                  @Param("endDate") String endDate,
                                                                                  @Param("mainId") String mainId);

    /**
     * 主表内联PromiseError
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联PromiseError
     */
    public List<EnhanceMainAndJsAndPromiseAndCustomVo> selectMainInnerJoinPromiseError(@Param("projectId") String projectId,
                                                                                       @Param("evn") String evn,
                                                                                       @Param("preDate") String preDate,
                                                                                       @Param("endDate") String endDate,
                                                                                       @Param("mainId") String mainId);

    /**
     * 主表内联Custom
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联CustomMessage
     */
    public List<EnhanceMainAndJsAndPromiseAndCustomVo> selectMainInnerJoinCustomMessage(@Param("projectId") String projectId,
                                                                                        @Param("evn") String evn,
                                                                                        @Param("preDate") String preDate,
                                                                                        @Param("endDate") String endDate,
                                                                                        @Param("mainId") String mainId);

    /**
     * 主表内联CustomResourceError
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联CustomMessage
     */
    public List<EnhanceMainAndResourceErrorVo> selectMainInnerJoinResourceError(@Param("projectId") String projectId,
                                                                                @Param("evn") String evn,
                                                                                @Param("preDate") String preDate,
                                                                                @Param("endDate") String endDate,
                                                                                @Param("mainId") String mainId);

    /**
     * 主表内联Resource
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联Resource
     */
    public List<EnhanceMainAndResourceVo> selectMainInnerResource(@Param("projectId") String projectId,
                                                                  @Param("evn") String evn,
                                                                  @Param("preDate") String preDate,
                                                                  @Param("endDate") String endDate,
                                                                  @Param("mainId") String mainId);

    /**
     * 主表内联BlankScreen
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联BlankScreen
     */
    public List<EnhanceMainAndBlankScreenVo> selectMainInnerJoinBlankScreen(@Param("projectId") String projectId,
                                                                            @Param("evn") String evn,
                                                                            @Param("preDate") String preDate,
                                                                            @Param("endDate") String endDate,
                                                                            @Param("mainId") String mainId);

    /**
     * 主表内联PaintTime
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联PaintTime
     */
    public List<EnhanceMainAndPaintTimeVo> selectMainInnerJoinPaintTime(@Param("projectId") String projectId,
                                                                        @Param("evn") String evn,
                                                                        @Param("preDate") String preDate,
                                                                        @Param("endDate") String endDate,
                                                                        @Param("mainId") String mainId);

    /**
     * 主表内联FirstInputDelay
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联FirstInputDelay
     */
    public List<EnhanceMainAndFirstInputDelayVo> selectMainInnerJoinFirstInputDelay(@Param("projectId") String projectId,
                                                                                    @Param("evn") String evn,
                                                                                    @Param("preDate") String preDate,
                                                                                    @Param("endDate") String endDate,
                                                                                    @Param("mainId") String mainId);

    /**
     * 主表内联LongTask
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联LongTask
     */
    public List<EnhanceMainAndLongTaskVo> selectMainInnerJoinLongTask(@Param("projectId") String projectId,
                                                                      @Param("evn") String evn,
                                                                      @Param("preDate") String preDate,
                                                                      @Param("endDate") String endDate,
                                                                      @Param("mainId") String mainId);

    /**
     * 主表内联VisitInfo
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联VisitInfo
     */
    public List<EnhanceMainAndVisitInfoVo> selectMainInnerJoinVisitInfo(@Param("projectId") String projectId,
                                                                        @Param("evn") String evn,
                                                                        @Param("preDate") String preDate,
                                                                        @Param("endDate") String endDate,
                                                                        @Param("mainId") String mainId);

    /**
     * 主表内联StayTime
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联StayTime
     */
    public List<EnhanceMainAndStayTimeVo> selectMainInnerJoinStayTime(@Param("projectId") String projectId,
                                                                      @Param("evn") String evn,
                                                                      @Param("preDate") String preDate,
                                                                      @Param("endDate") String endDate,
                                                                      @Param("mainId") String mainId);

    /**
     * 主表内联StayTime
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联StayTime
     */
    public List<EnhanceMainAndHashVo> selectMainInnerHash(@Param("projectId") String projectId,
                                                          @Param("evn") String evn,
                                                          @Param("preDate") String preDate,
                                                          @Param("endDate") String endDate,
                                                          @Param("mainId") String mainId);

    /**
     * 主表内联StayTime
     *
     * @param projectId PID
     * @param evn       环境
     * @param preDate   开始时间，可以为null
     * @param endDate   结束时间，可以为null
     * @return 主表内联StayTime
     */
    public List<EnhanceMainAndHistoryVo> selectMainInnerHistory(@Param("projectId") String projectId,
                                                                @Param("evn") String evn,
                                                                @Param("preDate") String preDate,
                                                                @Param("endDate") String endDate,
                                                                @Param("mainId") String mainId);

}
