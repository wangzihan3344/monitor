package com.ruoyi.fontMonitor.mapper;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorPaintTime;

/**
 * 监控渲染时间Mapper接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface FontMonitorPaintTimeMapper 
{
    /**
     * 查询监控渲染时间
     * 
     * @param id 监控渲染时间主键
     * @return 监控渲染时间
     */
    public FontMonitorPaintTime selectFontMonitorPaintTimeById(Long id);

    /**
     * 查询监控渲染时间列表
     * 
     * @param fontMonitorPaintTime 监控渲染时间
     * @return 监控渲染时间集合
     */
    public List<FontMonitorPaintTime> selectFontMonitorPaintTimeList(FontMonitorPaintTime fontMonitorPaintTime);

    /**
     * 新增监控渲染时间
     * 
     * @param fontMonitorPaintTime 监控渲染时间
     * @return 结果
     */
    public int insertFontMonitorPaintTime(FontMonitorPaintTime fontMonitorPaintTime);

    /**
     * 修改监控渲染时间
     * 
     * @param fontMonitorPaintTime 监控渲染时间
     * @return 结果
     */
    public int updateFontMonitorPaintTime(FontMonitorPaintTime fontMonitorPaintTime);

    /**
     * 删除监控渲染时间
     * 
     * @param id 监控渲染时间主键
     * @return 结果
     */
    public int deleteFontMonitorPaintTimeById(Long id);

    /**
     * 批量删除监控渲染时间
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorPaintTimeByIds(Long[] ids);
}
