package com.ruoyi.fontMonitor.mapper;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorPromiseError;

/**
 * 监控 promiseErrorMapper接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface FontMonitorPromiseErrorMapper 
{
    /**
     * 查询监控 promiseError
     * 
     * @param id 监控 promiseError主键
     * @return 监控 promiseError
     */
    public FontMonitorPromiseError selectFontMonitorPromiseErrorById(Long id);

    /**
     * 查询监控 promiseError列表
     * 
     * @param fontMonitorPromiseError 监控 promiseError
     * @return 监控 promiseError集合
     */
    public List<FontMonitorPromiseError> selectFontMonitorPromiseErrorList(FontMonitorPromiseError fontMonitorPromiseError);

    /**
     * 新增监控 promiseError
     * 
     * @param fontMonitorPromiseError 监控 promiseError
     * @return 结果
     */
    public int insertFontMonitorPromiseError(FontMonitorPromiseError fontMonitorPromiseError);

    /**
     * 修改监控 promiseError
     * 
     * @param fontMonitorPromiseError 监控 promiseError
     * @return 结果
     */
    public int updateFontMonitorPromiseError(FontMonitorPromiseError fontMonitorPromiseError);

    /**
     * 删除监控 promiseError
     * 
     * @param id 监控 promiseError主键
     * @return 结果
     */
    public int deleteFontMonitorPromiseErrorById(Long id);

    /**
     * 批量删除监控 promiseError
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorPromiseErrorByIds(Long[] ids);
}
