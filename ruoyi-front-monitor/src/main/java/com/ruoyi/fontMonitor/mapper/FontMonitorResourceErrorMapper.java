package com.ruoyi.fontMonitor.mapper;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorResourceError;

/**
 * 监控 resourceErrorMapper接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface FontMonitorResourceErrorMapper 
{
    /**
     * 查询监控 resourceError
     * 
     * @param id 监控 resourceError主键
     * @return 监控 resourceError
     */
    public FontMonitorResourceError selectFontMonitorResourceErrorById(Long id);

    /**
     * 查询监控 resourceError列表
     * 
     * @param fontMonitorResourceError 监控 resourceError
     * @return 监控 resourceError集合
     */
    public List<FontMonitorResourceError> selectFontMonitorResourceErrorList(FontMonitorResourceError fontMonitorResourceError);

    /**
     * 新增监控 resourceError
     * 
     * @param fontMonitorResourceError 监控 resourceError
     * @return 结果
     */
    public int insertFontMonitorResourceError(FontMonitorResourceError fontMonitorResourceError);

    /**
     * 修改监控 resourceError
     * 
     * @param fontMonitorResourceError 监控 resourceError
     * @return 结果
     */
    public int updateFontMonitorResourceError(FontMonitorResourceError fontMonitorResourceError);

    /**
     * 删除监控 resourceError
     * 
     * @param id 监控 resourceError主键
     * @return 结果
     */
    public int deleteFontMonitorResourceErrorById(Long id);

    /**
     * 批量删除监控 resourceError
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorResourceErrorByIds(Long[] ids);
}
