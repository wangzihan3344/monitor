package com.ruoyi.fontMonitor.mapper;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorUserConnectTeam;

/**
 * 用户和团队的关系Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
public interface FontMonitorUserConnectTeamMapper 
{
    /**
     * 查询用户和团队的关系
     * 
     * @param id 用户和团队的关系主键
     * @return 用户和团队的关系
     */
    public FontMonitorUserConnectTeam selectFontMonitorUserConnectTeamById(Long id);

    /**
     * 查询用户和团队的关系列表
     * 
     * @param fontMonitorUserConnectTeam 用户和团队的关系
     * @return 用户和团队的关系集合
     */
    public List<FontMonitorUserConnectTeam> selectFontMonitorUserConnectTeamList(FontMonitorUserConnectTeam fontMonitorUserConnectTeam);

    /**
     * 新增用户和团队的关系
     * 
     * @param fontMonitorUserConnectTeam 用户和团队的关系
     * @return 结果
     */
    public int insertFontMonitorUserConnectTeam(FontMonitorUserConnectTeam fontMonitorUserConnectTeam);

    /**
     * 修改用户和团队的关系
     * 
     * @param fontMonitorUserConnectTeam 用户和团队的关系
     * @return 结果
     */
    public int updateFontMonitorUserConnectTeam(FontMonitorUserConnectTeam fontMonitorUserConnectTeam);

    /**
     * 删除用户和团队的关系
     * 
     * @param id 用户和团队的关系主键
     * @return 结果
     */
    public int deleteFontMonitorUserConnectTeamById(Long id);

    /**
     * 批量删除用户和团队的关系
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorUserConnectTeamByIds(Long[] ids);
}
