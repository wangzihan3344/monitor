package com.ruoyi.fontMonitor.mapper;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorUserTeamProject;

/**
 * 团队负责的项目Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
public interface FontMonitorUserTeamProjectMapper 
{
    /**
     * 查询团队负责的项目
     * 
     * @param id 团队负责的项目主键
     * @return 团队负责的项目
     */
    public FontMonitorUserTeamProject selectFontMonitorUserTeamProjectById(Long id);

    /**
     * 查询团队负责的项目列表
     * 
     * @param fontMonitorUserTeamProject 团队负责的项目
     * @return 团队负责的项目集合
     */
    public List<FontMonitorUserTeamProject> selectFontMonitorUserTeamProjectList(FontMonitorUserTeamProject fontMonitorUserTeamProject);

    /**
     * 新增团队负责的项目
     * 
     * @param fontMonitorUserTeamProject 团队负责的项目
     * @return 结果
     */
    public int insertFontMonitorUserTeamProject(FontMonitorUserTeamProject fontMonitorUserTeamProject);

    /**
     * 修改团队负责的项目
     * 
     * @param fontMonitorUserTeamProject 团队负责的项目
     * @return 结果
     */
    public int updateFontMonitorUserTeamProject(FontMonitorUserTeamProject fontMonitorUserTeamProject);

    /**
     * 删除团队负责的项目
     * 
     * @param id 团队负责的项目主键
     * @return 结果
     */
    public int deleteFontMonitorUserTeamProjectById(Long id);

    /**
     * 批量删除团队负责的项目
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorUserTeamProjectByIds(Long[] ids);
}
