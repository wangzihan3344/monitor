package com.ruoyi.fontMonitor.mapper;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorVisitInfo;

/**
 * 记录访问信息Mapper接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface FontMonitorVisitInfoMapper 
{
    /**
     * 查询记录访问信息
     * 
     * @param id 记录访问信息主键
     * @return 记录访问信息
     */
    public FontMonitorVisitInfo selectFontMonitorVisitInfoById(Long id);

    /**
     * 查询记录访问信息列表
     * 
     * @param fontMonitorVisitInfo 记录访问信息
     * @return 记录访问信息集合
     */
    public List<FontMonitorVisitInfo> selectFontMonitorVisitInfoList(FontMonitorVisitInfo fontMonitorVisitInfo);

    /**
     * 新增记录访问信息
     * 
     * @param fontMonitorVisitInfo 记录访问信息
     * @return 结果
     */
    public int insertFontMonitorVisitInfo(FontMonitorVisitInfo fontMonitorVisitInfo);

    /**
     * 修改记录访问信息
     * 
     * @param fontMonitorVisitInfo 记录访问信息
     * @return 结果
     */
    public int updateFontMonitorVisitInfo(FontMonitorVisitInfo fontMonitorVisitInfo);

    /**
     * 删除记录访问信息
     * 
     * @param id 记录访问信息主键
     * @return 结果
     */
    public int deleteFontMonitorVisitInfoById(Long id);

    /**
     * 批量删除记录访问信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorVisitInfoByIds(Long[] ids);
}
