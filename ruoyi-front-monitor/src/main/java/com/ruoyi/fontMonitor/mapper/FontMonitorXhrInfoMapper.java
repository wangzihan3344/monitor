package com.ruoyi.fontMonitor.mapper;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorXhrInfo;

/**
 * 监控 xhr 接口Mapper接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface FontMonitorXhrInfoMapper 
{
    /**
     * 查询监控 xhr 接口
     * 
     * @param id 监控 xhr 接口主键
     * @return 监控 xhr 接口
     */
    public FontMonitorXhrInfo selectFontMonitorXhrInfoById(Long id);

    /**
     * 查询监控 xhr 接口列表
     * 
     * @param fontMonitorXhrInfo 监控 xhr 接口
     * @return 监控 xhr 接口集合
     */
    public List<FontMonitorXhrInfo> selectFontMonitorXhrInfoList(FontMonitorXhrInfo fontMonitorXhrInfo);

    /**
     * 新增监控 xhr 接口
     * 
     * @param fontMonitorXhrInfo 监控 xhr 接口
     * @return 结果
     */
    public int insertFontMonitorXhrInfo(FontMonitorXhrInfo fontMonitorXhrInfo);

    /**
     * 修改监控 xhr 接口
     * 
     * @param fontMonitorXhrInfo 监控 xhr 接口
     * @return 结果
     */
    public int updateFontMonitorXhrInfo(FontMonitorXhrInfo fontMonitorXhrInfo);

    /**
     * 删除监控 xhr 接口
     * 
     * @param id 监控 xhr 接口主键
     * @return 结果
     */
    public int deleteFontMonitorXhrInfoById(Long id);

    /**
     * 批量删除监控 xhr 接口
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorXhrInfoByIds(Long[] ids);
}
