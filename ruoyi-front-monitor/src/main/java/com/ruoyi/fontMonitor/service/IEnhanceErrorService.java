package com.ruoyi.fontMonitor.service;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.fontMonitor.domain.EnhanceMainAndJsAndPromiseAndCustomVo;
import com.ruoyi.fontMonitor.domain.EnhanceMainAndResourceErrorVo;
import com.ruoyi.fontMonitor.domain.EnhanceMainAndXhrAndFetchVo;
import com.ruoyi.fontMonitor.domain.FontMonitorMain;

import java.util.List;

public interface IEnhanceErrorService {
    /**
     * 获取过往30天每天的jsError和promiseError
     *
     * @param projectId       项目id，为null或""时返回所有项目jsError和promiseError的总和
     * @param reportTimestamp 时间戳，为null或""时默认结束时间为当前时间
     * @return 过往30天每天的jsError和promiseError数量
     */
    JSONObject numberOfJsAndPromiseError(String projectId, String reportTimestamp, String env);

    /**
     * 获取js/promise/custom错误列表
     *
     * @param projectId       项目Id
     * @param env             项目环境
     * @param reportTimestamp 结束日期，默认当前时间
     * @param timeType        时间范围的类型(days/hours/minutes)，默认是天
     * @param range           时间范围，默认100天
     * @param type            决定返回的类型，类型有null、js、promise、custom、jsAndPromise、jsAndCustom、promiseAndCustom
     * @return 错误数据
     */
    List<EnhanceMainAndJsAndPromiseAndCustomVo> getJsAndPromiseAndCustomError(String projectId, String env, String reportTimestamp, String timeType, String range, String type);

    /**
     * 获取过往30天每天的Error(ApiError)
     *
     * @param projectId       项目id，为null或""时返回所有项目apiError的总和
     * @param reportTimestamp 时间戳，为null或""时默认结束时间为当前时间
     * @return 过往30天每天的apiError数量
     */
    JSONObject numberOfApiError(String projectId, String reportTimestamp, String env);

    /**
     * 查询一定时间内的数据列表
     *
     * @param projectId       项目id，，不填会返回所有项目
     * @param timeType        数据类型，不填会返回所有类型
     * @param reportTimestamp 结束时间时间戳，默认当前时间
     * @param env             环境，不填会返回所有环境
     * @param range           时间范围，默认100天
     * @return 数据列表
     */
    List<FontMonitorMain> searchApiErrorDataByDesignation(String projectId,
                                                          String env,
                                                          String reportTimestamp,
                                                          String timeType,
                                                          String range);

    /**
     * 获取api错误列表(包含子表数据)
     *
     * @param projectId       项目id，，不填会返回所有项目
     * @param timeType        数据类型，不填会返回所有类型
     * @param reportTimestamp 结束时间时间戳，默认当前时间
     * @param env             环境，不填会返回所有环境
     * @param range           时间范围，默认100天
     * @return 数据列表
     */
    List<EnhanceMainAndXhrAndFetchVo> getApiErrorByDesignation(String projectId,
                                                               String env,
                                                               String reportTimestamp,
                                                               String timeType,
                                                               String range);

    /**
     * 获取指定时间30天内各个项目环境每天产生的静态资源错误数量
     *
     * @param projectId       项目id，为null或""时返回所有项目ResourceError的总和
     * @param reportTimestamp 时间戳，为null或""时默认结束时间为当前时间
     * @return 过往30天每天的ResourceError数量
     */
    JSONObject numberOfResourceError(String projectId, String reportTimestamp, String env);

    /**
     * 获取Resource错误列表(包含子表数据)
     *
     * @param projectId       项目id，，不填会返回所有项目
     * @param timeType        数据类型，不填会返回所有类型
     * @param reportTimestamp 结束时间时间戳，默认当前时间
     * @param env             环境，不填会返回所有环境
     * @param range           时间范围，默认100天
     * @return获取Resource错误列表(包含子表数据)
     */
    List<EnhanceMainAndResourceErrorVo> getResourceErrorByDesignation(String projectId, String env, String reportTimestamp, String timeType, String range);

    /**
     * 获取当天项目健康评分、js报错量、promise异常量、自定义错误量、静态资源异常率、接口异常率
     *
     * @param projectId 项目id，必填
     * @return 当天项目健康评分、js报错量、promise异常量、自定义错误量、静态资源异常率、接口异常率
     */
    JSONObject getProjectScore(String projectId, String env, String reportTimestamp);

    /**
     * 获取指定时间24小时及一周前同一天24小时每小时内报错数量图表数据
     *
     * @param projectId 项目id，为null或""时返回所有项目的总和
     * @param env       环境，不填会返回所有环境
     * @param errorType 报错类型（js/customMessage/resourceError/apiError/promise）
     * @return
     */
    JSONObject getTwentyFourHourError(String projectId, String env, String errorType, String reportTimestamp);
}
