package com.ruoyi.fontMonitor.service;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.fontMonitor.domain.FontMonitorMain;

import java.util.Date;
import java.util.List;

public interface IEnhanceMainService {
    /**
     * 获取结束时间，如果为null或者空，则默认当前时间
     *
     * @param reportTimestamp 结束时间的时间戳
     * @return 结束时间
     */
    Date getEndDate(String reportTimestamp);

    /**
     * 查询一定时间内的数据列表
     *
     * @param projectId       项目id，，不填会返回所有项目
     * @param type            数据类型，不填会返回所有类型
     * @param reportTimestamp 结束时间时间戳，默认当前时间
     * @param env             环境，不填会返回所有环境
     * @param range           时间范围，默认100天
     * @return 数据列表
     */
    List<FontMonitorMain> searchDataByDesignation(String projectId,
                                                  String type,
                                                  String env,
                                                  String reportTimestamp,
                                                  String timeType,
                                                  String range);

    /**
     * 获取指定时间静态资源概况数据：总静态资源错误数、影响的页面数、影响的用户数
     *
     * @param projectId       项目id，不填会返回所有项目
     * @param reportTimestamp 结束时间时间戳，默认当前时间
     * @param env             环境，不填会返回所有环境
     * @param typeId          数据类型，不填会返回所有类型
     * @return 数据列表
     */
    public JSONObject listMainByTimeAndEnvANdGroupByUuid(String projectId, String reportTimestamp, String env, String typeId);

    /**
     * 获取开始时间
     *
     * @param endDate  结束时间
     * @param dateType 时间类型，详情见 Calendar
     * @param range    时间范围
     * @return 结束时间
     */
    public Date getPreDate(Date endDate, int dateType, int range);

    /**
     * 获取开始时间（当天0时作为开始时间）
     *
     * @param endDate 结束时间
     * @return 开始时间
     */
    public Date getPreDateZero(Date endDate);
}
