package com.ruoyi.fontMonitor.service;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.fontMonitor.domain.EnhanceDataStatistics;
import com.ruoyi.fontMonitor.domain.EnhanceMainAndLoadTimeVo;
import com.ruoyi.fontMonitor.domain.EnhanceMainAndXhrAndFetchVo;

import java.io.UnsupportedEncodingException;
import java.util.List;

public interface IEnhancePerformanceService {
    /**
     * 获取传入时间当天的api数量（根据耗时分段归类）和占据百分比
     *
     * @param projectId       项目id
     * @param sub             耗时分段
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 传入时间到当天每一个耗时分段的api数量和占据百分比
     */
    public JSONObject getApiNumberAndPercentage(String projectId, Integer sub, String reportTimestamp, String env);

    /**
     * 根据传入的时间分割条件获取指定日期指定耗时分段内的接口请求量，天、小时、分钟
     *
     * @param projectId       项目id
     * @param sub             耗时分段
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 每一个时间段每一个耗时分段的api数量和占据百分比
     */
    public List<EnhanceDataStatistics> getThirtyDaysApiNumber(String projectId, Integer sub, String reportTimestamp, String env, String timeType, Integer range);

    /**
     * 获取指定日期指定耗时分段1小时内的接口信息平均网络耗时、影响用户数、发生页面数、发生页面列表
     *
     * @param projectId       项目id
     * @param sub             耗时分段
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 指定日期指定耗时分段1小时内的接口信息平均网络耗时、影响用户数、发生页面数、发生页面列表
     */
    public JSONObject getApiMessage(String projectId, Integer sub, String reportTimestamp, String env);

    public JSONObject getApiInfo(String address, String projectId, Integer sub, String reportTimestamp, String env) throws UnsupportedEncodingException;

    /**
     * 获取传入时间到前一段时间的页面数量（根据耗时分段归类）和占据百分比
     *
     * @param projectId       项目id
     * @param sub             耗时分段
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @param timeType        时间类型，默认是天
     * @param range           时间跨度，默认30天
     * @return 传入时间到前一段时间某一个耗时分段的页面数量和占据百分比
     */
    public JSONObject getPageNumberAndPercentage(String projectId, Integer sub, String reportTimestamp, String env, String timeType, Integer range);

    /**
     * 根据传入的时间分割条件获取指定日期指定耗时分段内的页面请求量，天、小时、分钟
     *
     * @param projectId       项目id
     * @param sub             耗时分段
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @param timeType        时间类型，默认是天
     * @param range           时间跨度，默认30天
     * @return 传入时间到前一个小时每一个耗时分段的api数量和占据百分比
     */
    public List<EnhanceDataStatistics> getPageNumberByTimeRange(String projectId, Integer sub, String reportTimestamp, String env, String timeType, Integer range);

    /**
     * 获取指定日期指定耗时分段1小时内的页面信息平均网络耗时、影响用户数、发生页面数、发生页面列表
     *
     * @param projectId       项目id
     * @param sub             耗时分段
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 指定日期指定耗时分段1小时内的接口信息平均网络耗时、影响用户数、发生页面数、发生页面列表
     */
    public JSONObject getPageMessage(String projectId, Integer sub, String reportTimestamp, String env);

    JSONObject getPageInfo(String url, String projectId, Integer sub, String reportTimestamp, String env) throws UnsupportedEncodingException;

    /**
     * 获取指定时间7天内TTFB、Dom解析、页面加载平均时间
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 平均时间
     */
    public JSONObject getPageAVGTime(String projectId, String reportTimestamp, String env);

    /**
     * 获取指定时间7天内总接口请求量、接口请求平均耗时、接口请求成功率
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 总接口请求量、接口请求平均耗时、接口请求成功率
     */
    public JSONObject getApiAVGTime(String projectId, String reportTimestamp, String env);

    /**
     * 获取指定时间7天内每个时间段访问的数量
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 每个时间段访问的数量
     */
    public JSONObject getPageNumberInSevenDaysBySub(String projectId, String reportTimestamp, String env);

    /**
     * 获取指定时间7天内每个时间段访问的数量
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 每个时间段访问的数量
     */
    public JSONObject getApiNumberInSevenDaysBySub(String projectId, String reportTimestamp, String env);

    /**
     * 页面加载耗时路由Top
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @param number          数量
     * @return
     */
    public List<EnhanceMainAndLoadTimeVo> getTopByPage(String projectId, String reportTimestamp, String env, Long number);

    /**
     * Api请求耗时路由Top
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @param number          数量
     * @return
     */
    public List<EnhanceMainAndXhrAndFetchVo> getTopByApi(String projectId, String reportTimestamp, String env, Long number);
}
