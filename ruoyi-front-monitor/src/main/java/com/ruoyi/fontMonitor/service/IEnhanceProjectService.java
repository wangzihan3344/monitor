package com.ruoyi.fontMonitor.service;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.fontMonitor.domain.FontMonitorMain;
import com.ruoyi.fontMonitor.domain.FontMonitorUserTeamProject;

public interface IEnhanceProjectService {
    /**
     * 团队创建项目
     *
     * @param projectMessage 项目信息
     * @return 项目信息
     */
    FontMonitorUserTeamProject createProject(FontMonitorUserTeamProject projectMessage);

    /**
     * 通过pid查询项目信息
     *
     * @param pid 项目id，不是项目的id，是pid
     * @return 项目信息
     */
    FontMonitorUserTeamProject searchProjectByPid(String pid);

    /**
     * 获取指定时间应用流量数据（pv数、uv数、新访客数、ip数、人均访问次数、跳出率及各自较昨日增长率）
     *
     * @param monitorMain 主表信息
     * @return pv数、uv数、新访客数、ip数、人均访问次数、跳出率及各自较昨日增长率
     */
    JSONObject applicationTraffic(FontMonitorMain monitorMain);
}
