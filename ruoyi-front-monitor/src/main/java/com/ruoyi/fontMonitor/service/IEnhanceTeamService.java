package com.ruoyi.fontMonitor.service;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.fontMonitor.domain.FontMonitorUserConnectTeam;
import com.ruoyi.fontMonitor.domain.FontMonitorUserTeam;

public interface IEnhanceTeamService {
    /**
     * 创建团队，用于用户创建团队，用户端
     *
     * @param team 团队信息
     * @return 团队信息
     */
    FontMonitorUserTeam createTeam(JSONObject team);

    /**
     * 添加团队成员
     *
     * @param teamAndMember 添加信息
     * @return 添加信息
     */
    JSONObject addTeamMember(JSONObject teamAndMember);
}
