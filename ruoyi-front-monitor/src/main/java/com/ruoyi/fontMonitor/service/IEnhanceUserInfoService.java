package com.ruoyi.fontMonitor.service;

import com.ruoyi.fontMonitor.domain.FontMonitorUserMessage;

public interface IEnhanceUserInfoService {
    /**
     * 用户登录
     *
     * @param userMessage 用户信息
     */
    void userLogin (FontMonitorUserMessage userMessage);

    /**
     * 用户注册
     *
     * @param userMessage 用户信息
     * @return 用户信息
     */
    FontMonitorUserMessage userRegister (FontMonitorUserMessage userMessage);

    /**
     * 添加通过邮箱查询用户信息的内部接口
     *
     * @param Email 用户邮箱
     * @return 用户信息
     */
    FontMonitorUserMessage searchUserMesByEmail (String Email);
}
