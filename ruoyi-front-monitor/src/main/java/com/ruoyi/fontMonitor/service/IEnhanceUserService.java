package com.ruoyi.fontMonitor.service;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.fontMonitor.domain.EnhanceDataStatistics;
import com.ruoyi.fontMonitor.domain.EnhanceTopAppoint;
import com.ruoyi.fontMonitor.domain.FontMonitorMain;

import java.util.ArrayList;
import java.util.List;

public interface IEnhanceUserService {
    /**
     * 获取指定时间30天每日访问新、老用户数
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 指定时间30天每日访问新、老用户数
     */
    public JSONObject statisticsThirtyDaysInUser(String projectId, String reportTimestamp, String env);

    /**
     * 获取项目指定日期当天和一周前24小时内每小时页面访问量(封装)
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @param typeId          可以为null，类型
     * @param oldUserFlag     是否为老用户，是填1，不是填0
     * @param groupBy         归类字段,根据用户填uuid,根据页面填url,不归类填null
     * @return 获取项目指定日期当天和一周前24小时内每小时页面访问量(封装)
     */
    public JSONObject getTwentyFourHourBy(String projectId, String reportTimestamp, String env, Long typeId, Integer oldUserFlag, String groupBy);

    /**
     * 获取项目指定日期当天和一周前24小时内每小时页面访问量（pv量）
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 项目指定日期当天和一周前24小时内每小时页面访问量（pv量）
     */
    JSONObject getTwentyFourHourPv(String projectId, String reportTimestamp, String env);

    /**
     * 获取项目指定日期当天和一周前24小时内每小时页面访问量（uv量）
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 项目指定日期当天和一周前24小时内每小时页面访问量（pv量）
     */
    JSONObject getTwentyFourHourUv(String projectId, String reportTimestamp, String env);

    /**
     * 获取项目指定日期当天和一周前24小时内每小时页面访问量（newUser）
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 项目指定日期当天和一周前24小时内每小时页面访问量（pv量）
     */
    JSONObject getTwentyFourHourNewUser(String projectId, String reportTimestamp, String env);

    /**
     * 获取项目指定日期10天内每天用户平均在线时长
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 项目指定日期10天内每天用户平均在线时长
     */
    ArrayList<Integer> getTenDaysUserAvgStay(String projectId, String reportTimestamp, String env);

    /**
     * 获取项目指定日期10天内每天用户平均在线时长
     *
     * @param projectId       项目id
     * @param reportTimestamp 结束时间戳
     * @param env             项目环境
     * @return 项目指定日期10天内每天用户平均在线时长
     */
    ArrayList<Double> getTenDaysNewUserInNextDay(String projectId, String reportTimestamp, String env);

    /**
     * 获取访问量最高的前n个网站地址
     *
     * @param projectId     项目id
     * @param appointNumber 指定数目
     * @param env           项目环境
     * @return 排名前n的网站地址
     */
    List<EnhanceTopAppoint> getTopByUrl(String projectId, Long appointNumber, String env);

    /**
     * 获取访问量最高的前n个浏览器
     *
     * @param projectId     项目id
     * @param appointNumber 指定数目
     * @param env           项目环境
     * @return 排名前n的浏览器
     */
    List<EnhanceTopAppoint> getTopByBrowser(String projectId, Long appointNumber, String env);

    /**
     * 获取访问量最高的前n个系统版本
     *
     * @param projectId     项目id
     * @param appointNumber 指定数目
     * @param env           项目环境
     * @return 排名前n的系统版本
     */
    List<EnhanceTopAppoint> getTopByOS(String projectId, Long appointNumber, String env);

    /**
     * 获取访问量最高的前n个屏幕分辨率
     *
     * @param projectId     项目id
     * @param appointNumber 指定数目
     * @param env           项目环境
     * @return 排名前n的屏幕分辨率
     */
    List<EnhanceTopAppoint> getTopByScreen(String projectId, Long appointNumber, String env);

    /**
     * 获取访问量最高的前n个城市
     *
     * @param projectId     项目id
     * @param appointNumber 指定数目
     * @param env           项目环境
     * @return 排名前n的城市分辨率
     */
    List<EnhanceTopAppoint> getTopByCity(String projectId, Long appointNumber, String env);

    /**
     * 获取访问量最高的前n个国家
     *
     * @param projectId       项目id
     * @param appointNumber   指定数目
     * @param env             项目环境
     * @param reportTimestamp 时间戳
     * @return 排名前n的国家分辨率
     */
    List<EnhanceTopAppoint> getTopByCountry(String projectId, Long appointNumber, String env, String reportTimestamp);

    /**
     * 获取访问量最高的前n个省份
     *
     * @param projectId       项目id
     * @param appointNumber   指定数目
     * @param env             项目环境
     * @param reportTimestamp 时间戳
     * @return 排名前n的省份分辨率
     */
    List<EnhanceTopAppoint> getTopByProvince(String projectId, Long appointNumber, String env, String reportTimestamp);

    /**
     * 获取最高的前n个来源网站地址
     *
     * @param projectId     项目id
     * @param appointNumber 指定数目
     * @param env           项目环境
     * @return 排名前n的省份分辨率
     */
    List<EnhanceTopAppoint> getTopByResourceUrl(String projectId, Long appointNumber, String env);

    /**
     * 获取项目用户信息列表
     *
     * @param projectId 项目id
     * @param ip        ip
     * @param timestamp 结束时间戳
     * @return 主表信息
     */
    List<FontMonitorMain> getMainListGroupByFirstTimestamp(String projectId, String ip, String reportTimestamp, String env, String uuid);


    /**
     * 获取用户行为记录
     *
     * @param mainId          主表Id
     * @param reportTimestamp 时间戳
     * @param type            类型
     * @return 用户行为记录
     */
    JSONObject getUserAction(String mainId, String reportTimestamp, String type);

    /**
     * 获取用户访问页面平均加载耗时
     *
     * @param mainId 主表Id
     * @return 用户访问页面平均加载耗时
     */
    JSONObject getUserPageAvgLoadTime(String mainId);

    /**
     * 获取用户访问接口耗时区间分布
     *
     * @param mainId 主表Id
     * @return 获取用户访问接口耗时区间分布
     */
    JSONObject getUserInterfaceAvgTime(String mainId);
}
