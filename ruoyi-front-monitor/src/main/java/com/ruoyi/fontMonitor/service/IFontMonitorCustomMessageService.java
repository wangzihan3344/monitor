package com.ruoyi.fontMonitor.service;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorCustomMessage;

/**
 * custom_error 的 message 数据Service接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface IFontMonitorCustomMessageService 
{
    /**
     * 查询custom_error 的 message 数据
     * 
     * @param id custom_error 的 message 数据主键
     * @return custom_error 的 message 数据
     */
    public FontMonitorCustomMessage selectFontMonitorCustomMessageById(Long id);

    /**
     * 查询custom_error 的 message 数据列表
     * 
     * @param fontMonitorCustomMessage custom_error 的 message 数据
     * @return custom_error 的 message 数据集合
     */
    public List<FontMonitorCustomMessage> selectFontMonitorCustomMessageList(FontMonitorCustomMessage fontMonitorCustomMessage);

    /**
     * 新增custom_error 的 message 数据
     * 
     * @param fontMonitorCustomMessage custom_error 的 message 数据
     * @return 结果
     */
    public int insertFontMonitorCustomMessage(FontMonitorCustomMessage fontMonitorCustomMessage);

    /**
     * 修改custom_error 的 message 数据
     * 
     * @param fontMonitorCustomMessage custom_error 的 message 数据
     * @return 结果
     */
    public int updateFontMonitorCustomMessage(FontMonitorCustomMessage fontMonitorCustomMessage);

    /**
     * 批量删除custom_error 的 message 数据
     * 
     * @param ids 需要删除的custom_error 的 message 数据主键集合
     * @return 结果
     */
    public int deleteFontMonitorCustomMessageByIds(Long[] ids);

    /**
     * 删除custom_error 的 message 数据信息
     * 
     * @param id custom_error 的 message 数据主键
     * @return 结果
     */
    public int deleteFontMonitorCustomMessageById(Long id);
}
