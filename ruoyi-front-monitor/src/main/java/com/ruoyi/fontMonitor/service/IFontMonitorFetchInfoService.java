package com.ruoyi.fontMonitor.service;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorFetchInfo;

/**
 * 监控 fetch 接口Service接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface IFontMonitorFetchInfoService 
{
    /**
     * 查询监控 fetch 接口
     * 
     * @param id 监控 fetch 接口主键
     * @return 监控 fetch 接口
     */
    public FontMonitorFetchInfo selectFontMonitorFetchInfoById(Long id);

    /**
     * 查询监控 fetch 接口列表
     * 
     * @param fontMonitorFetchInfo 监控 fetch 接口
     * @return 监控 fetch 接口集合
     */
    public List<FontMonitorFetchInfo> selectFontMonitorFetchInfoList(FontMonitorFetchInfo fontMonitorFetchInfo);

    /**
     * 新增监控 fetch 接口
     * 
     * @param fontMonitorFetchInfo 监控 fetch 接口
     * @return 结果
     */
    public int insertFontMonitorFetchInfo(FontMonitorFetchInfo fontMonitorFetchInfo);

    /**
     * 修改监控 fetch 接口
     * 
     * @param fontMonitorFetchInfo 监控 fetch 接口
     * @return 结果
     */
    public int updateFontMonitorFetchInfo(FontMonitorFetchInfo fontMonitorFetchInfo);

    /**
     * 批量删除监控 fetch 接口
     * 
     * @param ids 需要删除的监控 fetch 接口主键集合
     * @return 结果
     */
    public int deleteFontMonitorFetchInfoByIds(Long[] ids);

    /**
     * 删除监控 fetch 接口信息
     * 
     * @param id 监控 fetch 接口主键
     * @return 结果
     */
    public int deleteFontMonitorFetchInfoById(Long id);
}
