package com.ruoyi.fontMonitor.service;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorHash;

/**
 * url变动Service接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface IFontMonitorHashService 
{
    /**
     * 查询url变动
     * 
     * @param id url变动主键
     * @return url变动
     */
    public FontMonitorHash selectFontMonitorHashById(Long id);

    /**
     * 查询url变动列表
     * 
     * @param fontMonitorHash url变动
     * @return url变动集合
     */
    public List<FontMonitorHash> selectFontMonitorHashList(FontMonitorHash fontMonitorHash);

    /**
     * 新增url变动
     * 
     * @param fontMonitorHash url变动
     * @return 结果
     */
    public int insertFontMonitorHash(FontMonitorHash fontMonitorHash);

    /**
     * 修改url变动
     * 
     * @param fontMonitorHash url变动
     * @return 结果
     */
    public int updateFontMonitorHash(FontMonitorHash fontMonitorHash);

    /**
     * 批量删除url变动
     * 
     * @param ids 需要删除的url变动主键集合
     * @return 结果
     */
    public int deleteFontMonitorHashByIds(Long[] ids);

    /**
     * 删除url变动信息
     * 
     * @param id url变动主键
     * @return 结果
     */
    public int deleteFontMonitorHashById(Long id);
}
