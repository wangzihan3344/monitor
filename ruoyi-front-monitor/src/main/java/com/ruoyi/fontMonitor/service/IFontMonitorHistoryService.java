package com.ruoyi.fontMonitor.service;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorHistory;

/**
 * 路由记录Service接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface IFontMonitorHistoryService 
{
    /**
     * 查询路由记录
     * 
     * @param id 路由记录主键
     * @return 路由记录
     */
    public FontMonitorHistory selectFontMonitorHistoryById(Long id);

    /**
     * 查询路由记录列表
     * 
     * @param fontMonitorHistory 路由记录
     * @return 路由记录集合
     */
    public List<FontMonitorHistory> selectFontMonitorHistoryList(FontMonitorHistory fontMonitorHistory);

    /**
     * 新增路由记录
     * 
     * @param fontMonitorHistory 路由记录
     * @return 结果
     */
    public int insertFontMonitorHistory(FontMonitorHistory fontMonitorHistory);

    /**
     * 修改路由记录
     * 
     * @param fontMonitorHistory 路由记录
     * @return 结果
     */
    public int updateFontMonitorHistory(FontMonitorHistory fontMonitorHistory);

    /**
     * 批量删除路由记录
     * 
     * @param ids 需要删除的路由记录主键集合
     * @return 结果
     */
    public int deleteFontMonitorHistoryByIds(Long[] ids);

    /**
     * 删除路由记录信息
     * 
     * @param id 路由记录主键
     * @return 结果
     */
    public int deleteFontMonitorHistoryById(Long id);
}
