package com.ruoyi.fontMonitor.service;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorJsError;

/**
 * 监控 jsErrorService接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface IFontMonitorJsErrorService 
{
    /**
     * 查询监控 jsError
     * 
     * @param id 监控 jsError主键
     * @return 监控 jsError
     */
    public FontMonitorJsError selectFontMonitorJsErrorById(Long id);

    /**
     * 查询监控 jsError列表
     * 
     * @param fontMonitorJsError 监控 jsError
     * @return 监控 jsError集合
     */
    public List<FontMonitorJsError> selectFontMonitorJsErrorList(FontMonitorJsError fontMonitorJsError);

    /**
     * 新增监控 jsError
     * 
     * @param fontMonitorJsError 监控 jsError
     * @return 结果
     */
    public int insertFontMonitorJsError(FontMonitorJsError fontMonitorJsError);

    /**
     * 修改监控 jsError
     * 
     * @param fontMonitorJsError 监控 jsError
     * @return 结果
     */
    public int updateFontMonitorJsError(FontMonitorJsError fontMonitorJsError);

    /**
     * 批量删除监控 jsError
     * 
     * @param ids 需要删除的监控 jsError主键集合
     * @return 结果
     */
    public int deleteFontMonitorJsErrorByIds(Long[] ids);

    /**
     * 删除监控 jsError信息
     * 
     * @param id 监控 jsError主键
     * @return 结果
     */
    public int deleteFontMonitorJsErrorById(Long id);
}
