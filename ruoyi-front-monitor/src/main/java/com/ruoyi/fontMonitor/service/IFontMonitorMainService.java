package com.ruoyi.fontMonitor.service;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorMain;

/**
 * 公共字段Service接口
 * 
 * @author ruoyi
 * @date 2022-08-30
 */
public interface IFontMonitorMainService 
{
    /**
     * 查询公共字段
     * 
     * @param id 公共字段主键
     * @return 公共字段
     */
    public FontMonitorMain selectFontMonitorMainById(Long id);

    /**
     * 查询公共字段列表
     * 
     * @param fontMonitorMain 公共字段
     * @return 公共字段集合
     */
    public List<FontMonitorMain> selectFontMonitorMainList(FontMonitorMain fontMonitorMain);

    /**
     * 新增公共字段
     * 
     * @param fontMonitorMain 公共字段
     * @return 结果
     */
    public int insertFontMonitorMain(FontMonitorMain fontMonitorMain);

    /**
     * 修改公共字段
     * 
     * @param fontMonitorMain 公共字段
     * @return 结果
     */
    public int updateFontMonitorMain(FontMonitorMain fontMonitorMain);

    /**
     * 批量删除公共字段
     * 
     * @param ids 需要删除的公共字段主键集合
     * @return 结果
     */
    public int deleteFontMonitorMainByIds(Long[] ids);

    /**
     * 删除公共字段信息
     * 
     * @param id 公共字段主键
     * @return 结果
     */
    public int deleteFontMonitorMainById(Long id);
}
