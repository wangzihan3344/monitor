package com.ruoyi.fontMonitor.service;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorResource;

/**
 * 资源监听Service接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface IFontMonitorResourceService 
{
    /**
     * 查询资源监听
     * 
     * @param id 资源监听主键
     * @return 资源监听
     */
    public FontMonitorResource selectFontMonitorResourceById(Long id);

    /**
     * 查询资源监听列表
     * 
     * @param fontMonitorResource 资源监听
     * @return 资源监听集合
     */
    public List<FontMonitorResource> selectFontMonitorResourceList(FontMonitorResource fontMonitorResource);

    /**
     * 新增资源监听
     * 
     * @param fontMonitorResource 资源监听
     * @return 结果
     */
    public int insertFontMonitorResource(FontMonitorResource fontMonitorResource);

    /**
     * 修改资源监听
     * 
     * @param fontMonitorResource 资源监听
     * @return 结果
     */
    public int updateFontMonitorResource(FontMonitorResource fontMonitorResource);

    /**
     * 批量删除资源监听
     * 
     * @param ids 需要删除的资源监听主键集合
     * @return 结果
     */
    public int deleteFontMonitorResourceByIds(Long[] ids);

    /**
     * 删除资源监听信息
     * 
     * @param id 资源监听主键
     * @return 结果
     */
    public int deleteFontMonitorResourceById(Long id);
}
