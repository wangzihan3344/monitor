package com.ruoyi.fontMonitor.service;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorStayTime;

/**
 * 记录停留时间Service接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface IFontMonitorStayTimeService 
{
    /**
     * 查询记录停留时间
     * 
     * @param id 记录停留时间主键
     * @return 记录停留时间
     */
    public FontMonitorStayTime selectFontMonitorStayTimeById(Long id);

    /**
     * 查询记录停留时间列表
     * 
     * @param fontMonitorStayTime 记录停留时间
     * @return 记录停留时间集合
     */
    public List<FontMonitorStayTime> selectFontMonitorStayTimeList(FontMonitorStayTime fontMonitorStayTime);

    /**
     * 新增记录停留时间
     * 
     * @param fontMonitorStayTime 记录停留时间
     * @return 结果
     */
    public int insertFontMonitorStayTime(FontMonitorStayTime fontMonitorStayTime);

    /**
     * 修改记录停留时间
     * 
     * @param fontMonitorStayTime 记录停留时间
     * @return 结果
     */
    public int updateFontMonitorStayTime(FontMonitorStayTime fontMonitorStayTime);

    /**
     * 批量删除记录停留时间
     * 
     * @param ids 需要删除的记录停留时间主键集合
     * @return 结果
     */
    public int deleteFontMonitorStayTimeByIds(Long[] ids);

    /**
     * 删除记录停留时间信息
     * 
     * @param id 记录停留时间主键
     * @return 结果
     */
    public int deleteFontMonitorStayTimeById(Long id);
}
