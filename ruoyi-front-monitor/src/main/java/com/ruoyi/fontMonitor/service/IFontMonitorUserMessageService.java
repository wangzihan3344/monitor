package com.ruoyi.fontMonitor.service;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorUserMessage;

/**
 * 用户信息Service接口
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
public interface IFontMonitorUserMessageService 
{
    /**
     * 查询用户信息
     * 
     * @param id 用户信息主键
     * @return 用户信息
     */
    public FontMonitorUserMessage selectFontMonitorUserMessageById(Long id);

    /**
     * 查询用户信息列表
     * 
     * @param fontMonitorUserMessage 用户信息
     * @return 用户信息集合
     */
    public List<FontMonitorUserMessage> selectFontMonitorUserMessageList(FontMonitorUserMessage fontMonitorUserMessage);

    /**
     * 新增用户信息
     * 
     * @param fontMonitorUserMessage 用户信息
     * @return 结果
     */
    public int insertFontMonitorUserMessage(FontMonitorUserMessage fontMonitorUserMessage);

    /**
     * 修改用户信息
     * 
     * @param fontMonitorUserMessage 用户信息
     * @return 结果
     */
    public int updateFontMonitorUserMessage(FontMonitorUserMessage fontMonitorUserMessage);

    /**
     * 批量删除用户信息
     * 
     * @param ids 需要删除的用户信息主键集合
     * @return 结果
     */
    public int deleteFontMonitorUserMessageByIds(Long[] ids);

    /**
     * 删除用户信息信息
     * 
     * @param id 用户信息主键
     * @return 结果
     */
    public int deleteFontMonitorUserMessageById(Long id);
}
