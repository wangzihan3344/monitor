package com.ruoyi.fontMonitor.service;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorUserTeam;

/**
 * 团队信息Service接口
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
public interface IFontMonitorUserTeamService 
{
    /**
     * 查询团队信息
     * 
     * @param id 团队信息主键
     * @return 团队信息
     */
    public FontMonitorUserTeam selectFontMonitorUserTeamById(Long id);

    /**
     * 查询团队信息列表
     * 
     * @param fontMonitorUserTeam 团队信息
     * @return 团队信息集合
     */
    public List<FontMonitorUserTeam> selectFontMonitorUserTeamList(FontMonitorUserTeam fontMonitorUserTeam);

    /**
     * 新增团队信息
     * 
     * @param fontMonitorUserTeam 团队信息
     * @return 结果
     */
    public int insertFontMonitorUserTeam(FontMonitorUserTeam fontMonitorUserTeam);

    /**
     * 修改团队信息
     * 
     * @param fontMonitorUserTeam 团队信息
     * @return 结果
     */
    public int updateFontMonitorUserTeam(FontMonitorUserTeam fontMonitorUserTeam);

    /**
     * 批量删除团队信息
     * 
     * @param ids 需要删除的团队信息主键集合
     * @return 结果
     */
    public int deleteFontMonitorUserTeamByIds(Long[] ids);

    /**
     * 删除团队信息信息
     * 
     * @param id 团队信息主键
     * @return 结果
     */
    public int deleteFontMonitorUserTeamById(Long id);
}
