package com.ruoyi.fontMonitor.service;

import java.util.List;
import com.ruoyi.fontMonitor.domain.FontMonitorVisitInfo;

/**
 * 记录访问信息Service接口
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
public interface IFontMonitorVisitInfoService 
{
    /**
     * 查询记录访问信息
     * 
     * @param id 记录访问信息主键
     * @return 记录访问信息
     */
    public FontMonitorVisitInfo selectFontMonitorVisitInfoById(Long id);

    /**
     * 查询记录访问信息列表
     * 
     * @param fontMonitorVisitInfo 记录访问信息
     * @return 记录访问信息集合
     */
    public List<FontMonitorVisitInfo> selectFontMonitorVisitInfoList(FontMonitorVisitInfo fontMonitorVisitInfo);

    /**
     * 新增记录访问信息
     * 
     * @param fontMonitorVisitInfo 记录访问信息
     * @return 结果
     */
    public int insertFontMonitorVisitInfo(FontMonitorVisitInfo fontMonitorVisitInfo);

    /**
     * 修改记录访问信息
     * 
     * @param fontMonitorVisitInfo 记录访问信息
     * @return 结果
     */
    public int updateFontMonitorVisitInfo(FontMonitorVisitInfo fontMonitorVisitInfo);

    /**
     * 批量删除记录访问信息
     * 
     * @param ids 需要删除的记录访问信息主键集合
     * @return 结果
     */
    public int deleteFontMonitorVisitInfoByIds(Long[] ids);

    /**
     * 删除记录访问信息信息
     * 
     * @param id 记录访问信息主键
     * @return 结果
     */
    public int deleteFontMonitorVisitInfoById(Long id);

    /**
     * 判断来源是否为外部网站
     */
    public Boolean isExternalWebsite(String resourceUrl, String url);
}
