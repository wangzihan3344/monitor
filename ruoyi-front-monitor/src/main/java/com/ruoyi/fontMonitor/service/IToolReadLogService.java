package com.ruoyi.fontMonitor.service;

import java.util.ArrayList;

@SuppressWarnings("all")
public interface IToolReadLogService {

    /**
     * 获取当天的Logs文件路径
     *
     * @return 获取当天的Logs文件路径
     */
    public String getLogsName();

    /**
     * 读取log文件，将其转换成String列表返回
     * 如果传进来是null，默认读取测试路径
     * 如果不是，读取传进来的路径
     *
     * @return logs的集合
     */
    ArrayList<String> readLog(String fileAddress);

    /**
     * 筛选记录，返回指定的数据
     *
     * @param logs 记录
     * @return 筛选结果
     */
    ArrayList<String> screenLogs(ArrayList<String> logs);

    /**
     * 将记录从String形式转换成JSON
     *
     * @param requirementsLogs String形式法的记录集合
     * @return JSON形式的记录集合
     */
    ArrayList logsStringConversionJSON(ArrayList<String> requirementsLogs);

    /**
     * 将Logs的数据存入数据库
     *
     * @param requirementsJSON JSON形式的Logs集合
     */
    void dataRecordedInDatabase(ArrayList requirementsJSON);
}
