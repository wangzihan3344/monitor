package com.ruoyi.fontMonitor.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.fontMonitor.domain.*;
import com.ruoyi.fontMonitor.enums.DataStatisticsTimeType;
import com.ruoyi.fontMonitor.mapper.FontMonitorMainMapper;
import com.ruoyi.fontMonitor.service.IEnhanceErrorService;
import com.ruoyi.fontMonitor.service.IEnhanceMainService;
import com.ruoyi.fontMonitor.service.IEnhanceProjectService;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.ruoyi.common.utils.PageUtils.startPage;

@Service
@SuppressWarnings("all")
public class EnhanceErrorServiceImpl implements IEnhanceErrorService {

    @Autowired
    private FontMonitorMainMapper mainMapper;

    @Autowired
    private IEnhanceProjectService enhanceProjectService;

    @Autowired
    private IEnhanceMainService enhanceMainService;

    @Autowired
    private SysDictDataMapper sysDictDataMapper;

    private Date getPreDate(Date endDate, String timeType, String range) {
        // 规定默认时间范围,range必须是数字，然后转为负数
        if (range == null || range.equals(""))
            range = String.valueOf(100L);
        else if (!StringUtils.isNumeric(range))
            throw new RuntimeException("时间范围为数字");
        int timeRange;
        try {
            timeRange = Integer.parseInt(range);
            timeRange = -timeRange;
        } catch (Exception e) {
            throw new RuntimeException("时间范围数字不准确");
        }
        // 规定时间范围类型
        if (timeType == null || timeType.equals(""))
            timeType = "days";
        int dateType;
        switch (timeType) {
            case "days":
                dateType = Calendar.DATE;
                break;
            case "hours":
                dateType = Calendar.HOUR;
                break;
            case "minutes":
                dateType = Calendar.MINUTE;
                break;
            default:
                throw new ServiceException("没有这种时间类型");
        }
        return enhanceMainService.getPreDate(endDate, dateType, timeRange);
    }

    @Override
    public JSONObject numberOfJsAndPromiseError(String projectId, String reportTimestamp, String env) {
        //获取结束时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();//格式化时间
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //创建Calendar实例
        Calendar cal = Calendar.getInstance();
        //输入结束时间
        cal.setTime(endDate);
        //在当前时间基础上减30天
        cal.add(Calendar.DATE, -30);
        //查询jsError近30天的数据
        List<EnhanceDataStatistics> jsErrorData = mainMapper.dataStatistics(
                DataStatisticsTimeType.EVERYDAY.getInfo(),
                simpleDateFormat.format(cal.getTime()),
                simpleDateFormat.format(endDate),
                projectId,
                0L,
                env,
                null, null
        );
        //查询promiseError近30天的数据
        List<EnhanceDataStatistics> promiseErrorData = mainMapper.dataStatistics(
                DataStatisticsTimeType.EVERYDAY.getInfo(),
                simpleDateFormat.format(cal.getTime()),
                simpleDateFormat.format(endDate),
                projectId,
                1L,
                env,
                null, null
        );
        //组装返回体
        JSONObject returnData = new JSONObject();
        returnData.put("jsErrorData", jsErrorData);
        returnData.put("promiseErrorData", promiseErrorData);
        return returnData;
    }

    @Override
    public List<EnhanceMainAndJsAndPromiseAndCustomVo> getJsAndPromiseAndCustomError(
            String projectId, String env, String reportTimestamp, String timeType, String range, String type) {
        // 验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        // 格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        // 获取结束时间
        Date date = enhanceMainService.getEndDate(reportTimestamp);
        String endDate = simpleDateFormat.format(date);
        // 获取开始时间
        String preDate = simpleDateFormat.format(getPreDate(date, timeType, range));
        // 根据type进行筛选，分别查询js、promise、custom
        List<EnhanceMainAndJsAndPromiseAndCustomVo> jsErrorList = new ArrayList<>();
        List<EnhanceMainAndJsAndPromiseAndCustomVo> promiseErrorList = new ArrayList<>();
        List<EnhanceMainAndJsAndPromiseAndCustomVo> customMessageList = new ArrayList<>();
        if (type == null || type.equals("") || type.equals("js") || type.equals("jsAndPromise") || type.equals("jsAndCustom")) {
            jsErrorList = mainMapper.selectMainInnerJoinJsError(projectId, env, preDate, endDate, null);
        }
        if (type == null || type.equals("") || type.equals("promise") || type.equals("jsAndPromise") || type.equals("promiseAndCustom")) {
            promiseErrorList = mainMapper.selectMainInnerJoinPromiseError(projectId, env, preDate, endDate, null);
        }
        if (type == null || type.equals("") || type.equals("custom") || type.equals("jsAndCustom") || type.equals("promiseAndCustom")) {
            customMessageList = mainMapper.selectMainInnerJoinCustomMessage(projectId, env, preDate, endDate, null);
        }
        // 整合数据
        List<EnhanceMainAndJsAndPromiseAndCustomVo> jsAndPromise =
                Stream.concat(jsErrorList.stream(), promiseErrorList.stream()).collect(Collectors.toList());
        List<EnhanceMainAndJsAndPromiseAndCustomVo> allDate =
                Stream.concat(jsAndPromise.stream(), customMessageList.stream())
                        .sorted((o1, o2) -> (int) (o1.getId() - o2.getId()))
                        .collect(Collectors.toList());
        return allDate;
    }

    @Override
    public JSONObject numberOfApiError(String projectId, String reportTimestamp, String env) {
        //获取结束时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();//格式化时间
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //创建Calendar实例
        Calendar cal = Calendar.getInstance();
        //输入结束时间
        cal.setTime(endDate);
        //在当前时间基础上减30天
        cal.add(Calendar.DATE, -30);
        //查询xhr近30天的Error数据
        List<EnhanceDataStatistics> apiErrorData = mainMapper.dataStatisticsNumberOfApi(
                DataStatisticsTimeType.EVERYDAY.getInfo(),
                simpleDateFormat.format(cal.getTime()),
                simpleDateFormat.format(endDate),
                projectId,
                env,
                true,
                false,
                null,
                null,
                null
        );
        //组装返回体
        JSONObject returnData = new JSONObject();
        returnData.put("apiErrorData", apiErrorData);
        return returnData;
    }

    @Override
    public List<FontMonitorMain> searchApiErrorDataByDesignation(String projectId, String env, String reportTimestamp, String timeType, String range) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        //range必须是数字，然后转为负数
        if (range == null || range.equals(""))
            range = String.valueOf(100L);
        else if (!StringUtils.isNumeric(range))
            throw new RuntimeException("时间范围为数字");
        int timeRange;
        try {
            timeRange = Integer.parseInt(range);
            timeRange = -timeRange;
        } catch (Exception e) {
            throw new RuntimeException("时间范围数字不准确");
        }
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //创建Calendar实例
        Calendar cal = Calendar.getInstance();
        //输入结束时间
        cal.setTime(endDate);
        //在当前时间基础上减减去规定的天数
        if (timeType != null && !timeType.equals(""))
            switch (timeType) {
                case "days":
                    cal.add(Calendar.DATE, timeRange);
                    break;
                case "hours":
                    cal.add(Calendar.HOUR, timeRange);
                    break;
                case "minutes":
                    cal.add(Calendar.MINUTE, timeRange);
                    break;
                default:
                    throw new RuntimeException("没有这个时间类型");
            }
        else
            cal.add(Calendar.DATE, timeRange);

        startPage();
        return mainMapper.selectFontMonitorApiErrorMainListByTimeRange(
                projectId,
                simpleDateFormat.format(cal.getTime()),
                simpleDateFormat.format(endDate),
                env,
                true,
                false,
                null,
                null,
                null,
                null);
    }

    @Override
    public List<EnhanceMainAndXhrAndFetchVo> getApiErrorByDesignation(
            String projectId, String env, String reportTimestamp, String timeType, String range) {
        // 验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        // 格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        // 获取结束时间
        Date date = enhanceMainService.getEndDate(reportTimestamp);
        String endDate = simpleDateFormat.format(date);
        // 获取开始时间
        String preDate = simpleDateFormat.format(getPreDate(date, timeType, range));
        // 分别获取xhr和fetch数据
        List<EnhanceMainAndXhrAndFetchVo> xhrList =
                mainMapper.selectMainInnerJoinXhr(projectId, env, preDate, endDate, false, null, null, null);
        List<EnhanceMainAndXhrAndFetchVo> fetchList =
                mainMapper.selectMainInnerJoinFetch(projectId, env, preDate, endDate, false, null, null, null);
        List<EnhanceMainAndXhrAndFetchVo> allList = Stream
                .concat(xhrList.stream(), fetchList.stream())
                .sorted((o1, o2) -> (int) (o1.getId() - o2.getId()))
                .collect(Collectors.toList());
        return allList;
    }

    @Override
    public JSONObject numberOfResourceError(String projectId, String reportTimestamp, String env) {
        //获取结束时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();//格式化时间
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //创建Calendar实例
        Calendar cal = Calendar.getInstance();
        //输入结束时间
        cal.setTime(endDate);
        //在当前时间基础上减30天
        cal.add(Calendar.DATE, -30);
        //查询resourceError近30天的数据
        List<EnhanceDataStatistics> resourceErrorData = mainMapper.dataStatistics(
                DataStatisticsTimeType.EVERYDAY.getInfo(),
                simpleDateFormat.format(cal.getTime()),
                simpleDateFormat.format(endDate),
                projectId,
                2L,
                env,
                null, null
        );
        //组装返回体
        JSONObject returnData = new JSONObject();
        returnData.put("resourceError", resourceErrorData);
        return returnData;
    }

    @Override
    public List<EnhanceMainAndResourceErrorVo> getResourceErrorByDesignation(
            String projectId, String env, String reportTimestamp, String timeType, String range) {
        // 验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        // 格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        // 获取结束时间
        Date date = enhanceMainService.getEndDate(reportTimestamp);
        String endDate = simpleDateFormat.format(date);
        // 获取开始时间
        String preDate = simpleDateFormat.format(getPreDate(date, timeType, range));
        // 获取数据
        startPage();
        return mainMapper.selectMainInnerJoinResourceError(projectId, env, preDate, endDate, null);
    }

    @Override
    public JSONObject getProjectScore(String projectId, String env, String reportTimestamp) {
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd");
        //获取时间
        String endDate;
        String preDate;
        Date date;
        if (reportTimestamp == null || reportTimestamp.equals(""))
            date = enhanceMainService.getEndDate(null);
        else
            date = enhanceMainService.getEndDate(reportTimestamp);
        endDate = simpleDateFormat.format(date) + " 23:59:59";
        preDate = simpleDateFormat.format(date) + " 00:00:00";
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //静态资源错误率 resourceErrorRate
        EnhanceProjectMessage enhanceProjectMessage = new EnhanceProjectMessage();
        enhanceProjectMessage.setPreDate(preDate);
        enhanceProjectMessage.setEndDate(endDate);
        enhanceProjectMessage.setProduction(env);
        enhanceProjectMessage.setTypeId(String.valueOf(2));
        List<FontMonitorMain> resourceErrorList = mainMapper.selectFontMonitorMainListGroupBy(enhanceProjectMessage);
        enhanceProjectMessage.setTypeId(String.valueOf(3));
        List<FontMonitorMain> resourceList = mainMapper.selectFontMonitorMainListGroupBy(enhanceProjectMessage);
        Double resourceErrorRate;
        if (resourceList.size() == 0 && resourceErrorList.size() == 0)
            resourceErrorRate = Double.valueOf(0);
        else
            resourceErrorRate = Double.valueOf(resourceErrorList.size()) / (resourceList.size() + resourceErrorList.size());
        //接口异常错误率 apiErrorRate
        enhanceProjectMessage.setTypeId(String.valueOf(4));
        List<FontMonitorMain> xhrList = mainMapper.selectFontMonitorMainListGroupBy(enhanceProjectMessage);
        enhanceProjectMessage.setTypeId(String.valueOf(5));
        List<FontMonitorMain> fetchList = mainMapper.selectFontMonitorMainListGroupBy(enhanceProjectMessage);
        List<FontMonitorMain> apiErrorList = mainMapper.dataStatisticsNumberOfApiErrorAll(preDate, endDate, projectId, env, true);
        Double apiErrorRate;
        if (xhrList.size() == 0 && fetchList.size() == 0)
            apiErrorRate = Double.valueOf(0);
        else
            apiErrorRate = Double.valueOf(apiErrorList.size()) / (xhrList.size() + fetchList.size());
        //js异常错误量 jsErrorNumber
        enhanceProjectMessage.setTypeId(String.valueOf(0));
        List<FontMonitorMain> jsErrorList = mainMapper.selectFontMonitorMainListGroupBy(enhanceProjectMessage);
        Integer jsErrorNumber = jsErrorList.size();
        //promise错误量 promiseErrorNumber
        enhanceProjectMessage.setTypeId(String.valueOf(1));
        List<FontMonitorMain> promiseErrorList = mainMapper.selectFontMonitorMainListGroupBy(enhanceProjectMessage);
        Integer promiseErrorNumber = promiseErrorList.size();
        //自定义错误量 customMessageErrorNumber
        enhanceProjectMessage.setTypeId(String.valueOf(15));
        List<FontMonitorMain> customMessageList = mainMapper.selectFontMonitorMainListGroupBy(enhanceProjectMessage);
        Integer customMessageErrorNumber = customMessageList.size();
        //计算健康评分
        Double projectScore = 100 - (100 * resourceErrorRate + 100 * apiErrorRate + jsErrorNumber + promiseErrorNumber + customMessageErrorNumber) / 4;
        //组装返回体
        JSONObject returnData = new JSONObject();
        returnData.put("projectScore", projectScore);
        returnData.put("jsErrorNumber", jsErrorNumber);
        returnData.put("promiseErrorNumber", promiseErrorNumber);
        returnData.put("customMessageErrorNumber", customMessageErrorNumber);
        returnData.put("resourceErrorRate", resourceErrorRate);
        returnData.put("apiErrorRate", apiErrorRate);
        return returnData;
    }

    @Override
    public JSONObject getTwentyFourHourError(String projectId, String env, String errorType, String reportTimestamp) {
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //获取结束时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取开始时间
        Date preDate = enhanceMainService.getPreDate(endDate, Calendar.HOUR, -24);
        //一周前结束时间
        Date endDateWeekAgo = enhanceMainService.getPreDate(endDate, Calendar.DATE, -7);
        //一周前开始时间
        Date preDateWeekAgo = enhanceMainService.getPreDate(endDateWeekAgo, Calendar.HOUR, -24);
        //根据类型返回不同的数据
        JSONObject returnDate = new JSONObject();
        switch (errorType) {
            case "js":
                List<EnhanceDataStatistics> jsDataStatistics = mainMapper.dataStatistics(
                        DataStatisticsTimeType.EVERYHOUR.getInfo(),
                        simpleDateFormat.format(preDate),
                        simpleDateFormat.format(endDate),
                        projectId,
                        0L,
                        env,
                        null, null);
                List<EnhanceDataStatistics> jsDataStatisticsWeekAgo = mainMapper.dataStatistics(
                        DataStatisticsTimeType.EVERYHOUR.getInfo(),
                        simpleDateFormat.format(preDateWeekAgo),
                        simpleDateFormat.format(endDateWeekAgo),
                        projectId,
                        0L,
                        env,
                        null, null);
                returnDate.put("now", jsDataStatistics);
                returnDate.put("weekAgo", jsDataStatisticsWeekAgo);
                break;
            case "customMessage":
                List<EnhanceDataStatistics> customMessageDataStatistics = mainMapper.dataStatistics(
                        DataStatisticsTimeType.EVERYHOUR.getInfo(),
                        simpleDateFormat.format(preDate),
                        simpleDateFormat.format(endDate),
                        projectId,
                        15L,
                        env,
                        null, null);
                List<EnhanceDataStatistics> customMessageDataStatisticsWeekAgo = mainMapper.dataStatistics(
                        DataStatisticsTimeType.EVERYHOUR.getInfo(),
                        simpleDateFormat.format(preDateWeekAgo),
                        simpleDateFormat.format(endDateWeekAgo),
                        projectId,
                        15L,
                        env,
                        null, null);
                returnDate.put("now", customMessageDataStatistics);
                returnDate.put("weekAgo", customMessageDataStatisticsWeekAgo);
                break;
            case "resourceError":
                List<EnhanceDataStatistics> resourceErrorDataStatistics = mainMapper.dataStatistics(
                        DataStatisticsTimeType.EVERYHOUR.getInfo(),
                        simpleDateFormat.format(preDate),
                        simpleDateFormat.format(endDate),
                        projectId,
                        1L,
                        env,
                        null, null);
                List<EnhanceDataStatistics> resourceErrorDataStatisticsWeekAgo = mainMapper.dataStatistics(
                        DataStatisticsTimeType.EVERYHOUR.getInfo(),
                        simpleDateFormat.format(preDateWeekAgo),
                        simpleDateFormat.format(endDateWeekAgo),
                        projectId,
                        1L,
                        env,
                        null, null);
                returnDate.put("now", resourceErrorDataStatistics);
                returnDate.put("weekAgo", resourceErrorDataStatisticsWeekAgo);
                break;
            case "apiError":
                List<EnhanceDataStatistics> apiErrorDataStatistics = mainMapper.dataStatisticsNumberOfApi(
                        DataStatisticsTimeType.EVERYHOUR.getInfo(),
                        simpleDateFormat.format(preDate),
                        simpleDateFormat.format(endDate),
                        projectId,
                        env,
                        true,
                        false,
                        null,
                        null,
                        null);
                List<EnhanceDataStatistics> apiErrorDataStatisticsWeekAgo = mainMapper.dataStatisticsNumberOfApi(
                        DataStatisticsTimeType.EVERYHOUR.getInfo(),
                        simpleDateFormat.format(preDateWeekAgo),
                        simpleDateFormat.format(endDateWeekAgo),
                        projectId,
                        env,
                        true,
                        false,
                        null,
                        null,
                        null);
                returnDate.put("now", apiErrorDataStatistics);
                returnDate.put("weekAgo", apiErrorDataStatisticsWeekAgo);
                break;
            case "promise":
                List<EnhanceDataStatistics> promiseDataStatistics = mainMapper.dataStatistics(
                        DataStatisticsTimeType.EVERYHOUR.getInfo(),
                        simpleDateFormat.format(preDate),
                        simpleDateFormat.format(endDate),
                        projectId,
                        1L,
                        env,
                        null, null);
                List<EnhanceDataStatistics> promiseDataStatisticsWeekAgo = mainMapper.dataStatistics(
                        DataStatisticsTimeType.EVERYHOUR.getInfo(),
                        simpleDateFormat.format(preDateWeekAgo),
                        simpleDateFormat.format(endDateWeekAgo),
                        projectId,
                        1L,
                        env,
                        null, null);
                returnDate.put("now", promiseDataStatistics);
                returnDate.put("weekAgo", promiseDataStatisticsWeekAgo);
                break;
            default:
                throw new ServiceException("未设置相关查询");
        }
        return returnDate;
    }
}
