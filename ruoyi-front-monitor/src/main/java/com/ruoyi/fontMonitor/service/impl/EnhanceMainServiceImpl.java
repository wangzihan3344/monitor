package com.ruoyi.fontMonitor.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.fontMonitor.Common.DateUtils;
import com.ruoyi.fontMonitor.domain.EnhanceProjectMessage;
import com.ruoyi.fontMonitor.domain.FontMonitorMain;
import com.ruoyi.fontMonitor.mapper.FontMonitorMainMapper;
import com.ruoyi.fontMonitor.service.IEnhanceMainService;
import com.ruoyi.fontMonitor.service.IEnhanceProjectService;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.ruoyi.common.utils.PageUtils.startPage;

@Service
@SuppressWarnings("all")
public class EnhanceMainServiceImpl implements IEnhanceMainService {
    @Autowired
    private FontMonitorMainMapper mainMapper;

    @Autowired
    private IEnhanceProjectService enhanceProjectService;

    @Autowired
    private SysDictDataMapper sysDictDataMapper;

    public Date getEndDate(String reportTimestamp) {
        //验证时间戳合理性（时间戳要13位）
        if (reportTimestamp != null && !reportTimestamp.equals("")) {
            if (!StringUtils.isNumeric(reportTimestamp) || reportTimestamp.length() > 13)
                throw new RuntimeException("时间戳错误");
            if (reportTimestamp.length() < 13) {
                int lenth = reportTimestamp.length();
                for (int i = 0; i < 13 - lenth; i++) {
                    reportTimestamp = reportTimestamp + "0";
                }
            }
            //更具时间戳设置结束时间
            return new Date(Long.valueOf(reportTimestamp));
        } else
            //设置当前时间为结束时间
            return new Date();
    }

    @Override
    public List<FontMonitorMain> searchDataByDesignation(String projectId, String type, String env, String reportTimestamp, String timeType, String range) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //根据type去字典表查询相应的typeId
        String typeId;
        if (type != null && !type.equals("")) {
            SysDictData searchDic = new SysDictData();
            searchDic.setDictType("font_monitor_type");
            searchDic.setDictLabel(type);
            List<SysDictData> searchDicResult = sysDictDataMapper.selectDictDataList(searchDic);
            if (searchDicResult.size() == 0)
                throw new RuntimeException("没有该字典类型");
            else if (searchDicResult.size() > 1)
                throw new RuntimeException("请联系管理员检查字典表是否存在重复数据");
            typeId = searchDicResult.get(0).getDictValue();
        } else
            typeId = null;
        //获取结束时间
        Date endDate = getEndDate(reportTimestamp);
        //range必须是数字，然后转为负数
        if (range == null || range.equals(""))
            range = String.valueOf(100L);
        else if (!StringUtils.isNumeric(range))
            throw new RuntimeException("时间范围不许是数字");
        int timeRange;
        try {
            timeRange = Integer.parseInt(range);
            timeRange = -timeRange;
        } catch (Exception e) {
            throw new RuntimeException("时间范围数字不准确");
        }
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //创建Calendar实例
        Calendar cal = Calendar.getInstance();
        //输入结束时间
        cal.setTime(endDate);
        //在当前时间基础上减减去规定的天数
        if (timeType != null && !timeType.equals(""))
            switch (timeType) {
                case "days":
                    cal.add(Calendar.DATE, timeRange);
                    break;
                case "hours":
                    cal.add(Calendar.HOUR, timeRange);
                    break;
                case "minutes":
                    cal.add(Calendar.MINUTE, timeRange);
                    break;
                default:
                    throw new RuntimeException("没有这个时间类型");
            }
        else
            cal.add(Calendar.DATE, timeRange);

        startPage();
        return mainMapper.selectFontMonitorMainListByTimeRange(
                projectId,
                typeId,
                simpleDateFormat.format(cal.getTime()),
                simpleDateFormat.format(endDate),
                env);
    }

    @Override
    public JSONObject listMainByTimeAndEnvANdGroupByUuid(String projectId, String reportTimestamp, String env, String typeId) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束时间
        Date endDate = getEndDate(reportTimestamp);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //创建Calendar实例
        Calendar cal = Calendar.getInstance();
        //输入结束时间
        cal.setTime(endDate);
        //在当前时间基础上减去24小时
        cal.add(Calendar.HOUR, -24);
        EnhanceProjectMessage projectMessage = new EnhanceProjectMessage();
        projectMessage.setPid(projectId);
        projectMessage.setProduction(env);
        projectMessage.setTypeId(typeId);
        projectMessage.setPreDate(simpleDateFormat.format(cal.getTime()));
        projectMessage.setEndDate(simpleDateFormat.format(endDate));
        List<FontMonitorMain> mainList = mainMapper.selectFontMonitorMainListGroupBy(projectMessage);
        projectMessage.setGroupBy("uuid");
        List<FontMonitorMain> mainListGroupByUuid = mainMapper.selectFontMonitorMainListGroupBy(projectMessage);
        projectMessage.setGroupBy("url");
        List<FontMonitorMain> mainListGroupByUrl = mainMapper.selectFontMonitorMainListGroupBy(projectMessage);
        JSONObject returnData = new JSONObject();
        returnData.put("errorNumber", mainList.size());
        returnData.put("pageNumber", mainListGroupByUrl.size());
        returnData.put("userNumber", mainListGroupByUuid.size());
        return returnData;
    }

    @Override
    public Date getPreDate(Date endDate, int dateType, int range) {
        //创建Calendar实例
        Calendar cal = Calendar.getInstance();
        //输入结束时间
        cal.setTime(endDate);
        //在当前时间基础上指定天数
        cal.add(dateType, range);
        return cal.getTime();
    }

    @Override
    public Date getPreDateZero(Date endDate) {
        //创建Calendar实例
        Calendar cal = Calendar.getInstance();
        //输入结束时间
        cal.setTime(endDate);
        //获取年月日
        Integer year = cal.get(Calendar.YEAR);
        Integer month = cal.get(Calendar.MONTH) + 1;
        Integer day = cal.get(Calendar.DATE);
        String dateStr = year + "-" + month + "-" + day + " 00:00:00";
        try {
            return DateUtils.stringToDate(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
    }
}
