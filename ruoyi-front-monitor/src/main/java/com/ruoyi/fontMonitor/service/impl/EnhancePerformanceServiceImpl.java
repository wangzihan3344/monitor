package com.ruoyi.fontMonitor.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.fontMonitor.domain.EnhanceDataStatistics;
import com.ruoyi.fontMonitor.domain.EnhanceMainAndLoadTimeVo;
import com.ruoyi.fontMonitor.domain.EnhanceMainAndXhrAndFetchVo;
import com.ruoyi.fontMonitor.domain.FontMonitorMain;
import com.ruoyi.fontMonitor.enums.DataStatisticsTimeType;
import com.ruoyi.fontMonitor.mapper.FontMonitorMainMapper;
import com.ruoyi.fontMonitor.service.IEnhanceMainService;
import com.ruoyi.fontMonitor.service.IEnhancePerformanceService;
import com.ruoyi.fontMonitor.service.IEnhanceProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@SuppressWarnings("all")
public class EnhancePerformanceServiceImpl implements IEnhancePerformanceService {

    /**
     * 时间范围内部类,其实这玩意儿可以设置成一个数组，但是考虑到设成数组的话可能看不懂，所以用一个内部类来表示
     */
    class Duration {
        Integer durationMin;
        Integer durationMax;
    }

    @Autowired
    private FontMonitorMainMapper mainMapper;

    @Autowired
    private IEnhanceMainService enhanceMainService;

    @Autowired
    private IEnhanceProjectService enhanceProjectService;

    /**
     * 设置持续时间的范围
     *
     * @param sub 持续时间类型
     * @return 最大和最小时间
     */
    public Duration getDuration(Integer sub) {
        Duration duration = new Duration();
        //设置持续时间的范围
        if (sub == null)
            throw new ServiceException("耗时分段不能为空");
        duration.durationMin = null;
        duration.durationMax = null;
        switch (sub) {
            case 0:
                duration.durationMin = 0;
                duration.durationMax = 1000;
                break;
            case 1:
                duration.durationMin = 1000;
                duration.durationMax = 5000;
                break;
            case 2:
                duration.durationMin = 5000;
                duration.durationMax = 10000;
                break;
            case 3:
                duration.durationMin = 10000;
                duration.durationMax = 30000;
                break;
            case 4:
                duration.durationMin = 30000;
                break;
            default:
                throw new ServiceException("没有这个类型");
        }
        return duration;
    }

    @Override
    public JSONObject getApiNumberAndPercentage(String projectId, Integer sub, String reportTimestamp, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd");
        //获取结束和开始时间
        Date date = enhanceMainService.getEndDate(reportTimestamp);
        String preDate = simpleDateFormat.format(date) + " 00:00:00";
        String endDate = simpleDateFormat.format(date) + " 23:59:59";
        //设置持续时间的范围
        Duration duration = getDuration(sub);
        //获取耗时分段的列表
        List<FontMonitorMain> mainList = mainMapper.selectFontMonitorApiErrorMainListByTimeRange(
                projectId,
                preDate,
                endDate,
                env,
                true,
                true,
                duration.durationMin,
                duration.durationMax,
                null,
                null);
        //数据总和
        List<FontMonitorMain> mainListAll = mainMapper.selectFontMonitorApiErrorMainListByTimeRange(
                projectId,
                preDate,
                endDate,
                env,
                true,
                true,
                0,
                null,
                null,
                null);
        //组装返回体
        JSONObject returnData = new JSONObject();
        returnData.put("mainListNumber", mainList.size());
        if (mainListAll.size() == 0)
            returnData.put("mainListPercentage", 0);
        else
            returnData.put("mainListPercentage", Double.valueOf(mainList.size()) / mainListAll.size());
        return returnData;
    }

    @Override
    public List<EnhanceDataStatistics> getThirtyDaysApiNumber(String projectId, Integer sub, String reportTimestamp, String env, String timeType, Integer range) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //验证range合理性
        if (timeType == null || timeType.equals(""))
            timeType = "days";
        if (range == null)
            range = 30;
        if (range <= 0)
            throw new ServiceException("时间范围不合理");
        //获取结束和开始时间,同时设置一下需要传入数据库的dateType
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate;
        String dateType;
        switch (timeType) {
            case "days":
                dateType = DataStatisticsTimeType.EVERYDAY.getInfo();
                preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -range);
                break;
            case "hours":
                dateType = DataStatisticsTimeType.EVERYHOUR.getInfo();
                preDate = enhanceMainService.getPreDate(endDate, Calendar.HOUR, -range);
                break;
            case "minutes":
                dateType = DataStatisticsTimeType.EVERYMINUTES.getInfo();
                preDate = enhanceMainService.getPreDate(endDate, Calendar.MINUTE, -range);
                break;
            default:
                throw new ServiceException("暂未设置该类型");
        }
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //设置持续时间的范围
        Duration duration = getDuration(sub);
        //获取接口请求量
        List<EnhanceDataStatistics> dataStatisticsList = mainMapper.dataStatisticsNumberOfApi(
                dateType,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                projectId,
                env,
                true,
                true,
                duration.durationMin,
                duration.durationMax,
                null);
        return dataStatisticsList;
    }

    @Override
    public JSONObject getApiMessage(String projectId, Integer sub, String reportTimestamp, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束和开始时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -1);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //设置持续时间的范围
        Duration duration = getDuration(sub);
        //分别计算xhr和fetch的和
        Integer xhrSum = mainMapper.selectDurationSumFromXhr(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, true, null);
        Integer fetchSum = mainMapper.selectDurationSumFromFetch(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, true, null);
        if (xhrSum == null)
            xhrSum = 0;
        if (fetchSum == null)
            fetchSum = 0;
        //根据页面归类
        List<EnhanceMainAndXhrAndFetchVo> mainListUrl = mainMapper.selectFontMonitorXhrJoinMainListByTimeRange(
                projectId,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                env,
                true,
                true,
                duration.durationMin,
                duration.durationMax,
                null,
                null,
                null,
                null,
                null,
                "address");
        //组装返回体
        System.out.println(mainListUrl);
        JSONObject returnData = new JSONObject();

        returnData.put("apiList", mainListUrl);
        return returnData;
    }

    @Override
    public JSONObject getApiInfo(String address, String projectId, Integer sub, String reportTimestamp, String env) throws UnsupportedEncodingException {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束和开始时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -1);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //设置持续时间的范围
        Duration duration = getDuration(sub);
        //分别计算xhr和fetch的和
        Integer xhrSum = mainMapper.selectDurationSumFromXhr(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, true, null);
        Integer fetchSum = mainMapper.selectDurationSumFromFetch(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, true, null);
        if (xhrSum == null)
            xhrSum = 0;
        if (fetchSum == null)
            fetchSum = 0;
        List<EnhanceMainAndXhrAndFetchVo> mainId2Api;
        //根据页面归类
        List<EnhanceMainAndXhrAndFetchVo> mainListUrl = mainMapper.selectFontMonitorXhrJoinMainListByTimeRange(
                projectId,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                env,
                true,
                true,
                duration.durationMin,
                duration.durationMax,
                null,
                null,
                null,
                null,
                URLEncoder.encode(address, "UTF-8"),
                null);
        //根据用戶归类
        Map<String, Integer> uuidMap = new HashMap<>();
        Map<String, Integer> pageMap = new HashMap<>();
        double druationSum = 0;

        for(int i = 0 ; i < mainListUrl.size() ; i++){
            String uuid = mainListUrl.get(i).getUuid();
            uuidMap.put(uuid,uuidMap.get(uuid) == null ? (Integer) 1 : (Integer) uuidMap.get(uuid) + 1);
            String url = mainListUrl.get(i).getUrl();
            pageMap.put(url,pageMap.get(url) == null ? (Integer) 1 : (Integer) pageMap.get(url) + 1);
            druationSum += mainListUrl.get(i).getDuration();
        }
        //组装返回体

        JSONObject returnData = new JSONObject();
        if (mainListUrl.size() == 0)
            returnData.put("averageTime", 0);
        else
        returnData.put("averageTime", Double.valueOf(druationSum) / (mainListUrl.size()));
        returnData.put("usrerNumber", uuidMap.size());
        returnData.put("urlNumber", pageMap.size());
        List<Map<String, Object>> list = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : pageMap.entrySet()) {
            Map<String, Object> keyValueMap = new HashMap<>();
            keyValueMap.put("key", entry.getKey());
            keyValueMap.put("value", entry.getValue());
            list.add(keyValueMap);
        }
        returnData.put("pageList", list);
        return returnData;
    }


    @Override
    public JSONObject getPageNumberAndPercentage(String projectId, Integer sub, String reportTimestamp, String env, String timeType, Integer range) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //验证range合理性
        if (timeType == null || timeType.equals(""))
            timeType = "days";
        if (range == null)
            range = 30;
        if (range <= 0)
            throw new ServiceException("时间范围不合理");
        //获取结束和开始时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate;
        switch (timeType) {
            case "days":
                preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -range);
                break;
            case "hours":
                preDate = enhanceMainService.getPreDate(endDate, Calendar.HOUR, -range);
                break;
            case "minutes":
                preDate = enhanceMainService.getPreDate(endDate, Calendar.MINUTE, -range);
                break;
            default:
                throw new ServiceException("暂未设置该类型");
        }
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //设置持续时间的范围
        Duration duration = getDuration(sub);
        //获取耗时分段的列表
        List<FontMonitorMain> mainList = mainMapper.selectFontMonitorMainListWithLoadTime(
                projectId,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                env,
                null,
                true,
                duration.durationMin,
                duration.durationMax
        );
        //获取数据总和
        List<FontMonitorMain> mainListAll = mainMapper.selectFontMonitorMainListWithLoadTime(
                projectId,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                env,
                null,
                true,
                0,
                null
        );
        //组装返回体
        JSONObject returnData = new JSONObject();
        returnData.put("mainListNumber", mainList.size());
        if (mainListAll.size() == 0)
            returnData.put("mainListPercentage", 0);
        else
            returnData.put("mainListPercentage", Double.valueOf(mainList.size()) / mainListAll.size());
        return returnData;
    }

    @Override
    public List<EnhanceDataStatistics> getPageNumberByTimeRange(String projectId, Integer sub, String reportTimestamp, String env, String timeType, Integer range) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //验证range合理性
        if (timeType == null || timeType.equals(""))
            timeType = "days";
        if (range == null)
            range = 30;
        if (range <= 0)
            throw new ServiceException("时间范围不合理");
        //获取结束和开始时间,同时设置一下需要传入数据库的dateType
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate;
        String dateType;
        switch (timeType) {
            case "days":
                dateType = DataStatisticsTimeType.EVERYDAY.getInfo();
                preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -range);
                break;
            case "hours":
                dateType = DataStatisticsTimeType.EVERYHOUR.getInfo();
                preDate = enhanceMainService.getPreDate(endDate, Calendar.HOUR, -range);
                break;
            case "minutes":
                dateType = DataStatisticsTimeType.EVERYMINUTES.getInfo();
                preDate = enhanceMainService.getPreDate(endDate, Calendar.MINUTE, -range);
                break;
            default:
                throw new ServiceException("暂未设置该类型");
        }
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //设置持续时间的范围
        Duration duration = getDuration(sub);
        List<EnhanceDataStatistics> enhanceDataStatisticsList = mainMapper.selectFontMonitorMainWithLoadTimeDataStatisticsNumber(
                dateType,
                projectId,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                env,
                null,
                true,
                duration.durationMin,
                duration.durationMax
        );
        return enhanceDataStatisticsList;
    }

    @Override
    public JSONObject getPageMessage(String projectId, Integer sub, String reportTimestamp, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束和开始时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -1);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //设置持续时间的范围
        Duration duration = getDuration(sub);
        //根据页面归类
        List<FontMonitorMain> mainListUrl = mainMapper.selectFontMonitorMainListWithLoadTime(
                projectId,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                env,
                "url",
                true,
                duration.durationMin,
                duration.durationMax);
        //组装返回体
        JSONObject returnData = new JSONObject();
        returnData.put("pageList", mainListUrl);
        return returnData;
    }
    @Override
    public JSONObject getPageInfo(String url, String projectId, Integer sub, String reportTimestamp, String env) throws UnsupportedEncodingException {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束和开始时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -1);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //设置持续时间的范围
        Duration duration = getDuration(sub);

        //根据页面归类
        List<EnhanceMainAndLoadTimeVo> mainListUrl = mainMapper.selectFontMonitorMainListWithLoadTimeAndUrl(
                URLEncoder.encode(url, "UTF-8"),
                projectId,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                env,
                duration.durationMin,
                duration.durationMax);
        //根据用戶归类
        Map<String, Integer> uuidMap = new HashMap<>();
        Map<String, Integer> pageMap = new HashMap<>();
        double connectDruationSum = 0;
        double ttfbtDruationSum = 0;
        double responseDruationSum = 0;
        double parseDomDruationSum = 0;
        double loadDomDruationSum = 0;
        double firstInteractiveDruationSum = 0;
        double loadTimeDruationSum = 0;

        for(int i = 0 ; i < mainListUrl.size() ; i++){
             connectDruationSum += Double.parseDouble(mainListUrl.get(i).getConnectTime());
             ttfbtDruationSum += Double.parseDouble(mainListUrl.get(i).getTtfbTime());
             responseDruationSum += Double.parseDouble(mainListUrl.get(i).getResponseTime());
             parseDomDruationSum += Double.parseDouble(mainListUrl.get(i).getParseDomTime());
             loadDomDruationSum += mainListUrl.get(i).getLoadTime();
             firstInteractiveDruationSum += Double.parseDouble(mainListUrl.get(i).getTimeToInteractive());
             loadTimeDruationSum += mainListUrl.get(i).getLoadTime();
             String uuid = mainListUrl.get(i).getUuid();                                                    
             uuidMap.put(uuid,uuidMap.get(uuid) == null ? (Integer) 1 : (Integer) uuidMap.get(uuid) + 1);   
        }
        //组装返回体

        JSONObject returnData = new JSONObject();
        if (mainListUrl.size() == 0) {

            returnData.put("averageConnectTime",0) ;
            returnData.put("averageTTFBTime",0) ;
            returnData.put("averageResponseTime",0);
            returnData.put("averageParseDomTime",0);
            returnData.put("averageLoadDomTime",0);
            returnData.put("averageTTI",0);
            returnData.put("averageLoadTimeTime",0);
        }else{
            returnData.put("averageConnectTime", Double.valueOf(connectDruationSum) / (mainListUrl.size()));
            returnData.put("averageTTFBTime", Double.valueOf(ttfbtDruationSum) / (mainListUrl.size()));
            returnData.put("averageResponseTime", Double.valueOf(responseDruationSum) / (mainListUrl.size()));
            returnData.put("averageParseDomTime", Double.valueOf(parseDomDruationSum) / (mainListUrl.size()));
            returnData.put("averageLoadDomTime", Double.valueOf(loadDomDruationSum) / (mainListUrl.size()));
            returnData.put("averageTTI", Double.valueOf(firstInteractiveDruationSum) / (mainListUrl.size()));
            returnData.put("averageLoadTimeTime", Double.valueOf(loadTimeDruationSum) / (mainListUrl.size()));
        }
        returnData.put("userNumber", uuidMap.size());
        returnData.put("sampleNumber", mainListUrl.size());
        return returnData;
    }


    @Override
    public JSONObject getPageAVGTime(String projectId, String reportTimestamp, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束和开始时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -7);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //分別求7天内TTFB、Dom解析、页面加载平均时间
        Integer ttfbTimeAvg = mainMapper.selectDataFromLoadTimeGetAVG(
                "ttfb_time",
                projectId,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                env,
                null,
                null);
        if (ttfbTimeAvg == null)
            ttfbTimeAvg = 0;
        Integer parseDomTimeAvg = mainMapper.selectDataFromLoadTimeGetAVG(
                "parse_dom_time",
                projectId,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                env,
                null,
                null);
        if (parseDomTimeAvg == null)
            parseDomTimeAvg = 0;
        Integer allTimeAvg = mainMapper.selectDataFromLoadTimeGetAVG(
                "load_time",
                projectId,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                env,
                null,
                null);
        if (allTimeAvg == null)
            allTimeAvg = 0;
        //组装返回体
        JSONObject returnData = new JSONObject();
        returnData.put("ttfbTimeAvg", ttfbTimeAvg);
        returnData.put("parseDomTimeAvg", parseDomTimeAvg);
        returnData.put("allTimeAvg", allTimeAvg);
        return returnData;
    }

    @Override
    public JSONObject getApiAVGTime(String projectId, String reportTimestamp, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束和开始时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -7);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //7天内总接口请求量、接口请求平均耗时、接口请求成功率
        List<FontMonitorMain> mainListInSevenDays = mainMapper.selectFontMonitorApiErrorMainListByTimeRange(
                projectId,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                env,
                true,
                null,
                null,
                null,
                null,
                null
        );
        //分别计算xhr和fetch的和
        Integer xhrSum = mainMapper.selectDurationSumFromXhr(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, null, null);
        Integer fetchSum = mainMapper.selectDurationSumFromFetch(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, null, null);
        if (xhrSum == null)
            xhrSum = 0;
        if (fetchSum == null)
            fetchSum = 0;
        List<FontMonitorMain> mainListSuccessInSevenDays = mainMapper.selectFontMonitorApiErrorMainListByTimeRange(
                projectId,
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                env,
                true,
                true,
                null,
                null,
                null,
                null
        );
        //组装返回体
        JSONObject returnData = new JSONObject();
        returnData.put("numberInSevenDays", mainListInSevenDays.size());
        if (xhrSum == 0 && fetchSum == 0)
            returnData.put("averageTime", 0);
        else
            returnData.put("averageTime", Double.valueOf(xhrSum + fetchSum) / mainListInSevenDays.size());
        if (mainListInSevenDays.size() == 0)
            returnData.put("successPre", 0);
        else
            returnData.put("successPre", Double.valueOf(mainListSuccessInSevenDays.size()) / mainListInSevenDays.size());
        return returnData;
    }

    @Override
    public JSONObject getPageNumberInSevenDaysBySub(String projectId, String reportTimestamp, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束和开始时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -7);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //每一个时间段的数量
        Duration duration = getDuration(0);
        List<FontMonitorMain> mainList0 = mainMapper.selectFontMonitorMainListWithLoadTime(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, null, true, duration.durationMin, duration.durationMax);
        duration = getDuration(1);
        List<FontMonitorMain> mainList1 = mainMapper.selectFontMonitorMainListWithLoadTime(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, null, true, duration.durationMin, duration.durationMax);
        duration = getDuration(2);
        List<FontMonitorMain> mainList2 = mainMapper.selectFontMonitorMainListWithLoadTime(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, null, true, duration.durationMin, duration.durationMax);
        duration = getDuration(3);
        List<FontMonitorMain> mainList3 = mainMapper.selectFontMonitorMainListWithLoadTime(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, null, true, duration.durationMin, duration.durationMax);
        duration = getDuration(4);
        List<FontMonitorMain> mainList4 = mainMapper.selectFontMonitorMainListWithLoadTime(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, null, true, duration.durationMin, duration.durationMax);
        //组装返回体
        JSONObject returnData = new JSONObject();
        returnData.put("number0", mainList0.size());
        returnData.put("number1", mainList1.size());
        returnData.put("number2", mainList2.size());
        returnData.put("number3", mainList3.size());
        returnData.put("number4", mainList4.size());
        return returnData;
    }

    @Override
    public JSONObject getApiNumberInSevenDaysBySub(String projectId, String reportTimestamp, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束和开始时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -1);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //每一个时间段的数量
        Duration duration = getDuration(0);
        List<FontMonitorMain> mainList0 = mainMapper.selectFontMonitorApiErrorMainListByTimeRange(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, true, null, duration.durationMin, duration.durationMax, null, null);
        duration = getDuration(1);
        List<FontMonitorMain> mainList1 = mainMapper.selectFontMonitorApiErrorMainListByTimeRange(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, true, null, duration.durationMin, duration.durationMax, null, null);
        duration = getDuration(2);
        List<FontMonitorMain> mainList2 = mainMapper.selectFontMonitorApiErrorMainListByTimeRange(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, true, null, duration.durationMin, duration.durationMax, null, null);
        duration = getDuration(3);
        List<FontMonitorMain> mainList3 = mainMapper.selectFontMonitorApiErrorMainListByTimeRange(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, true, null, duration.durationMin, duration.durationMax, null, null);
        duration = getDuration(4);
        List<FontMonitorMain> mainList4 = mainMapper.selectFontMonitorApiErrorMainListByTimeRange(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, true, null, duration.durationMin, duration.durationMax, null, null);
        //组装返回体
        JSONObject returnData = new JSONObject();
        returnData.put("number0", mainList0.size());
        returnData.put("number1", mainList1.size());
        returnData.put("number2", mainList2.size());
        returnData.put("number3", mainList3.size());
        returnData.put("number4", mainList4.size());
        return returnData;
    }

    @Override
    public List<EnhanceMainAndLoadTimeVo> getTopByPage(String projectId, String reportTimestamp, String env, Long number) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        // 如果时间戳不为空，返回当天数据
        String preDate = null;
        String endDate = null;
        if (reportTimestamp != null && !reportTimestamp.equals("")) {
            // 获取传入时间
            Date date = enhanceMainService.getEndDate(reportTimestamp);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            Integer year = calendar.get(Calendar.YEAR);
            Integer month = calendar.get(Calendar.MONTH) + 1;
            Integer day = calendar.get(Calendar.DATE);
            // 设置统计开始时间和结束时间
            preDate = year + "-" + month + "-" + day + " 00:00:00";
            endDate = year + "-" + month + "-" + day + " 23:59:59";
        }
        // 如果没有数量限制，默认前10条
        if (number == null)
            number = 10L;
        // 获取所有数据
        List<EnhanceMainAndLoadTimeVo> enhanceMainAndLoadTimeVoList = mainMapper.selectMainLeftJoinLoadTime(projectId, env, preDate, endDate, null, null, null);
        // 排序并限制
        enhanceMainAndLoadTimeVoList = enhanceMainAndLoadTimeVoList.stream()
//                .map((EnhanceMainAndLoadTimeVo enhanceMainAndLoadTimeVo) -> Float.valueOf(enhanceMainAndLoadTimeVo.getLoadTime()))
//                .sorted(Comparator.comparing(Float::floatValue).reversed())
                .sorted(Comparator.comparing(enhanceMainAndLoadTimeVo -> enhanceMainAndLoadTimeVo.getLoadTime(), Comparator.reverseOrder()))
                .limit(number)
                .collect(Collectors.toList());
        return enhanceMainAndLoadTimeVoList;
    }

    @Override
    public List<EnhanceMainAndXhrAndFetchVo> getTopByApi(String projectId, String reportTimestamp, String env, Long number) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        // 如果时间戳不为空，返回当天数据
        String preDate = null;
        String endDate = null;
        if (reportTimestamp != null && !reportTimestamp.equals("")) {
            // 获取传入时间
            Date date = enhanceMainService.getEndDate(reportTimestamp);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            Integer year = calendar.get(Calendar.YEAR);
            Integer month = calendar.get(Calendar.MONTH) + 1;
            Integer day = calendar.get(Calendar.DATE);
            // 设置统计开始时间和结束时间
            preDate = year + "-" + month + "-" + day + " 00:00:00";
            endDate = year + "-" + month + "-" + day + " 23:59:59";
        }
        // 如果没有数量限制，默认前10条
        if (number == null)
            number = 10L;
        // 获取所有数据，分别对xhr和fetch进行联查，然后在用stream流聚合
        List<EnhanceMainAndXhrAndFetchVo> mainAndXhrList = mainMapper.selectMainInnerJoinXhr(projectId, env, preDate, endDate, null, null, null, null);
        List<EnhanceMainAndXhrAndFetchVo> mainAndFetchList = mainMapper.selectMainInnerJoinFetch(projectId, env, preDate, endDate, null, null, null, null);
        List<EnhanceMainAndXhrAndFetchVo> collectList = Stream.concat(mainAndXhrList.stream(), mainAndFetchList.stream())
                .collect(Collectors.toList());
        // 排序并限制
        collectList = collectList.stream()
                .sorted(Comparator.comparing(collect ->collect.getDuration(),Comparator.reverseOrder()))
                .limit(number)
                .collect(Collectors.toList());
        return collectList;
    }
}
