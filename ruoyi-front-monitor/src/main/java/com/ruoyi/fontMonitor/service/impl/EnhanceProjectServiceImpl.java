package com.ruoyi.fontMonitor.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.fontMonitor.domain.FontMonitorMain;
import com.ruoyi.fontMonitor.domain.FontMonitorUserTeam;
import com.ruoyi.fontMonitor.domain.FontMonitorUserTeamProject;
import com.ruoyi.fontMonitor.mapper.FontMonitorUserTeamMapper;
import com.ruoyi.fontMonitor.mapper.FontMonitorUserTeamProjectMapper;
import com.ruoyi.fontMonitor.service.IEnhanceProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@SuppressWarnings("all")
public class EnhanceProjectServiceImpl implements IEnhanceProjectService {
    @Autowired
    private FontMonitorUserTeamProjectMapper projectMapper;

    @Autowired
    private FontMonitorUserTeamMapper teamMapper;

    @Override
    public FontMonitorUserTeamProject createProject(FontMonitorUserTeamProject projectMessage) {
        //查空
        if (projectMessage.getProjectName() == null || projectMessage.getProjectName().equals(""))
            throw new RuntimeException("项目名称不能为空");
        if (projectMessage.getTeamId() == null)
            throw new RuntimeException("团队Id不能为空");
        //验证团队存在性
        FontMonitorUserTeam team = teamMapper.selectFontMonitorUserTeamById(projectMessage.getTeamId());
        if (team == null)
            throw new RuntimeException("团队不存在");
        //设置pid
        projectMessage.setProjectId(getProjectId());
        projectMessage.setCreateTime(new Date());
        //默认启动
        projectMessage.setStatus("1");
        projectMapper.insertFontMonitorUserTeamProject(projectMessage);
        return projectMessage;
    }

    /**
     * 创建pid
     *
     * @return pid
     */
    public String getProjectId() {
        //用hashcode和时间戳生成pid
        String projectId = "";
        projectId = projectId + Math.abs("projectId".hashCode());
        projectId = projectId + System.currentTimeMillis();
        //一般pid不会重复，但是以防万一用生成的pid查一下，如果已有数据，进行递归，知道不重复为止
        try {
            FontMonitorUserTeamProject searchResult = searchProjectByPid(projectId);
            if (searchResult != null)
                projectId = getProjectId();
        } catch (RuntimeException e) {
            if (e.getMessage().equals("有多条数据，请联系管理员"))
                projectId = getProjectId();
        }
        return projectId;
    }

    @Override
    public FontMonitorUserTeamProject searchProjectByPid(String pid) {
        FontMonitorUserTeamProject project = new FontMonitorUserTeamProject();
        project.setProjectId(pid);
        List<FontMonitorUserTeamProject> searchResult = projectMapper.selectFontMonitorUserTeamProjectList(project);
        if (searchResult.size() == 0)
            throw new RuntimeException("项目不存在");
        if (searchResult.size() != 1)
            throw new RuntimeException("有多条数据，请联系管理员");
        return searchResult.get(0);
    }

    @Override
    public JSONObject applicationTraffic(FontMonitorMain monitorMain) {
        return null;
    }
}
