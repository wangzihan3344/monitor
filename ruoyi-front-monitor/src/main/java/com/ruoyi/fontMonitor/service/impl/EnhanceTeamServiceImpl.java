package com.ruoyi.fontMonitor.service.impl;

import com.ruoyi.fontMonitor.service.IEnhanceUserInfoService;
import org.apache.commons.lang3.StringUtils;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.fontMonitor.domain.FontMonitorUserConnectTeam;
import com.ruoyi.fontMonitor.domain.FontMonitorUserMessage;
import com.ruoyi.fontMonitor.domain.FontMonitorUserTeam;
import com.ruoyi.fontMonitor.mapper.FontMonitorUserConnectTeamMapper;
import com.ruoyi.fontMonitor.mapper.FontMonitorUserMessageMapper;
import com.ruoyi.fontMonitor.mapper.FontMonitorUserTeamMapper;
import com.ruoyi.fontMonitor.service.IEnhanceTeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@SuppressWarnings("all")
public class EnhanceTeamServiceImpl implements IEnhanceTeamService {
    @Autowired
    private FontMonitorUserTeamMapper teamMapper;

    @Autowired
    private FontMonitorUserConnectTeamMapper userConnectTeamMapper;

    @Autowired
    private FontMonitorUserMessageMapper userMessageMapper;

    @Autowired
    private IEnhanceUserInfoService enhanceUserInfoService;

    @Override
    @Transactional
    public FontMonitorUserTeam createTeam(JSONObject team) {
        //这个方法后期要进行改写，通过token拿到创建者的信息
        String teamCreater = team.getString("teamCreater");
        //查空
        if (teamCreater == null || teamCreater.equals(""))
            throw new RuntimeException("团队创建者为空");
        if (team.getString("teamName") == null || team.getString("teamName").equals(""))
            throw new RuntimeException("团队名称为空");
        //查询用户是否存在
        FontMonitorUserMessage searchUserMessage = enhanceUserInfoService.searchUserMesByEmail(teamCreater);
        //将团队信息进行保存
        FontMonitorUserTeam insertTeamData = new FontMonitorUserTeam();
        insertTeamData.setTeamName(team.getString("teamName"));
        //默认团队状态为启用
        insertTeamData.setStatus("1");
        insertTeamData.setCreateBy(teamCreater);
        insertTeamData.setCreateTime(new Date());
        teamMapper.insertFontMonitorUserTeam(insertTeamData);
        //将创建者与团队绑定
        FontMonitorUserConnectTeam insertUserConnectTeam = new FontMonitorUserConnectTeam();
        insertUserConnectTeam.setTeamId(insertTeamData.getId());
        insertUserConnectTeam.setUserId(searchUserMessage.getId());
        //创建者默认拥有管理者权限
        insertUserConnectTeam.setPermission("1");
        insertUserConnectTeam.setCreateTime(new Date());
        //默认为启用
        insertUserConnectTeam.setStatus("1");
        userConnectTeamMapper.insertFontMonitorUserConnectTeam(insertUserConnectTeam);
        return insertTeamData;
    }

    @Override
    @Transactional
    public JSONObject addTeamMember(JSONObject teamAndMember) {
        //这个方法后期要进行改写，通过token拿到管理员的信息
        String teamAdministrators = teamAndMember.getString("teamAdministrators");
        //查空以及排除不符合要求的数据
        String teamId = teamAndMember.getString("teamId");
        if (teamId == null || teamId.equals("") || !StringUtils.isNumeric(teamId))
            throw new RuntimeException("团队Id为空或者非数字");
        String usrId = teamAndMember.getString("usrId");
        if (!StringUtils.isNumeric(usrId))
            throw new RuntimeException("用户Id为空或者非数字");
        String usrEmail = teamAndMember.getString("usrEmail");
        //用户Id和用户邮箱至少存在一个(好像不会触发了)
        if ((usrId == null || usrId.equals("")) && (usrEmail == null || usrEmail.equals("")))
            throw new RuntimeException("用户Id和用户邮箱至少存在一个");
        //查询添加用户信息
        FontMonitorUserMessage userMessage;
        if (usrId != null && !usrId.equals("")) {
            userMessage = userMessageMapper.selectFontMonitorUserMessageById(Long.valueOf(usrId));
            if (userMessage == null)
                throw new RuntimeException("未查询到相关用户记录");
        } else {
            userMessage = enhanceUserInfoService.searchUserMesByEmail(usrEmail);
        }
        //如果用户Id和用户邮箱都有，则进行对比，使用上文通过用户Id查询到的信息和传入的用户邮箱对比
        //不过一般不会出现这种情况
        if (usrId != null && !usrId.equals("") && usrEmail != null && !usrEmail.equals("") && !usrEmail.equals(userMessage.getEmail()))
            throw new RuntimeException("用户Id和用户邮箱不匹配");
        //查询团队Id是否存在
        FontMonitorUserTeam searchTeam = teamMapper.selectFontMonitorUserTeamById(Long.valueOf(teamId));
        if (searchTeam == null)
            throw new RuntimeException("相关团队不存在");
        //查询操作者的权限
        FontMonitorUserMessage administrators = enhanceUserInfoService.searchUserMesByEmail(teamAdministrators);
        FontMonitorUserConnectTeam searchUserPermission = new FontMonitorUserConnectTeam();
        searchUserPermission.setTeamId(Long.valueOf(teamId));
        searchUserPermission.setUserId(administrators.getId());
        List<FontMonitorUserConnectTeam> searchPermissionResult = userConnectTeamMapper.selectFontMonitorUserConnectTeamList(searchUserPermission);
        if (searchPermissionResult.size() == 0)
            throw new RuntimeException("未查询到相关添加记录");
        if (Long.valueOf(searchPermissionResult.get(0).getPermission()) > 1)
            throw new RuntimeException("对不起，您没有权限添加成员");
        //添加记录相关的信息
        FontMonitorUserConnectTeam insertData = new FontMonitorUserConnectTeam();
        insertData.setTeamId(Long.valueOf(teamId));
        insertData.setUserId(userMessage.getId());
        //添加的成员不能是团队已有的成员(通过用户Id和团队Id查询)
        List<FontMonitorUserConnectTeam> searchResult = userConnectTeamMapper.selectFontMonitorUserConnectTeamList(insertData);
        if (searchResult.size() != 0)
            throw new RuntimeException("不能重复添加成员");
        //添加的成员默认普通用户
        insertData.setPermission("2");
        insertData.setCreateTime(new Date());
        //状态默认为启动
        insertData.setStatus("0");
        userConnectTeamMapper.insertFontMonitorUserConnectTeam(insertData);
        return null;
    }
}
