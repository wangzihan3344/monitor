package com.ruoyi.fontMonitor.service.impl;

import com.ruoyi.fontMonitor.Common.MD5;
import com.ruoyi.fontMonitor.domain.FontMonitorUserMessage;
import com.ruoyi.fontMonitor.mapper.FontMonitorUserMessageMapper;
import com.ruoyi.fontMonitor.service.IEnhanceUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@SuppressWarnings("all")
public class EnhanceUserInfoImpl implements IEnhanceUserInfoService {
    @Autowired
    private FontMonitorUserMessageMapper userMessageMapper;

    @Override
    public void userLogin(FontMonitorUserMessage userMessage) {
        //判空
        if (userMessage.getEmail() == null || userMessage.getEmail().trim().equals(""))
            throw new RuntimeException("请输入登录邮箱");
        if (userMessage.getPassword() == null || userMessage.getPassword().trim().equals(""))
            throw new RuntimeException("请输入登录密码");
        //通过邮箱查询用户信息
        FontMonitorUserMessage searchUser = new FontMonitorUserMessage();
        searchUser.setEmail(userMessage.getEmail());
        List<FontMonitorUserMessage> searchResult = userMessageMapper.selectFontMonitorUserMessageList(searchUser);
        if (searchResult.size() == 0)
            throw new RuntimeException("没有该用户");
        //判断密码
        try {
            if (!MD5.verify(userMessage.getPassword(), searchResult.get(0).getPassword()))
                throw new RuntimeException("密码错误");
        } catch (Exception e) {
            if (e.getMessage().equals("密码错误"))
                throw new RuntimeException("密码错误");
            e.printStackTrace();
        }
    }

    @Override
    public FontMonitorUserMessage userRegister(FontMonitorUserMessage userMessage) {
        //查空
        if (userMessage.getUserName() == null || userMessage.getUserName().trim().equals(""))
            throw new RuntimeException("用户昵称不能为空");
        if (userMessage.getEmail() == null || userMessage.getEmail().trim().equals(""))
            throw new RuntimeException("登录邮箱不能为空");
        if (userMessage.getPassword() == null || userMessage.getPassword().trim().equals(""))
            throw new RuntimeException("登录密码不能为空");
        //email查重
        FontMonitorUserMessage searchEmail = new FontMonitorUserMessage();
        searchEmail.setEmail(userMessage.getEmail());
        List<FontMonitorUserMessage> searchResult = userMessageMapper.selectFontMonitorUserMessageList(searchEmail);
        if (searchResult.size() != 0)
            throw new RuntimeException("该邮箱已被注册");
        //密码加密
        try {
            userMessage.setPassword(MD5.md5(userMessage.getPassword()));
        } catch (Exception e) {
            System.out.println("加密失败");
            e.printStackTrace();
        }
        //数据储存
        userMessage.setStatus("0");
        userMessage.setCreateTime(new Date());
        userMessageMapper.insertFontMonitorUserMessage(userMessage);
        return userMessage;
    }

    @Override
    public FontMonitorUserMessage searchUserMesByEmail(String Email) {
        FontMonitorUserMessage searchUser = new FontMonitorUserMessage();
        searchUser.setEmail(Email);
        List<FontMonitorUserMessage> searchResult = userMessageMapper.selectFontMonitorUserMessageList(searchUser);
        if (searchResult.size() == 0)
            throw new RuntimeException("不存在该用户");
        if (searchResult.size() > 1)
            throw new RuntimeException("Email数据重复，存在重复账户，请联系管理员");
        return searchResult.get(0);
    }
}
