package com.ruoyi.fontMonitor.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.fontMonitor.domain.*;
import com.ruoyi.fontMonitor.enums.DataStatisticsTimeType;
import com.ruoyi.fontMonitor.mapper.FontMonitorMainMapper;
import com.ruoyi.fontMonitor.service.IEnhanceMainService;
import com.ruoyi.fontMonitor.service.IEnhanceProjectService;
import com.ruoyi.fontMonitor.service.IEnhanceUserService;
import com.ruoyi.fontMonitor.utils.BeanCopyUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.ruoyi.common.utils.PageUtils.startPage;

@Service
@SuppressWarnings("all")
public class EnhanceUserServiceImpl implements IEnhanceUserService {
    @Autowired
    private FontMonitorMainMapper mainMapper;

    @Autowired
    private IEnhanceProjectService enhanceProjectService;

    @Autowired
    private IEnhanceMainService enhanceMainService;

    @Override
    public JSONObject statisticsThirtyDaysInUser(String projectId, String reportTimestamp, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束和开始时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -30);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //分别查询30天新老用户数
        List<EnhanceDataStatistics> newUserNumber = mainMapper.dataStatistics(
                DataStatisticsTimeType.EVERYDAY.getInfo(),
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                projectId,
                null,
                env,
                0, null);
        List<EnhanceDataStatistics> oldUserNumber = mainMapper.dataStatistics(
                DataStatisticsTimeType.EVERYDAY.getInfo(),
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                projectId,
                null,
                env,
                1, null);
        //组装返回体
        JSONObject returnData = new JSONObject();
        returnData.put("newUserNumber", newUserNumber);
        returnData.put("oldUserNumber", oldUserNumber);
        return returnData;
    }

    @Override
    public JSONObject getTwentyFourHourBy(String projectId, String reportTimestamp, String env, Long typeId, Integer oldUserFlag, String groupBy) {
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //获取结束时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取开始时间
        Date preDate = enhanceMainService.getPreDate(endDate, Calendar.HOUR, -24);
        //一周前结束时间
        Date endDateWeekAgo = enhanceMainService.getPreDate(endDate, Calendar.DATE, -7);
        //一周前开始时间
        Date preDateWeekAgo = enhanceMainService.getPreDate(endDateWeekAgo, Calendar.HOUR, -24);
        //分别统计
        List<EnhanceDataStatistics> twentyFourHoursNumber = mainMapper.dataStatistics(
                DataStatisticsTimeType.EVERYDAY.getInfo(),
                simpleDateFormat.format(preDate),
                simpleDateFormat.format(endDate),
                projectId,
                typeId,
                env,
                oldUserFlag,
                groupBy);
        List<EnhanceDataStatistics> weekAgoNumber = mainMapper.dataStatistics(
                DataStatisticsTimeType.EVERYDAY.getInfo(),
                simpleDateFormat.format(preDateWeekAgo),
                simpleDateFormat.format(endDateWeekAgo),
                projectId,
                typeId,
                env,
                oldUserFlag,
                groupBy);
        //组装返回体
        JSONObject returnDate = new JSONObject();
        returnDate.put("twentyFourHoursNumber", twentyFourHoursNumber);
        returnDate.put("weekAgoNumber", weekAgoNumber);
        return returnDate;
    }

    @Override
    public JSONObject getTwentyFourHourPv(String projectId, String reportTimestamp, String env) {
        return getTwentyFourHourBy(projectId, reportTimestamp, env, 11L, null, null);
    }

    @Override
    public JSONObject getTwentyFourHourUv(String projectId, String reportTimestamp, String env) {
        return getTwentyFourHourBy(projectId, reportTimestamp, env, null, null, "uuid");
    }

    @Override
    public JSONObject getTwentyFourHourNewUser(String projectId, String reportTimestamp, String env) {
        return getTwentyFourHourBy(projectId, reportTimestamp, env, null, 0, null);
    }

    @Override
    public ArrayList<Integer> getTenDaysUserAvgStay(String projectId, String reportTimestamp, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束和开始时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate = enhanceMainService.getPreDateZero(endDate);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //10天数目比较小，摆烂不想sql了
        ArrayList<Integer> returnData = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Integer result = mainMapper.statisticTenDaysUserAvgStay(projectId, simpleDateFormat.format(preDate), simpleDateFormat.format(endDate), env, null, null);
            if (result == null)
                result = 0;
            returnData.add(result);
            endDate = preDate;
            preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -1);
        }
        return returnData;
    }

    @Override
    public ArrayList<Double> getTenDaysNewUserInNextDay(String projectId, String reportTimestamp, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //获取结束和开始时间
        Date endDate = enhanceMainService.getEndDate(reportTimestamp);
        Date preDate = enhanceMainService.getPreDateZero(endDate);
        //获取前一天时间
        Date endDateDayAgo = preDate;
        Date preDateDayAgo = enhanceMainService.getPreDate(endDateDayAgo, Calendar.DATE, -1);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        //10天数目比较小，摆烂不想sql了
        ArrayList<Double> returnData = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            //获取前一天新用户数量
            EnhanceProjectMessage enhanceProjectMessage = new EnhanceProjectMessage();
            enhanceProjectMessage.setPreDate(simpleDateFormat.format(preDateDayAgo));
            enhanceProjectMessage.setEndDate(simpleDateFormat.format(endDateDayAgo));
            enhanceProjectMessage.setPid(projectId);
            enhanceProjectMessage.setProduction(env);
            enhanceProjectMessage.setOldUserFlag(0);
            enhanceProjectMessage.setGroupBy("uuid");
            List<FontMonitorMain> mainList = mainMapper.selectFontMonitorMainListGroupBy(enhanceProjectMessage);
            //获取当日前一日新用户数据
            List<FontMonitorMain> mainListDayAgo = mainMapper.selectMainListOnTenDaysNewUserInNextDay(
                    projectId,
                    simpleDateFormat.format(preDate),
                    simpleDateFormat.format(endDate),
                    env,
                    simpleDateFormat.format(preDateDayAgo),
                    simpleDateFormat.format(endDateDayAgo));
            //计算
            Double result = 0D;
            if (mainListDayAgo.size() != 0 && mainList.size() != 0)
                result = Double.valueOf(mainListDayAgo.size()) / mainList.size();
            returnData.add(result);
            //设置时间
            endDate = preDate;
            preDate = enhanceMainService.getPreDate(endDate, Calendar.DATE, -1);
            endDateDayAgo = preDate;
            preDateDayAgo = enhanceMainService.getPreDate(endDateDayAgo, Calendar.DATE, -1);
        }
        return returnData;
    }

    @Override
    public List<EnhanceTopAppoint> getTopByUrl(String projectId, Long appointNumber, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        return mainMapper.statisticTopByUrlAppointInMain(projectId, env, appointNumber);
    }

    @Override
    public List<EnhanceTopAppoint> getTopByBrowser(String projectId, Long appointNumber, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        return mainMapper.statisticTopByBrowserAppointInMain(projectId, env, appointNumber);
    }

    @Override
    public List<EnhanceTopAppoint> getTopByOS(String projectId, Long appointNumber, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        return mainMapper.statisticTopByOSAppointInMain(projectId, env, appointNumber);
    }

    @Override
    public List<EnhanceTopAppoint> getTopByScreen(String projectId, Long appointNumber, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        return mainMapper.statisticTopByScreenAppointInMainAndBlankScreen(projectId, env, appointNumber);
    }

    @Override
    public List<EnhanceTopAppoint> getTopByCity(String projectId, Long appointNumber, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        return mainMapper.statisticTopByIpCityAppointInMain(projectId, env, appointNumber);
    }

    @Override
    public List<EnhanceTopAppoint> getTopByCountry(String projectId, Long appointNumber, String env, String reportTimestamp) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        // 如果时间戳不为空，返回当天数据
        String preDate = null;
        String endDate = null;
        if (reportTimestamp != null && !reportTimestamp.equals("")) {
            // 获取传入时间
            Date date = enhanceMainService.getEndDate(reportTimestamp);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            Integer year = calendar.get(Calendar.YEAR);
            Integer month = calendar.get(Calendar.MONTH) + 1;
            Integer day = calendar.get(Calendar.DATE);
            // 设置统计开始时间和结束时间
            preDate = year + "-" + month + "-" + day + " 00:00:00";
            endDate = year + "-" + month + "-" + day + " 23:59:59";
        }
        return mainMapper.statisticTopByIpCountryAppointInMain(projectId, env, appointNumber, preDate, endDate);
    }

    @Override
    public List<EnhanceTopAppoint> getTopByProvince(String projectId, Long appointNumber, String env, String reportTimestamp) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        // 如果时间戳不为空，返回当天数据
        String preDate = null;
        String endDate = null;
        if (reportTimestamp != null && !reportTimestamp.equals("")) {
            // 获取传入时间
            Date date = enhanceMainService.getEndDate(reportTimestamp);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            Integer year = calendar.get(Calendar.YEAR);
            Integer month = calendar.get(Calendar.MONTH) + 1;
            Integer day = calendar.get(Calendar.DATE);
            // 设置统计开始时间和结束时间
            preDate = year + "-" + month + "-" + day + " 00:00:00";
            endDate = year + "-" + month + "-" + day + " 23:59:59";
        }
        return mainMapper.statisticTopByIpProvinceAppointInMain(projectId, env, appointNumber, preDate, endDate);
    }

    @Override
    public List<EnhanceTopAppoint> getTopByResourceUrl(String projectId, Long appointNumber, String env) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        return mainMapper.statisticTopByResourceOutWebAppointInMainAndVisitInfo(projectId, env, appointNumber);
    }

    @Override
    public List<FontMonitorMain> getMainListGroupByFirstTimestamp(String projectId, String ip, String reportTimestamp, String env, String uuid) {
        //验证pid存在性
        if (projectId != null && !projectId.equals(""))
            enhanceProjectService.searchProjectByPid(projectId);
        //格式化时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd");
        // 如果时间戳不为空，返回当天数据
        String preDate = null;
        String endDate = null;
        if (reportTimestamp != null && !reportTimestamp.equals("")) {
            Date date = enhanceMainService.getEndDate(reportTimestamp);
            endDate = simpleDateFormat.format(date) + " 23:59:59";
            preDate = simpleDateFormat.format(date) + " 00:00:00";
        }
        startPage();
        return mainMapper.selectMainListGroupByFirstTimestamp(projectId, env, preDate, endDate, ip, uuid);
    }

    @Override
    public JSONObject getUserAction(String mainId, String reportTimestamp, String type) {
        // 首先根据传入的mainId查找到相应的第一条主数据
        FontMonitorMain firstMainData = new FontMonitorMain();
        firstMainData.setMainId(mainId);
        List<FontMonitorMain> firstMainDataList = mainMapper.selectFontMonitorMainList(firstMainData);
        if (firstMainDataList.isEmpty())
            throw new ServiceException("没有查询到相关记录");
        if (firstMainDataList.size() > 1)
            throw new ServiceException("出现重复记录，请联系管理员");
        firstMainData = firstMainDataList.get(0);
        // 根据第一条记录的uuid、projectId、first_timestamp查询相关记录
        FontMonitorMain searchUserAction = new FontMonitorMain();
        searchUserAction.setUuid(firstMainData.getUuid());
        searchUserAction.setPid(firstMainData.getPid());
        searchUserAction.setFirstTimestamp(firstMainData.getFirstTimestamp());
        List<FontMonitorMain> userActionList = mainMapper.selectFontMonitorMainList(searchUserAction);
        // 构造返回体
        JSONObject returnData = new JSONObject();
        // 根据类型分别查询相应的子表，然后将信息传进返回体中
        for (int i = 0; i < userActionList.size(); i++) {
            String userActionMainId = userActionList.get(i).getMainId();
            switch (userActionList.get(i).getTypeId()) {
                case "0":
                    List<EnhanceMainAndJsAndPromiseAndCustomVo> jsList
                            = mainMapper.selectMainInnerJoinJsError(null, null, null, null, userActionMainId);
                    if (jsList.size() != 0)
                        returnData.put(String.valueOf(i), jsList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndJsAndPromiseAndCustomVo.class));
                    break;
                case "1":
                    List<EnhanceMainAndJsAndPromiseAndCustomVo> promiseList
                            = mainMapper.selectMainInnerJoinPromiseError(null, null, null, null, userActionMainId);
                    if (promiseList.size() != 0)
                        returnData.put(String.valueOf(i), promiseList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndJsAndPromiseAndCustomVo.class));
                    break;
                case "2":
                    List<EnhanceMainAndResourceErrorVo> resourceErrorList
                            = mainMapper.selectMainInnerJoinResourceError(null, null, null, null, userActionMainId);
                    if (resourceErrorList.size() != 0)
                        returnData.put(String.valueOf(i), resourceErrorList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndResourceErrorVo.class));
                    break;
                case "3":
                    List<EnhanceMainAndResourceVo> resourceList
                            = mainMapper.selectMainInnerResource(null, null, null, null, userActionMainId);
                    if (resourceList.size() != 0)
                        returnData.put(String.valueOf(i), resourceList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndResourceVo.class));
                    break;
                case "4":
                    List<EnhanceMainAndXhrAndFetchVo> xhrList
                            = mainMapper.selectMainInnerJoinXhr(null, null, null, null, null, userActionMainId, null, null);
                    if (xhrList.size() != 0)
                        returnData.put(String.valueOf(i), xhrList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndXhrAndFetchVo.class));
                    break;
                case "5":
                    List<EnhanceMainAndXhrAndFetchVo> fetchList
                            = mainMapper.selectMainInnerJoinXhr(null, null, null, null, null, userActionMainId, null, null);
                    if (fetchList.size() != 0)
                        returnData.put(String.valueOf(i), fetchList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndXhrAndFetchVo.class));
                    break;
                case "6":
                    List<EnhanceMainAndBlankScreenVo> blankScreenList
                            = mainMapper.selectMainInnerJoinBlankScreen(null, null, null, null, userActionMainId);
                    if (blankScreenList.size() != 0)
                        returnData.put(String.valueOf(i), blankScreenList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndBlankScreenVo.class));
                    break;
                case "7":
                    List<EnhanceMainAndLoadTimeVo> loadTimeList
                            = mainMapper.selectMainLeftJoinLoadTime(null, null, null, null, userActionMainId, null, null);
                    if (loadTimeList.size() != 0)
                        returnData.put(String.valueOf(i), loadTimeList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndLoadTimeVo.class));
                    break;
                case "8":
                    List<EnhanceMainAndPaintTimeVo> paintTimeList
                            = mainMapper.selectMainInnerJoinPaintTime(null, null, null, null, userActionMainId);
                    if (paintTimeList.size() != 0)
                        returnData.put(String.valueOf(i), paintTimeList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndPaintTimeVo.class));
                    break;
                case "9":
                    List<EnhanceMainAndFirstInputDelayVo> firstInputDelayList
                            = mainMapper.selectMainInnerJoinFirstInputDelay(null, null, null, null, userActionMainId);
                    if (firstInputDelayList.size() != 0)
                        returnData.put(String.valueOf(i), firstInputDelayList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndFirstInputDelayVo.class));
                    break;
                case "10":
                    List<EnhanceMainAndLongTaskVo> longTaskList
                            = mainMapper.selectMainInnerJoinLongTask(null, null, null, null, userActionMainId);
                    if (longTaskList.size() != 0)
                        returnData.put(String.valueOf(i), longTaskList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndLongTaskVo.class));
                    break;
                case "11":
                    List<EnhanceMainAndVisitInfoVo> visitInfoList
                            = mainMapper.selectMainInnerJoinVisitInfo(null, null, null, null, userActionMainId);
                    if (visitInfoList.size() != 0)
                        returnData.put(String.valueOf(i), visitInfoList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndVisitInfoVo.class));
                    break;
                case "12":
                    List<EnhanceMainAndStayTimeVo> stayTimeList
                            = mainMapper.selectMainInnerJoinStayTime(null, null, null, null, userActionMainId);
                    if (stayTimeList.size() != 0)
                        returnData.put(String.valueOf(i), stayTimeList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndStayTimeVo.class));
                    break;
                case "13":
                    List<EnhanceMainAndHashVo> hashList
                            = mainMapper.selectMainInnerHash(null, null, null, null, userActionMainId);
                    if (hashList.size() != 0)
                        returnData.put(String.valueOf(i), hashList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndHashVo.class));
                    break;
                case "14":
                    List<EnhanceMainAndHistoryVo> historyList
                            = mainMapper.selectMainInnerHistory(null, null, null, null, userActionMainId);
                    if (historyList.size() != 0)
                        returnData.put(String.valueOf(i), historyList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndHistoryVo.class));
                    break;
                case "15":
                    List<EnhanceMainAndJsAndPromiseAndCustomVo> customList
                            = mainMapper.selectMainInnerJoinCustomMessage(null, null, null, null, userActionMainId);
                    if (customList.size() != 0)
                        returnData.put(String.valueOf(i), customList.get(0));
                    else
                        returnData.put(String.valueOf(i), BeanCopyUtils.copyBean(userActionList.get(i), EnhanceMainAndJsAndPromiseAndCustomVo.class));
                    break;
//                default:
//                    returnData.put(String.valueOf(i), userActionList.get(i));
//                    break;
            }
        }
        return returnData;
    }

    @Override
    public JSONObject getUserPageAvgLoadTime(String mainId) {
        // 首先根据传入的mainId查找到相应的第一条主数据
        FontMonitorMain firstMainData = new FontMonitorMain();
        firstMainData.setMainId(mainId);
        List<FontMonitorMain> firstMainDataList = mainMapper.selectFontMonitorMainList(firstMainData);
        if (firstMainDataList.isEmpty())
            throw new ServiceException("没有查询到相关记录");
        if (firstMainDataList.size() > 1)
            throw new ServiceException("出现重复记录，请联系管理员");
        firstMainData = firstMainDataList.get(0);
        // 根据第一条记录的uuid、projectId、first_timestamp查询相关记录
        List<EnhanceMainAndLoadTimeVo> loadTimeList
                = mainMapper.selectMainLeftJoinLoadTime(
                firstMainData.getPid(),
                null,
                null,
                null,
                null,
                firstMainData.getUuid(),
                firstMainData.getFirstTimestamp());


        List<StatisticPageAndInterface> statisticList = new ArrayList<>();

        // 统计数据
        for (int i = 0; i < loadTimeList.size(); i++) {
            final String url = loadTimeList.get(i).getUrl();// lambda表达式必须为final
            final Double time = loadTimeList.get(i).getLoadTime();// lambda表达式必须为final
            final String title = loadTimeList.get(i).getTitle();
            // 找到statisticList里相应的名字
            List<StatisticPageAndInterface> searchResult = statisticList.stream()
                    .filter(statistic -> statistic.getName().equals(url))
                    .collect(Collectors.toList());
            if (searchResult.isEmpty()) {
                StatisticPageAndInterface statistic = new StatisticPageAndInterface();
                statistic.setName(url);
                statistic.setTime(time);
                statistic.setNumber(1);
                statistic.setTitle(title);
                statisticList.add(statistic);
            } else
                searchResult.stream()
                        .forEach(statistic -> {
                            statistic.setTime(statistic.getTime() + time);
                            statistic.setNumber(statistic.getNumber() + 1);
                        });


        }

        // 构造返回体
        JSONObject returnData = new JSONObject();
        for (int i = 0; i < statisticList.size(); i++) {
            returnData.put(statisticList.get(i).getName(),
                    statisticList.get(i).getTime() / statisticList.get(i).getNumber());
        }

        return returnData;
    }

    @Override
    public JSONObject getUserInterfaceAvgTime(String mainId) {
        /// 首先根据传入的mainId查找到相应的第一条主数据
        FontMonitorMain firstMainData = new FontMonitorMain();
        firstMainData.setMainId(mainId);
        List<FontMonitorMain> firstMainDataList = mainMapper.selectFontMonitorMainList(firstMainData);
        if (firstMainDataList.isEmpty())
            throw new ServiceException("没有查询到相关记录");
        if (firstMainDataList.size() > 1)
            throw new ServiceException("出现重复记录，请联系管理员");
        firstMainData = firstMainDataList.get(0);
        // 根据第一条记录的uuid、projectId、first_timestamp查询相关记录
        List<EnhanceMainAndXhrAndFetchVo> xhrList = mainMapper.selectMainInnerJoinXhr(
                firstMainData.getPid(),
                null,
                null,
                null,
                null,
                null,
                firstMainData.getUuid(),
                firstMainData.getFirstTimestamp()
        );
        List<EnhanceMainAndXhrAndFetchVo> fetchList = mainMapper.selectMainInnerJoinFetch(
                firstMainData.getPid(),
                null,
                null,
                null,
                null,
                null,
                firstMainData.getUuid(),
                firstMainData.getFirstTimestamp()
        );
        // 合并数据
        List<EnhanceMainAndXhrAndFetchVo> allList = Stream
                .concat(xhrList.stream(), fetchList.stream())
                .collect(Collectors.toList());
        // 统计数据
        List<StatisticPageAndInterface> statisticList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            StatisticPageAndInterface statisticPageAndInterface = new StatisticPageAndInterface();
            statisticPageAndInterface.setNumber(0);
            statisticList.add(statisticPageAndInterface);
        }
        for (int i = 0; i < allList.size(); i++) {
            final Double getDuration = allList.get(i).getDuration();
            if (getDuration < 1) {
                statisticList.get(0).setNumber(statisticList.get(0).getNumber() + 1);
            } else if (getDuration < 5) {
                statisticList.get(1).setNumber(statisticList.get(1).getNumber() + 1);
            } else if (getDuration < 10) {
                statisticList.get(2).setNumber(statisticList.get(2).getNumber() + 1);
            } else if (getDuration < 30) {
                statisticList.get(3).setNumber(statisticList.get(3).getNumber() + 1);
            } else {
                statisticList.get(4).setNumber(statisticList.get(4).getNumber() + 1);
            }
        }
        // 构造返回体
        JSONObject returnData = new JSONObject();
        returnData.put("小于1s", statisticList.get(0).getNumber());
        returnData.put("1-5s", statisticList.get(1).getNumber());
        returnData.put("5-10s", statisticList.get(2).getNumber());
        returnData.put("10-30s", statisticList.get(3).getNumber());
        returnData.put("大于30s", statisticList.get(4).getNumber());
        return returnData;
    }

    @Data
    private class StatisticPageAndInterface {
        private String name;

        private String title;

        private Double time;

        private int number;

        private Double agvTime;
    }
}
