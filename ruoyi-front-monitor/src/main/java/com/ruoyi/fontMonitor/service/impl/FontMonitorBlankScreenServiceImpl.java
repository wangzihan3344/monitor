package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorBlankScreenMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorBlankScreen;
import com.ruoyi.fontMonitor.service.IFontMonitorBlankScreenService;

/**
 * 监控白屏Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorBlankScreenServiceImpl implements IFontMonitorBlankScreenService 
{
    @Autowired
    private FontMonitorBlankScreenMapper fontMonitorBlankScreenMapper;

    /**
     * 查询监控白屏
     * 
     * @param id 监控白屏主键
     * @return 监控白屏
     */
    @Override
    public FontMonitorBlankScreen selectFontMonitorBlankScreenById(Long id)
    {
        return fontMonitorBlankScreenMapper.selectFontMonitorBlankScreenById(id);
    }

    /**
     * 查询监控白屏列表
     * 
     * @param fontMonitorBlankScreen 监控白屏
     * @return 监控白屏
     */
    @Override
    public List<FontMonitorBlankScreen> selectFontMonitorBlankScreenList(FontMonitorBlankScreen fontMonitorBlankScreen)
    {
        return fontMonitorBlankScreenMapper.selectFontMonitorBlankScreenList(fontMonitorBlankScreen);
    }

    /**
     * 新增监控白屏
     * 
     * @param fontMonitorBlankScreen 监控白屏
     * @return 结果
     */
    @Override
    public int insertFontMonitorBlankScreen(FontMonitorBlankScreen fontMonitorBlankScreen)
    {
        return fontMonitorBlankScreenMapper.insertFontMonitorBlankScreen(fontMonitorBlankScreen);
    }

    /**
     * 修改监控白屏
     * 
     * @param fontMonitorBlankScreen 监控白屏
     * @return 结果
     */
    @Override
    public int updateFontMonitorBlankScreen(FontMonitorBlankScreen fontMonitorBlankScreen)
    {
        return fontMonitorBlankScreenMapper.updateFontMonitorBlankScreen(fontMonitorBlankScreen);
    }

    /**
     * 批量删除监控白屏
     * 
     * @param ids 需要删除的监控白屏主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorBlankScreenByIds(Long[] ids)
    {
        return fontMonitorBlankScreenMapper.deleteFontMonitorBlankScreenByIds(ids);
    }

    /**
     * 删除监控白屏信息
     * 
     * @param id 监控白屏主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorBlankScreenById(Long id)
    {
        return fontMonitorBlankScreenMapper.deleteFontMonitorBlankScreenById(id);
    }
}
