package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorCustomMessageMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorCustomMessage;
import com.ruoyi.fontMonitor.service.IFontMonitorCustomMessageService;

/**
 * custom_error 的 message 数据Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorCustomMessageServiceImpl implements IFontMonitorCustomMessageService 
{
    @Autowired
    private FontMonitorCustomMessageMapper fontMonitorCustomMessageMapper;

    /**
     * 查询custom_error 的 message 数据
     * 
     * @param id custom_error 的 message 数据主键
     * @return custom_error 的 message 数据
     */
    @Override
    public FontMonitorCustomMessage selectFontMonitorCustomMessageById(Long id)
    {
        return fontMonitorCustomMessageMapper.selectFontMonitorCustomMessageById(id);
    }

    /**
     * 查询custom_error 的 message 数据列表
     * 
     * @param fontMonitorCustomMessage custom_error 的 message 数据
     * @return custom_error 的 message 数据
     */
    @Override
    public List<FontMonitorCustomMessage> selectFontMonitorCustomMessageList(FontMonitorCustomMessage fontMonitorCustomMessage)
    {
        return fontMonitorCustomMessageMapper.selectFontMonitorCustomMessageList(fontMonitorCustomMessage);
    }

    /**
     * 新增custom_error 的 message 数据
     * 
     * @param fontMonitorCustomMessage custom_error 的 message 数据
     * @return 结果
     */
    @Override
    public int insertFontMonitorCustomMessage(FontMonitorCustomMessage fontMonitorCustomMessage)
    {
        return fontMonitorCustomMessageMapper.insertFontMonitorCustomMessage(fontMonitorCustomMessage);
    }

    /**
     * 修改custom_error 的 message 数据
     * 
     * @param fontMonitorCustomMessage custom_error 的 message 数据
     * @return 结果
     */
    @Override
    public int updateFontMonitorCustomMessage(FontMonitorCustomMessage fontMonitorCustomMessage)
    {
        return fontMonitorCustomMessageMapper.updateFontMonitorCustomMessage(fontMonitorCustomMessage);
    }

    /**
     * 批量删除custom_error 的 message 数据
     * 
     * @param ids 需要删除的custom_error 的 message 数据主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorCustomMessageByIds(Long[] ids)
    {
        return fontMonitorCustomMessageMapper.deleteFontMonitorCustomMessageByIds(ids);
    }

    /**
     * 删除custom_error 的 message 数据信息
     * 
     * @param id custom_error 的 message 数据主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorCustomMessageById(Long id)
    {
        return fontMonitorCustomMessageMapper.deleteFontMonitorCustomMessageById(id);
    }
}
