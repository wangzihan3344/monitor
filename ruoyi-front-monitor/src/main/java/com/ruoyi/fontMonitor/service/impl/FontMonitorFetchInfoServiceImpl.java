package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorFetchInfoMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorFetchInfo;
import com.ruoyi.fontMonitor.service.IFontMonitorFetchInfoService;

/**
 * 监控 fetch 接口Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorFetchInfoServiceImpl implements IFontMonitorFetchInfoService 
{
    @Autowired
    private FontMonitorFetchInfoMapper fontMonitorFetchInfoMapper;

    /**
     * 查询监控 fetch 接口
     * 
     * @param id 监控 fetch 接口主键
     * @return 监控 fetch 接口
     */
    @Override
    public FontMonitorFetchInfo selectFontMonitorFetchInfoById(Long id)
    {
        return fontMonitorFetchInfoMapper.selectFontMonitorFetchInfoById(id);
    }

    /**
     * 查询监控 fetch 接口列表
     * 
     * @param fontMonitorFetchInfo 监控 fetch 接口
     * @return 监控 fetch 接口
     */
    @Override
    public List<FontMonitorFetchInfo> selectFontMonitorFetchInfoList(FontMonitorFetchInfo fontMonitorFetchInfo)
    {
        return fontMonitorFetchInfoMapper.selectFontMonitorFetchInfoList(fontMonitorFetchInfo);
    }

    /**
     * 新增监控 fetch 接口
     * 
     * @param fontMonitorFetchInfo 监控 fetch 接口
     * @return 结果
     */
    @Override
    public int insertFontMonitorFetchInfo(FontMonitorFetchInfo fontMonitorFetchInfo)
    {
        return fontMonitorFetchInfoMapper.insertFontMonitorFetchInfo(fontMonitorFetchInfo);
    }

    /**
     * 修改监控 fetch 接口
     * 
     * @param fontMonitorFetchInfo 监控 fetch 接口
     * @return 结果
     */
    @Override
    public int updateFontMonitorFetchInfo(FontMonitorFetchInfo fontMonitorFetchInfo)
    {
        return fontMonitorFetchInfoMapper.updateFontMonitorFetchInfo(fontMonitorFetchInfo);
    }

    /**
     * 批量删除监控 fetch 接口
     * 
     * @param ids 需要删除的监控 fetch 接口主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorFetchInfoByIds(Long[] ids)
    {
        return fontMonitorFetchInfoMapper.deleteFontMonitorFetchInfoByIds(ids);
    }

    /**
     * 删除监控 fetch 接口信息
     * 
     * @param id 监控 fetch 接口主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorFetchInfoById(Long id)
    {
        return fontMonitorFetchInfoMapper.deleteFontMonitorFetchInfoById(id);
    }
}
