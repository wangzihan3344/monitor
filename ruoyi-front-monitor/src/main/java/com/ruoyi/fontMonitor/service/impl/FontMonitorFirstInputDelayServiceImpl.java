package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorFirstInputDelayMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorFirstInputDelay;
import com.ruoyi.fontMonitor.service.IFontMonitorFirstInputDelayService;

/**
 * 监控首次输入响应延迟Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorFirstInputDelayServiceImpl implements IFontMonitorFirstInputDelayService 
{
    @Autowired
    private FontMonitorFirstInputDelayMapper fontMonitorFirstInputDelayMapper;

    /**
     * 查询监控首次输入响应延迟
     * 
     * @param id 监控首次输入响应延迟主键
     * @return 监控首次输入响应延迟
     */
    @Override
    public FontMonitorFirstInputDelay selectFontMonitorFirstInputDelayById(Long id)
    {
        return fontMonitorFirstInputDelayMapper.selectFontMonitorFirstInputDelayById(id);
    }

    /**
     * 查询监控首次输入响应延迟列表
     * 
     * @param fontMonitorFirstInputDelay 监控首次输入响应延迟
     * @return 监控首次输入响应延迟
     */
    @Override
    public List<FontMonitorFirstInputDelay> selectFontMonitorFirstInputDelayList(FontMonitorFirstInputDelay fontMonitorFirstInputDelay)
    {
        return fontMonitorFirstInputDelayMapper.selectFontMonitorFirstInputDelayList(fontMonitorFirstInputDelay);
    }

    /**
     * 新增监控首次输入响应延迟
     * 
     * @param fontMonitorFirstInputDelay 监控首次输入响应延迟
     * @return 结果
     */
    @Override
    public int insertFontMonitorFirstInputDelay(FontMonitorFirstInputDelay fontMonitorFirstInputDelay)
    {
        return fontMonitorFirstInputDelayMapper.insertFontMonitorFirstInputDelay(fontMonitorFirstInputDelay);
    }

    /**
     * 修改监控首次输入响应延迟
     * 
     * @param fontMonitorFirstInputDelay 监控首次输入响应延迟
     * @return 结果
     */
    @Override
    public int updateFontMonitorFirstInputDelay(FontMonitorFirstInputDelay fontMonitorFirstInputDelay)
    {
        return fontMonitorFirstInputDelayMapper.updateFontMonitorFirstInputDelay(fontMonitorFirstInputDelay);
    }

    /**
     * 批量删除监控首次输入响应延迟
     * 
     * @param ids 需要删除的监控首次输入响应延迟主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorFirstInputDelayByIds(Long[] ids)
    {
        return fontMonitorFirstInputDelayMapper.deleteFontMonitorFirstInputDelayByIds(ids);
    }

    /**
     * 删除监控首次输入响应延迟信息
     * 
     * @param id 监控首次输入响应延迟主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorFirstInputDelayById(Long id)
    {
        return fontMonitorFirstInputDelayMapper.deleteFontMonitorFirstInputDelayById(id);
    }
}
