package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorHashMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorHash;
import com.ruoyi.fontMonitor.service.IFontMonitorHashService;

/**
 * url变动Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorHashServiceImpl implements IFontMonitorHashService 
{
    @Autowired
    private FontMonitorHashMapper fontMonitorHashMapper;

    /**
     * 查询url变动
     * 
     * @param id url变动主键
     * @return url变动
     */
    @Override
    public FontMonitorHash selectFontMonitorHashById(Long id)
    {
        return fontMonitorHashMapper.selectFontMonitorHashById(id);
    }

    /**
     * 查询url变动列表
     * 
     * @param fontMonitorHash url变动
     * @return url变动
     */
    @Override
    public List<FontMonitorHash> selectFontMonitorHashList(FontMonitorHash fontMonitorHash)
    {
        return fontMonitorHashMapper.selectFontMonitorHashList(fontMonitorHash);
    }

    /**
     * 新增url变动
     * 
     * @param fontMonitorHash url变动
     * @return 结果
     */
    @Override
    public int insertFontMonitorHash(FontMonitorHash fontMonitorHash)
    {
        return fontMonitorHashMapper.insertFontMonitorHash(fontMonitorHash);
    }

    /**
     * 修改url变动
     * 
     * @param fontMonitorHash url变动
     * @return 结果
     */
    @Override
    public int updateFontMonitorHash(FontMonitorHash fontMonitorHash)
    {
        return fontMonitorHashMapper.updateFontMonitorHash(fontMonitorHash);
    }

    /**
     * 批量删除url变动
     * 
     * @param ids 需要删除的url变动主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorHashByIds(Long[] ids)
    {
        return fontMonitorHashMapper.deleteFontMonitorHashByIds(ids);
    }

    /**
     * 删除url变动信息
     * 
     * @param id url变动主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorHashById(Long id)
    {
        return fontMonitorHashMapper.deleteFontMonitorHashById(id);
    }
}
