package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorHistoryMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorHistory;
import com.ruoyi.fontMonitor.service.IFontMonitorHistoryService;

/**
 * 路由记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorHistoryServiceImpl implements IFontMonitorHistoryService 
{
    @Autowired
    private FontMonitorHistoryMapper fontMonitorHistoryMapper;

    /**
     * 查询路由记录
     * 
     * @param id 路由记录主键
     * @return 路由记录
     */
    @Override
    public FontMonitorHistory selectFontMonitorHistoryById(Long id)
    {
        return fontMonitorHistoryMapper.selectFontMonitorHistoryById(id);
    }

    /**
     * 查询路由记录列表
     * 
     * @param fontMonitorHistory 路由记录
     * @return 路由记录
     */
    @Override
    public List<FontMonitorHistory> selectFontMonitorHistoryList(FontMonitorHistory fontMonitorHistory)
    {
        return fontMonitorHistoryMapper.selectFontMonitorHistoryList(fontMonitorHistory);
    }

    /**
     * 新增路由记录
     * 
     * @param fontMonitorHistory 路由记录
     * @return 结果
     */
    @Override
    public int insertFontMonitorHistory(FontMonitorHistory fontMonitorHistory)
    {
        return fontMonitorHistoryMapper.insertFontMonitorHistory(fontMonitorHistory);
    }

    /**
     * 修改路由记录
     * 
     * @param fontMonitorHistory 路由记录
     * @return 结果
     */
    @Override
    public int updateFontMonitorHistory(FontMonitorHistory fontMonitorHistory)
    {
        return fontMonitorHistoryMapper.updateFontMonitorHistory(fontMonitorHistory);
    }

    /**
     * 批量删除路由记录
     * 
     * @param ids 需要删除的路由记录主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorHistoryByIds(Long[] ids)
    {
        return fontMonitorHistoryMapper.deleteFontMonitorHistoryByIds(ids);
    }

    /**
     * 删除路由记录信息
     * 
     * @param id 路由记录主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorHistoryById(Long id)
    {
        return fontMonitorHistoryMapper.deleteFontMonitorHistoryById(id);
    }
}
