package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorJsErrorMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorJsError;
import com.ruoyi.fontMonitor.service.IFontMonitorJsErrorService;

/**
 * 监控 jsErrorService业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorJsErrorServiceImpl implements IFontMonitorJsErrorService 
{
    @Autowired
    private FontMonitorJsErrorMapper fontMonitorJsErrorMapper;

    /**
     * 查询监控 jsError
     * 
     * @param id 监控 jsError主键
     * @return 监控 jsError
     */
    @Override
    public FontMonitorJsError selectFontMonitorJsErrorById(Long id)
    {
        return fontMonitorJsErrorMapper.selectFontMonitorJsErrorById(id);
    }

    /**
     * 查询监控 jsError列表
     * 
     * @param fontMonitorJsError 监控 jsError
     * @return 监控 jsError
     */
    @Override
    public List<FontMonitorJsError> selectFontMonitorJsErrorList(FontMonitorJsError fontMonitorJsError)
    {
        return fontMonitorJsErrorMapper.selectFontMonitorJsErrorList(fontMonitorJsError);
    }

    /**
     * 新增监控 jsError
     * 
     * @param fontMonitorJsError 监控 jsError
     * @return 结果
     */
    @Override
    public int insertFontMonitorJsError(FontMonitorJsError fontMonitorJsError)
    {
        return fontMonitorJsErrorMapper.insertFontMonitorJsError(fontMonitorJsError);
    }

    /**
     * 修改监控 jsError
     * 
     * @param fontMonitorJsError 监控 jsError
     * @return 结果
     */
    @Override
    public int updateFontMonitorJsError(FontMonitorJsError fontMonitorJsError)
    {
        return fontMonitorJsErrorMapper.updateFontMonitorJsError(fontMonitorJsError);
    }

    /**
     * 批量删除监控 jsError
     * 
     * @param ids 需要删除的监控 jsError主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorJsErrorByIds(Long[] ids)
    {
        return fontMonitorJsErrorMapper.deleteFontMonitorJsErrorByIds(ids);
    }

    /**
     * 删除监控 jsError信息
     * 
     * @param id 监控 jsError主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorJsErrorById(Long id)
    {
        return fontMonitorJsErrorMapper.deleteFontMonitorJsErrorById(id);
    }
}
