package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorLoadTimeMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorLoadTime;
import com.ruoyi.fontMonitor.service.IFontMonitorLoadTimeService;

/**
 * 监控加载时间Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorLoadTimeServiceImpl implements IFontMonitorLoadTimeService 
{
    @Autowired
    private FontMonitorLoadTimeMapper fontMonitorLoadTimeMapper;

    /**
     * 查询监控加载时间
     * 
     * @param id 监控加载时间主键
     * @return 监控加载时间
     */
    @Override
    public FontMonitorLoadTime selectFontMonitorLoadTimeById(Long id)
    {
        return fontMonitorLoadTimeMapper.selectFontMonitorLoadTimeById(id);
    }

    /**
     * 查询监控加载时间列表
     * 
     * @param fontMonitorLoadTime 监控加载时间
     * @return 监控加载时间
     */
    @Override
    public List<FontMonitorLoadTime> selectFontMonitorLoadTimeList(FontMonitorLoadTime fontMonitorLoadTime)
    {
        return fontMonitorLoadTimeMapper.selectFontMonitorLoadTimeList(fontMonitorLoadTime);
    }

    /**
     * 新增监控加载时间
     * 
     * @param fontMonitorLoadTime 监控加载时间
     * @return 结果
     */
    @Override
    public int insertFontMonitorLoadTime(FontMonitorLoadTime fontMonitorLoadTime)
    {
        return fontMonitorLoadTimeMapper.insertFontMonitorLoadTime(fontMonitorLoadTime);
    }

    /**
     * 修改监控加载时间
     * 
     * @param fontMonitorLoadTime 监控加载时间
     * @return 结果
     */
    @Override
    public int updateFontMonitorLoadTime(FontMonitorLoadTime fontMonitorLoadTime)
    {
        return fontMonitorLoadTimeMapper.updateFontMonitorLoadTime(fontMonitorLoadTime);
    }

    /**
     * 批量删除监控加载时间
     * 
     * @param ids 需要删除的监控加载时间主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorLoadTimeByIds(Long[] ids)
    {
        return fontMonitorLoadTimeMapper.deleteFontMonitorLoadTimeByIds(ids);
    }

    /**
     * 删除监控加载时间信息
     * 
     * @param id 监控加载时间主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorLoadTimeById(Long id)
    {
        return fontMonitorLoadTimeMapper.deleteFontMonitorLoadTimeById(id);
    }
}
