package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorLongTaskMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorLongTask;
import com.ruoyi.fontMonitor.service.IFontMonitorLongTaskService;

/**
 * 监控卡顿Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorLongTaskServiceImpl implements IFontMonitorLongTaskService 
{
    @Autowired
    private FontMonitorLongTaskMapper fontMonitorLongTaskMapper;

    /**
     * 查询监控卡顿
     * 
     * @param id 监控卡顿主键
     * @return 监控卡顿
     */
    @Override
    public FontMonitorLongTask selectFontMonitorLongTaskById(Long id)
    {
        return fontMonitorLongTaskMapper.selectFontMonitorLongTaskById(id);
    }

    /**
     * 查询监控卡顿列表
     * 
     * @param fontMonitorLongTask 监控卡顿
     * @return 监控卡顿
     */
    @Override
    public List<FontMonitorLongTask> selectFontMonitorLongTaskList(FontMonitorLongTask fontMonitorLongTask)
    {
        return fontMonitorLongTaskMapper.selectFontMonitorLongTaskList(fontMonitorLongTask);
    }

    /**
     * 新增监控卡顿
     * 
     * @param fontMonitorLongTask 监控卡顿
     * @return 结果
     */
    @Override
    public int insertFontMonitorLongTask(FontMonitorLongTask fontMonitorLongTask)
    {
        return fontMonitorLongTaskMapper.insertFontMonitorLongTask(fontMonitorLongTask);
    }

    /**
     * 修改监控卡顿
     * 
     * @param fontMonitorLongTask 监控卡顿
     * @return 结果
     */
    @Override
    public int updateFontMonitorLongTask(FontMonitorLongTask fontMonitorLongTask)
    {
        return fontMonitorLongTaskMapper.updateFontMonitorLongTask(fontMonitorLongTask);
    }

    /**
     * 批量删除监控卡顿
     * 
     * @param ids 需要删除的监控卡顿主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorLongTaskByIds(Long[] ids)
    {
        return fontMonitorLongTaskMapper.deleteFontMonitorLongTaskByIds(ids);
    }

    /**
     * 删除监控卡顿信息
     * 
     * @param id 监控卡顿主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorLongTaskById(Long id)
    {
        return fontMonitorLongTaskMapper.deleteFontMonitorLongTaskById(id);
    }
}
