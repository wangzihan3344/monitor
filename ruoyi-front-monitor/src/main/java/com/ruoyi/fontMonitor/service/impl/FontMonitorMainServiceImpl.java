package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorMainMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorMain;
import com.ruoyi.fontMonitor.service.IFontMonitorMainService;

/**
 * 公共字段Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-30
 */
@Service
public class FontMonitorMainServiceImpl implements IFontMonitorMainService 
{
    @Autowired
    private FontMonitorMainMapper fontMonitorMainMapper;

    /**
     * 查询公共字段
     * 
     * @param id 公共字段主键
     * @return 公共字段
     */
    @Override
    public FontMonitorMain selectFontMonitorMainById(Long id)
    {
        return fontMonitorMainMapper.selectFontMonitorMainById(id);
    }

    /**
     * 查询公共字段列表
     * 
     * @param fontMonitorMain 公共字段
     * @return 公共字段
     */
    @Override
    public List<FontMonitorMain> selectFontMonitorMainList(FontMonitorMain fontMonitorMain)
    {
        return fontMonitorMainMapper.selectFontMonitorMainList(fontMonitorMain);
    }

    /**
     * 新增公共字段
     * 
     * @param fontMonitorMain 公共字段
     * @return 结果
     */
    @Override
    public int insertFontMonitorMain(FontMonitorMain fontMonitorMain)
    {
        return fontMonitorMainMapper.insertFontMonitorMain(fontMonitorMain);
    }

    /**
     * 修改公共字段
     * 
     * @param fontMonitorMain 公共字段
     * @return 结果
     */
    @Override
    public int updateFontMonitorMain(FontMonitorMain fontMonitorMain)
    {
        return fontMonitorMainMapper.updateFontMonitorMain(fontMonitorMain);
    }

    /**
     * 批量删除公共字段
     * 
     * @param ids 需要删除的公共字段主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorMainByIds(Long[] ids)
    {
        return fontMonitorMainMapper.deleteFontMonitorMainByIds(ids);
    }

    /**
     * 删除公共字段信息
     * 
     * @param id 公共字段主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorMainById(Long id)
    {
        return fontMonitorMainMapper.deleteFontMonitorMainById(id);
    }
}
