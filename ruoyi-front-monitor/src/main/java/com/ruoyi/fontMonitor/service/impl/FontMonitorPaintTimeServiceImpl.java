package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorPaintTimeMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorPaintTime;
import com.ruoyi.fontMonitor.service.IFontMonitorPaintTimeService;

/**
 * 监控渲染时间Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorPaintTimeServiceImpl implements IFontMonitorPaintTimeService 
{
    @Autowired
    private FontMonitorPaintTimeMapper fontMonitorPaintTimeMapper;

    /**
     * 查询监控渲染时间
     * 
     * @param id 监控渲染时间主键
     * @return 监控渲染时间
     */
    @Override
    public FontMonitorPaintTime selectFontMonitorPaintTimeById(Long id)
    {
        return fontMonitorPaintTimeMapper.selectFontMonitorPaintTimeById(id);
    }

    /**
     * 查询监控渲染时间列表
     * 
     * @param fontMonitorPaintTime 监控渲染时间
     * @return 监控渲染时间
     */
    @Override
    public List<FontMonitorPaintTime> selectFontMonitorPaintTimeList(FontMonitorPaintTime fontMonitorPaintTime)
    {
        return fontMonitorPaintTimeMapper.selectFontMonitorPaintTimeList(fontMonitorPaintTime);
    }

    /**
     * 新增监控渲染时间
     * 
     * @param fontMonitorPaintTime 监控渲染时间
     * @return 结果
     */
    @Override
    public int insertFontMonitorPaintTime(FontMonitorPaintTime fontMonitorPaintTime)
    {
        return fontMonitorPaintTimeMapper.insertFontMonitorPaintTime(fontMonitorPaintTime);
    }

    /**
     * 修改监控渲染时间
     * 
     * @param fontMonitorPaintTime 监控渲染时间
     * @return 结果
     */
    @Override
    public int updateFontMonitorPaintTime(FontMonitorPaintTime fontMonitorPaintTime)
    {
        return fontMonitorPaintTimeMapper.updateFontMonitorPaintTime(fontMonitorPaintTime);
    }

    /**
     * 批量删除监控渲染时间
     * 
     * @param ids 需要删除的监控渲染时间主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorPaintTimeByIds(Long[] ids)
    {
        return fontMonitorPaintTimeMapper.deleteFontMonitorPaintTimeByIds(ids);
    }

    /**
     * 删除监控渲染时间信息
     * 
     * @param id 监控渲染时间主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorPaintTimeById(Long id)
    {
        return fontMonitorPaintTimeMapper.deleteFontMonitorPaintTimeById(id);
    }
}
