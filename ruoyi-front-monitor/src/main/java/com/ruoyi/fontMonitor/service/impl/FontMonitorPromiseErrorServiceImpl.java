package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorPromiseErrorMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorPromiseError;
import com.ruoyi.fontMonitor.service.IFontMonitorPromiseErrorService;

/**
 * 监控 promiseErrorService业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorPromiseErrorServiceImpl implements IFontMonitorPromiseErrorService 
{
    @Autowired
    private FontMonitorPromiseErrorMapper fontMonitorPromiseErrorMapper;

    /**
     * 查询监控 promiseError
     * 
     * @param id 监控 promiseError主键
     * @return 监控 promiseError
     */
    @Override
    public FontMonitorPromiseError selectFontMonitorPromiseErrorById(Long id)
    {
        return fontMonitorPromiseErrorMapper.selectFontMonitorPromiseErrorById(id);
    }

    /**
     * 查询监控 promiseError列表
     * 
     * @param fontMonitorPromiseError 监控 promiseError
     * @return 监控 promiseError
     */
    @Override
    public List<FontMonitorPromiseError> selectFontMonitorPromiseErrorList(FontMonitorPromiseError fontMonitorPromiseError)
    {
        return fontMonitorPromiseErrorMapper.selectFontMonitorPromiseErrorList(fontMonitorPromiseError);
    }

    /**
     * 新增监控 promiseError
     * 
     * @param fontMonitorPromiseError 监控 promiseError
     * @return 结果
     */
    @Override
    public int insertFontMonitorPromiseError(FontMonitorPromiseError fontMonitorPromiseError)
    {
        return fontMonitorPromiseErrorMapper.insertFontMonitorPromiseError(fontMonitorPromiseError);
    }

    /**
     * 修改监控 promiseError
     * 
     * @param fontMonitorPromiseError 监控 promiseError
     * @return 结果
     */
    @Override
    public int updateFontMonitorPromiseError(FontMonitorPromiseError fontMonitorPromiseError)
    {
        return fontMonitorPromiseErrorMapper.updateFontMonitorPromiseError(fontMonitorPromiseError);
    }

    /**
     * 批量删除监控 promiseError
     * 
     * @param ids 需要删除的监控 promiseError主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorPromiseErrorByIds(Long[] ids)
    {
        return fontMonitorPromiseErrorMapper.deleteFontMonitorPromiseErrorByIds(ids);
    }

    /**
     * 删除监控 promiseError信息
     * 
     * @param id 监控 promiseError主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorPromiseErrorById(Long id)
    {
        return fontMonitorPromiseErrorMapper.deleteFontMonitorPromiseErrorById(id);
    }
}
