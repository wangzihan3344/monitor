package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorResourceErrorMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorResourceError;
import com.ruoyi.fontMonitor.service.IFontMonitorResourceErrorService;

/**
 * 监控 resourceErrorService业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorResourceErrorServiceImpl implements IFontMonitorResourceErrorService 
{
    @Autowired
    private FontMonitorResourceErrorMapper fontMonitorResourceErrorMapper;

    /**
     * 查询监控 resourceError
     * 
     * @param id 监控 resourceError主键
     * @return 监控 resourceError
     */
    @Override
    public FontMonitorResourceError selectFontMonitorResourceErrorById(Long id)
    {
        return fontMonitorResourceErrorMapper.selectFontMonitorResourceErrorById(id);
    }

    /**
     * 查询监控 resourceError列表
     * 
     * @param fontMonitorResourceError 监控 resourceError
     * @return 监控 resourceError
     */
    @Override
    public List<FontMonitorResourceError> selectFontMonitorResourceErrorList(FontMonitorResourceError fontMonitorResourceError)
    {
        return fontMonitorResourceErrorMapper.selectFontMonitorResourceErrorList(fontMonitorResourceError);
    }

    /**
     * 新增监控 resourceError
     * 
     * @param fontMonitorResourceError 监控 resourceError
     * @return 结果
     */
    @Override
    public int insertFontMonitorResourceError(FontMonitorResourceError fontMonitorResourceError)
    {
        return fontMonitorResourceErrorMapper.insertFontMonitorResourceError(fontMonitorResourceError);
    }

    /**
     * 修改监控 resourceError
     * 
     * @param fontMonitorResourceError 监控 resourceError
     * @return 结果
     */
    @Override
    public int updateFontMonitorResourceError(FontMonitorResourceError fontMonitorResourceError)
    {
        return fontMonitorResourceErrorMapper.updateFontMonitorResourceError(fontMonitorResourceError);
    }

    /**
     * 批量删除监控 resourceError
     * 
     * @param ids 需要删除的监控 resourceError主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorResourceErrorByIds(Long[] ids)
    {
        return fontMonitorResourceErrorMapper.deleteFontMonitorResourceErrorByIds(ids);
    }

    /**
     * 删除监控 resourceError信息
     * 
     * @param id 监控 resourceError主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorResourceErrorById(Long id)
    {
        return fontMonitorResourceErrorMapper.deleteFontMonitorResourceErrorById(id);
    }
}
