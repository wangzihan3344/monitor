package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorResourceMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorResource;
import com.ruoyi.fontMonitor.service.IFontMonitorResourceService;

/**
 * 资源监听Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorResourceServiceImpl implements IFontMonitorResourceService 
{
    @Autowired
    private FontMonitorResourceMapper fontMonitorResourceMapper;

    /**
     * 查询资源监听
     * 
     * @param id 资源监听主键
     * @return 资源监听
     */
    @Override
    public FontMonitorResource selectFontMonitorResourceById(Long id)
    {
        return fontMonitorResourceMapper.selectFontMonitorResourceById(id);
    }

    /**
     * 查询资源监听列表
     * 
     * @param fontMonitorResource 资源监听
     * @return 资源监听
     */
    @Override
    public List<FontMonitorResource> selectFontMonitorResourceList(FontMonitorResource fontMonitorResource)
    {
        return fontMonitorResourceMapper.selectFontMonitorResourceList(fontMonitorResource);
    }

    /**
     * 新增资源监听
     * 
     * @param fontMonitorResource 资源监听
     * @return 结果
     */
    @Override
    public int insertFontMonitorResource(FontMonitorResource fontMonitorResource)
    {
        return fontMonitorResourceMapper.insertFontMonitorResource(fontMonitorResource);
    }

    /**
     * 修改资源监听
     * 
     * @param fontMonitorResource 资源监听
     * @return 结果
     */
    @Override
    public int updateFontMonitorResource(FontMonitorResource fontMonitorResource)
    {
        return fontMonitorResourceMapper.updateFontMonitorResource(fontMonitorResource);
    }

    /**
     * 批量删除资源监听
     * 
     * @param ids 需要删除的资源监听主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorResourceByIds(Long[] ids)
    {
        return fontMonitorResourceMapper.deleteFontMonitorResourceByIds(ids);
    }

    /**
     * 删除资源监听信息
     * 
     * @param id 资源监听主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorResourceById(Long id)
    {
        return fontMonitorResourceMapper.deleteFontMonitorResourceById(id);
    }
}
