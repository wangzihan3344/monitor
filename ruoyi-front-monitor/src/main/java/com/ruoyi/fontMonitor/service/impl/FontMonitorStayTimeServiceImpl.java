package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorStayTimeMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorStayTime;
import com.ruoyi.fontMonitor.service.IFontMonitorStayTimeService;

/**
 * 记录停留时间Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorStayTimeServiceImpl implements IFontMonitorStayTimeService 
{
    @Autowired
    private FontMonitorStayTimeMapper fontMonitorStayTimeMapper;

    /**
     * 查询记录停留时间
     * 
     * @param id 记录停留时间主键
     * @return 记录停留时间
     */
    @Override
    public FontMonitorStayTime selectFontMonitorStayTimeById(Long id)
    {
        return fontMonitorStayTimeMapper.selectFontMonitorStayTimeById(id);
    }

    /**
     * 查询记录停留时间列表
     * 
     * @param fontMonitorStayTime 记录停留时间
     * @return 记录停留时间
     */
    @Override
    public List<FontMonitorStayTime> selectFontMonitorStayTimeList(FontMonitorStayTime fontMonitorStayTime)
    {
        return fontMonitorStayTimeMapper.selectFontMonitorStayTimeList(fontMonitorStayTime);
    }

    /**
     * 新增记录停留时间
     * 
     * @param fontMonitorStayTime 记录停留时间
     * @return 结果
     */
    @Override
    public int insertFontMonitorStayTime(FontMonitorStayTime fontMonitorStayTime)
    {
        return fontMonitorStayTimeMapper.insertFontMonitorStayTime(fontMonitorStayTime);
    }

    /**
     * 修改记录停留时间
     * 
     * @param fontMonitorStayTime 记录停留时间
     * @return 结果
     */
    @Override
    public int updateFontMonitorStayTime(FontMonitorStayTime fontMonitorStayTime)
    {
        return fontMonitorStayTimeMapper.updateFontMonitorStayTime(fontMonitorStayTime);
    }

    /**
     * 批量删除记录停留时间
     * 
     * @param ids 需要删除的记录停留时间主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorStayTimeByIds(Long[] ids)
    {
        return fontMonitorStayTimeMapper.deleteFontMonitorStayTimeByIds(ids);
    }

    /**
     * 删除记录停留时间信息
     * 
     * @param id 记录停留时间主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorStayTimeById(Long id)
    {
        return fontMonitorStayTimeMapper.deleteFontMonitorStayTimeById(id);
    }
}
