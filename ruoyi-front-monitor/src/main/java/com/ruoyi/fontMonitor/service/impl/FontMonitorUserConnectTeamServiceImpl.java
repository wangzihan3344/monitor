package com.ruoyi.fontMonitor.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorUserConnectTeamMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorUserConnectTeam;
import com.ruoyi.fontMonitor.service.IFontMonitorUserConnectTeamService;

/**
 * 用户和团队的关系Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
@Service
public class FontMonitorUserConnectTeamServiceImpl implements IFontMonitorUserConnectTeamService 
{
    @Autowired
    private FontMonitorUserConnectTeamMapper fontMonitorUserConnectTeamMapper;

    /**
     * 查询用户和团队的关系
     * 
     * @param id 用户和团队的关系主键
     * @return 用户和团队的关系
     */
    @Override
    public FontMonitorUserConnectTeam selectFontMonitorUserConnectTeamById(Long id)
    {
        return fontMonitorUserConnectTeamMapper.selectFontMonitorUserConnectTeamById(id);
    }

    /**
     * 查询用户和团队的关系列表
     * 
     * @param fontMonitorUserConnectTeam 用户和团队的关系
     * @return 用户和团队的关系
     */
    @Override
    public List<FontMonitorUserConnectTeam> selectFontMonitorUserConnectTeamList(FontMonitorUserConnectTeam fontMonitorUserConnectTeam)
    {
        return fontMonitorUserConnectTeamMapper.selectFontMonitorUserConnectTeamList(fontMonitorUserConnectTeam);
    }

    /**
     * 新增用户和团队的关系
     * 
     * @param fontMonitorUserConnectTeam 用户和团队的关系
     * @return 结果
     */
    @Override
    public int insertFontMonitorUserConnectTeam(FontMonitorUserConnectTeam fontMonitorUserConnectTeam)
    {
        fontMonitorUserConnectTeam.setCreateTime(DateUtils.getNowDate());
        return fontMonitorUserConnectTeamMapper.insertFontMonitorUserConnectTeam(fontMonitorUserConnectTeam);
    }

    /**
     * 修改用户和团队的关系
     * 
     * @param fontMonitorUserConnectTeam 用户和团队的关系
     * @return 结果
     */
    @Override
    public int updateFontMonitorUserConnectTeam(FontMonitorUserConnectTeam fontMonitorUserConnectTeam)
    {
        return fontMonitorUserConnectTeamMapper.updateFontMonitorUserConnectTeam(fontMonitorUserConnectTeam);
    }

    /**
     * 批量删除用户和团队的关系
     * 
     * @param ids 需要删除的用户和团队的关系主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorUserConnectTeamByIds(Long[] ids)
    {
        return fontMonitorUserConnectTeamMapper.deleteFontMonitorUserConnectTeamByIds(ids);
    }

    /**
     * 删除用户和团队的关系信息
     * 
     * @param id 用户和团队的关系主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorUserConnectTeamById(Long id)
    {
        return fontMonitorUserConnectTeamMapper.deleteFontMonitorUserConnectTeamById(id);
    }
}
