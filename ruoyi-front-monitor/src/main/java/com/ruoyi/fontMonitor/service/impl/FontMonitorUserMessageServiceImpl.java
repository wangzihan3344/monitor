package com.ruoyi.fontMonitor.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorUserMessageMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorUserMessage;
import com.ruoyi.fontMonitor.service.IFontMonitorUserMessageService;

/**
 * 用户信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
@Service
public class FontMonitorUserMessageServiceImpl implements IFontMonitorUserMessageService 
{
    @Autowired
    private FontMonitorUserMessageMapper fontMonitorUserMessageMapper;

    /**
     * 查询用户信息
     * 
     * @param id 用户信息主键
     * @return 用户信息
     */
    @Override
    public FontMonitorUserMessage selectFontMonitorUserMessageById(Long id)
    {
        return fontMonitorUserMessageMapper.selectFontMonitorUserMessageById(id);
    }

    /**
     * 查询用户信息列表
     * 
     * @param fontMonitorUserMessage 用户信息
     * @return 用户信息
     */
    @Override
    public List<FontMonitorUserMessage> selectFontMonitorUserMessageList(FontMonitorUserMessage fontMonitorUserMessage)
    {
        return fontMonitorUserMessageMapper.selectFontMonitorUserMessageList(fontMonitorUserMessage);
    }

    /**
     * 新增用户信息
     * 
     * @param fontMonitorUserMessage 用户信息
     * @return 结果
     */
    @Override
    public int insertFontMonitorUserMessage(FontMonitorUserMessage fontMonitorUserMessage)
    {
        fontMonitorUserMessage.setCreateTime(DateUtils.getNowDate());
        return fontMonitorUserMessageMapper.insertFontMonitorUserMessage(fontMonitorUserMessage);
    }

    /**
     * 修改用户信息
     * 
     * @param fontMonitorUserMessage 用户信息
     * @return 结果
     */
    @Override
    public int updateFontMonitorUserMessage(FontMonitorUserMessage fontMonitorUserMessage)
    {
        return fontMonitorUserMessageMapper.updateFontMonitorUserMessage(fontMonitorUserMessage);
    }

    /**
     * 批量删除用户信息
     * 
     * @param ids 需要删除的用户信息主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorUserMessageByIds(Long[] ids)
    {
        return fontMonitorUserMessageMapper.deleteFontMonitorUserMessageByIds(ids);
    }

    /**
     * 删除用户信息信息
     * 
     * @param id 用户信息主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorUserMessageById(Long id)
    {
        return fontMonitorUserMessageMapper.deleteFontMonitorUserMessageById(id);
    }
}
