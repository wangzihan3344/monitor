package com.ruoyi.fontMonitor.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorUserTeamProjectMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorUserTeamProject;
import com.ruoyi.fontMonitor.service.IFontMonitorUserTeamProjectService;

/**
 * 团队负责的项目Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
@Service
public class FontMonitorUserTeamProjectServiceImpl implements IFontMonitorUserTeamProjectService 
{
    @Autowired
    private FontMonitorUserTeamProjectMapper fontMonitorUserTeamProjectMapper;

    /**
     * 查询团队负责的项目
     * 
     * @param id 团队负责的项目主键
     * @return 团队负责的项目
     */
    @Override
    public FontMonitorUserTeamProject selectFontMonitorUserTeamProjectById(Long id)
    {
        return fontMonitorUserTeamProjectMapper.selectFontMonitorUserTeamProjectById(id);
    }

    /**
     * 查询团队负责的项目列表
     * 
     * @param fontMonitorUserTeamProject 团队负责的项目
     * @return 团队负责的项目
     */
    @Override
    public List<FontMonitorUserTeamProject> selectFontMonitorUserTeamProjectList(FontMonitorUserTeamProject fontMonitorUserTeamProject)
    {
        return fontMonitorUserTeamProjectMapper.selectFontMonitorUserTeamProjectList(fontMonitorUserTeamProject);
    }

    /**
     * 新增团队负责的项目
     * 
     * @param fontMonitorUserTeamProject 团队负责的项目
     * @return 结果
     */
    @Override
    public int insertFontMonitorUserTeamProject(FontMonitorUserTeamProject fontMonitorUserTeamProject)
    {
        fontMonitorUserTeamProject.setCreateTime(DateUtils.getNowDate());
        return fontMonitorUserTeamProjectMapper.insertFontMonitorUserTeamProject(fontMonitorUserTeamProject);
    }

    /**
     * 修改团队负责的项目
     * 
     * @param fontMonitorUserTeamProject 团队负责的项目
     * @return 结果
     */
    @Override
    public int updateFontMonitorUserTeamProject(FontMonitorUserTeamProject fontMonitorUserTeamProject)
    {
        return fontMonitorUserTeamProjectMapper.updateFontMonitorUserTeamProject(fontMonitorUserTeamProject);
    }

    /**
     * 批量删除团队负责的项目
     * 
     * @param ids 需要删除的团队负责的项目主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorUserTeamProjectByIds(Long[] ids)
    {
        return fontMonitorUserTeamProjectMapper.deleteFontMonitorUserTeamProjectByIds(ids);
    }

    /**
     * 删除团队负责的项目信息
     * 
     * @param id 团队负责的项目主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorUserTeamProjectById(Long id)
    {
        return fontMonitorUserTeamProjectMapper.deleteFontMonitorUserTeamProjectById(id);
    }
}
