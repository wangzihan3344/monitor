package com.ruoyi.fontMonitor.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorUserTeamMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorUserTeam;
import com.ruoyi.fontMonitor.service.IFontMonitorUserTeamService;

/**
 * 团队信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-27
 */
@Service
public class FontMonitorUserTeamServiceImpl implements IFontMonitorUserTeamService 
{
    @Autowired
    private FontMonitorUserTeamMapper fontMonitorUserTeamMapper;

    /**
     * 查询团队信息
     * 
     * @param id 团队信息主键
     * @return 团队信息
     */
    @Override
    public FontMonitorUserTeam selectFontMonitorUserTeamById(Long id)
    {
        return fontMonitorUserTeamMapper.selectFontMonitorUserTeamById(id);
    }

    /**
     * 查询团队信息列表
     * 
     * @param fontMonitorUserTeam 团队信息
     * @return 团队信息
     */
    @Override
    public List<FontMonitorUserTeam> selectFontMonitorUserTeamList(FontMonitorUserTeam fontMonitorUserTeam)
    {
        return fontMonitorUserTeamMapper.selectFontMonitorUserTeamList(fontMonitorUserTeam);
    }

    /**
     * 新增团队信息
     * 
     * @param fontMonitorUserTeam 团队信息
     * @return 结果
     */
    @Override
    public int insertFontMonitorUserTeam(FontMonitorUserTeam fontMonitorUserTeam)
    {
        fontMonitorUserTeam.setCreateTime(DateUtils.getNowDate());
        return fontMonitorUserTeamMapper.insertFontMonitorUserTeam(fontMonitorUserTeam);
    }

    /**
     * 修改团队信息
     * 
     * @param fontMonitorUserTeam 团队信息
     * @return 结果
     */
    @Override
    public int updateFontMonitorUserTeam(FontMonitorUserTeam fontMonitorUserTeam)
    {
        return fontMonitorUserTeamMapper.updateFontMonitorUserTeam(fontMonitorUserTeam);
    }

    /**
     * 批量删除团队信息
     * 
     * @param ids 需要删除的团队信息主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorUserTeamByIds(Long[] ids)
    {
        return fontMonitorUserTeamMapper.deleteFontMonitorUserTeamByIds(ids);
    }

    /**
     * 删除团队信息信息
     * 
     * @param id 团队信息主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorUserTeamById(Long id)
    {
        return fontMonitorUserTeamMapper.deleteFontMonitorUserTeamById(id);
    }
}
