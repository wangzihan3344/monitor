package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorVisitInfoMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorVisitInfo;
import com.ruoyi.fontMonitor.service.IFontMonitorVisitInfoService;

/**
 * 记录访问信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorVisitInfoServiceImpl implements IFontMonitorVisitInfoService 
{
    @Autowired
    private FontMonitorVisitInfoMapper fontMonitorVisitInfoMapper;

    /**
     * 查询记录访问信息
     * 
     * @param id 记录访问信息主键
     * @return 记录访问信息
     */
    @Override
    public FontMonitorVisitInfo selectFontMonitorVisitInfoById(Long id)
    {
        return fontMonitorVisitInfoMapper.selectFontMonitorVisitInfoById(id);
    }

    /**
     * 查询记录访问信息列表
     * 
     * @param fontMonitorVisitInfo 记录访问信息
     * @return 记录访问信息
     */
    @Override
    public List<FontMonitorVisitInfo> selectFontMonitorVisitInfoList(FontMonitorVisitInfo fontMonitorVisitInfo)
    {
        return fontMonitorVisitInfoMapper.selectFontMonitorVisitInfoList(fontMonitorVisitInfo);
    }

    /**
     * 新增记录访问信息
     * 
     * @param fontMonitorVisitInfo 记录访问信息
     * @return 结果
     */
    @Override
    public int insertFontMonitorVisitInfo(FontMonitorVisitInfo fontMonitorVisitInfo)
    {
        return fontMonitorVisitInfoMapper.insertFontMonitorVisitInfo(fontMonitorVisitInfo);
    }

    /**
     * 修改记录访问信息
     * 
     * @param fontMonitorVisitInfo 记录访问信息
     * @return 结果
     */
    @Override
    public int updateFontMonitorVisitInfo(FontMonitorVisitInfo fontMonitorVisitInfo)
    {
        return fontMonitorVisitInfoMapper.updateFontMonitorVisitInfo(fontMonitorVisitInfo);
    }

    /**
     * 批量删除记录访问信息
     * 
     * @param ids 需要删除的记录访问信息主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorVisitInfoByIds(Long[] ids)
    {
        return fontMonitorVisitInfoMapper.deleteFontMonitorVisitInfoByIds(ids);
    }

    /**
     * 删除记录访问信息信息
     * 
     * @param id 记录访问信息主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorVisitInfoById(Long id)
    {
        return fontMonitorVisitInfoMapper.deleteFontMonitorVisitInfoById(id);
    }

    @Override
    public Boolean isExternalWebsite(String resourceUrl, String url) {
        if (resourceUrl == null)
            return true;
        String[] resourceUrlPart1 = resourceUrl.split("://");
        String[] resourceUrlPart2 = resourceUrlPart1[1].split("/");
        String[] urlPart1 = url.split("://");
        String[] urlPart2 = urlPart1[1].split("/");
        return !resourceUrlPart2[0].equals(urlPart2[0]);
    }
}
