package com.ruoyi.fontMonitor.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fontMonitor.mapper.FontMonitorXhrInfoMapper;
import com.ruoyi.fontMonitor.domain.FontMonitorXhrInfo;
import com.ruoyi.fontMonitor.service.IFontMonitorXhrInfoService;

/**
 * 监控 xhr 接口Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-31
 */
@Service
public class FontMonitorXhrInfoServiceImpl implements IFontMonitorXhrInfoService 
{
    @Autowired
    private FontMonitorXhrInfoMapper fontMonitorXhrInfoMapper;

    /**
     * 查询监控 xhr 接口
     * 
     * @param id 监控 xhr 接口主键
     * @return 监控 xhr 接口
     */
    @Override
    public FontMonitorXhrInfo selectFontMonitorXhrInfoById(Long id)
    {
        return fontMonitorXhrInfoMapper.selectFontMonitorXhrInfoById(id);
    }

    /**
     * 查询监控 xhr 接口列表
     * 
     * @param fontMonitorXhrInfo 监控 xhr 接口
     * @return 监控 xhr 接口
     */
    @Override
    public List<FontMonitorXhrInfo> selectFontMonitorXhrInfoList(FontMonitorXhrInfo fontMonitorXhrInfo)
    {
        return fontMonitorXhrInfoMapper.selectFontMonitorXhrInfoList(fontMonitorXhrInfo);
    }

    /**
     * 新增监控 xhr 接口
     * 
     * @param fontMonitorXhrInfo 监控 xhr 接口
     * @return 结果
     */
    @Override
    public int insertFontMonitorXhrInfo(FontMonitorXhrInfo fontMonitorXhrInfo)
    {
        return fontMonitorXhrInfoMapper.insertFontMonitorXhrInfo(fontMonitorXhrInfo);
    }

    /**
     * 修改监控 xhr 接口
     * 
     * @param fontMonitorXhrInfo 监控 xhr 接口
     * @return 结果
     */
    @Override
    public int updateFontMonitorXhrInfo(FontMonitorXhrInfo fontMonitorXhrInfo)
    {
        return fontMonitorXhrInfoMapper.updateFontMonitorXhrInfo(fontMonitorXhrInfo);
    }

    /**
     * 批量删除监控 xhr 接口
     * 
     * @param ids 需要删除的监控 xhr 接口主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorXhrInfoByIds(Long[] ids)
    {
        return fontMonitorXhrInfoMapper.deleteFontMonitorXhrInfoByIds(ids);
    }

    /**
     * 删除监控 xhr 接口信息
     * 
     * @param id 监控 xhr 接口主键
     * @return 结果
     */
    @Override
    public int deleteFontMonitorXhrInfoById(Long id)
    {
        return fontMonitorXhrInfoMapper.deleteFontMonitorXhrInfoById(id);
    }
}
