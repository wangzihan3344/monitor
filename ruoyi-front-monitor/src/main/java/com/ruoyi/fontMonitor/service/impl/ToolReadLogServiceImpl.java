package com.ruoyi.fontMonitor.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.fontMonitor.Common.IPAddressSearchUtils;
import com.ruoyi.fontMonitor.domain.*;
import com.ruoyi.fontMonitor.service.*;
import com.ruoyi.system.service.ISysDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@SuppressWarnings("all")
public class ToolReadLogServiceImpl implements IToolReadLogService {

    @Value("${readLogs.address}")
    private String fileAddress;

    @Value("${readLogs.pointKey}")
    private String pointKey;

    @Value("${readLogs.testAddress}")
    private String fileTestAddress;

    @Autowired
    private ISysDictDataService dictDataService;

    @Autowired
    private IEnhanceProjectService enhanceProjectService;

    @Autowired
    private IFontMonitorMainService mainService;

    @Autowired
    private IFontMonitorJsErrorService jsErrorService;

    @Autowired
    private IFontMonitorPromiseErrorService promiseErrorService;

    @Autowired
    private IFontMonitorResourceErrorService resourceErrorService;

    @Autowired
    private IFontMonitorResourceService resourceService;

    @Autowired
    private IFontMonitorXhrInfoService xhrInfoService;

    @Autowired
    private IFontMonitorFetchInfoService fetchInfoService;

    @Autowired
    private IFontMonitorBlankScreenService blankScreenService;

    @Autowired
    private IFontMonitorLoadTimeService loadTimeService;

    @Autowired
    private IFontMonitorPaintTimeService paintTimeService;

    @Autowired
    private IFontMonitorFirstInputDelayService firstInputDelayService;

    @Autowired
    private IFontMonitorLongTaskService longTaskService;

    @Autowired
    private IFontMonitorVisitInfoService visitInfoService;

    @Autowired
    private IFontMonitorStayTimeService stayTimeService;

    @Autowired
    private IFontMonitorHashService hashService;

    @Autowired
    private IFontMonitorHistoryService historyService;

    /**
     * 前端监控的字典数据
     */
    private SysDictData monitorTypeDictData;

    public String getLogsName() {
        Date nowDate = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();//格式化时间
        simpleDateFormat.applyPattern("yyyy-MM-dd-HH");
        //创建Calendar实例
        Calendar cal = Calendar.getInstance();
        //设置当前时间
        cal.setTime(nowDate);
        //在当前时间基础上减一小时
        cal.add(Calendar.HOUR, -1);
        //将路径组装成“总路径/日期文件名.log”的形式
        return fileAddress + simpleDateFormat.format(cal.getTime()) + ".log";
    }

    public ArrayList<String> readLog(String fileAddress) {
        ArrayList<String> log = new ArrayList<>();
        try {
            if (fileAddress == null)
                fileAddress = fileTestAddress;
            System.out.println("读取文件名为：" + fileAddress);
            BufferedReader in = new BufferedReader(new FileReader(fileAddress));
            String str;
            while ((str = in.readLine()) != null) {
                log.add(str);
            }
            in.close();
        } catch (IOException e) {
            System.out.println("文件读取错误");
            throw new RuntimeException("文件读取错误");
        }
        return log;
    }

    public ArrayList<String> screenLogs(ArrayList<String> logs) {
        ArrayList<String> requirementsLogs = new ArrayList<>();
        //通过pointKey筛选记录，直接获取我们需要的数据
        for (int i = 0; i < logs.size(); i++) {
            String[] decomposeLogs = logs.get(i).split(pointKey);
            //获取ip
            String ip = "";
            // 这个正则存在问题，暂时保留
            // String regular = "\\d{3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}";
            String regular = "((25[0-5]|2[0-4]\\d|((1\\d{2})|([1-9]?\\d)))\\.){3}(25[0-5]|2[0-4]\\d|((1\\d{2})|([1-9]?\\d)))";
            //或者下面的方式都可以
            //String regular = "(?<=//|)((\\w)+\\.)+\\w+";
            String str = decomposeLogs[0];
            Pattern pattern = Pattern.compile(regular);
            Matcher matcher = pattern.matcher(str);
            if (matcher.find()){
                ip = matcher.group();
            }
            //获取传输主体
            if (decomposeLogs.length != 1) {
                decomposeLogs = decomposeLogs[1].split(" ");
                //将ip拼接到 decomposeLogs[0] 后面，这样在后续操作可以提取
                requirementsLogs.add(decomposeLogs[0] + "&ip=" + ip);
            }
        }
        return requirementsLogs;
    }

    public ArrayList logsStringConversionJSON(ArrayList<String> requirementsLogs) {
        ArrayList requirementsJSON = new ArrayList();
        // 对每一条记录进行操作
        for (int i = 0; i < requirementsLogs.size(); i++) {
            JSONObject conversionJSON = new JSONObject();
            //将每一条记录进行拆解
            String[] decomposeLogs = requirementsLogs.get(i).split("&");
            for (int j = 0; j < decomposeLogs.length; j++) {
                //将拆解后的记录以键值对的形式存进JSON
                String[] keyAndValue = decomposeLogs[j].split("=");
                if (keyAndValue.length == 1)
                    conversionJSON.put(keyAndValue[0], null);
                else
                    conversionJSON.put(keyAndValue[0], keyAndValue[1]);
            }
            requirementsJSON.add(conversionJSON);
        }
        return requirementsJSON;
    }

    public void dataRecordedInDatabase(ArrayList requirementsJSON) {
        //根据type获取字典相应的data记录合集，需要向system微服务获取字典数据
        //JSONObject dictSearchResult = feignClientSystemDic.searchSystemDictDataByDictType("font_monitor_type");
        //monitorTypeDictData = (List<SysDictData>)dictSearchResult.get("data");
        for (int i = 0; i < requirementsJSON.size(); i++) {
            //取出每一个json
            JSONObject part = (JSONObject) requirementsJSON.get(i);
            try {
                savePartData(part, i);
            } catch (RuntimeException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * 保存单条记录
     *
     * @param part  单条记录
     * @param index 当天的第几条记录
     */
    @Transactional
    public void savePartData(JSONObject part, int index) {
        //识别type，根据type进行分类保存手
        String type = part.getString("type");
        if (type == null)
            throw new RuntimeException("第" + index + "条记录出现错误,无传输类型");
        //处理公共部分
        FontMonitorMain monitorMain = new FontMonitorMain();
        //将type在字典中monitorType对应的的Id记录进数据库
        //分部查询的方式，每次需要时查询
        try {
            SysDictData searchDictData = new SysDictData();
            searchDictData.setDictLabel(type);
            List<SysDictData> searchResult = dictDataService.selectDictDataList(searchDictData);
            monitorTypeDictData = searchResult.get(0);
            monitorMain.setTypeId(monitorTypeDictData.getDictValue());
        } catch (Exception e) {
            throw new RuntimeException("查询不到第" + index + "条的相关字典记录");
        }
        //验证pid存在性
        try {
            if (part.getString("pid") == null)
                throw new RuntimeException("无pid");
            enhanceProjectService.searchProjectByPid(part.getString("pid"));
        } catch (RuntimeException e) {
            throw new RuntimeException("第" + index + "条记录出现错误," + e.getMessage());
        }
        //拿pid、uuid、title、url、browser、OS、production、timestamp、ip
        monitorMain.setMainId(getMainId(index));
        monitorMain.setPid(part.getString("pid"));
        monitorMain.setUuid(part.getString("uuid"));
        monitorMain.setTitle(part.getString("title"));
        monitorMain.setUrl(part.getString("url"));
        monitorMain.setIp(part.getString("ip"));
        //通过ip拿到用户所在地然后填入数据库，理论上来讲，这个地方需要做一个缓存，避免重复提交数据
        JSONObject ipAddress = IPAddressSearchUtils.getRealAddress(monitorMain.getIp());
        monitorMain.setIpCountry(ipAddress.getString("country"));
        monitorMain.setIpProvince(ipAddress.getString("province"));
        monitorMain.setIpCity(ipAddress.getString("city"));
        //判断一下用户是否为老用户
        if (monitorMain.getUuid() == null)
            throw new RuntimeException("uuid为空");
        FontMonitorMain searchUuid = new FontMonitorMain();
        searchUuid.setUuid(monitorMain.getUuid());
        List<FontMonitorMain> mainList = mainService.selectFontMonitorMainList(searchUuid);
        if (mainList.size() > 0)
            monitorMain.setOldUserFlag(1);
        else
            monitorMain.setOldUserFlag(0);
        // 获取最后一条记录，判断上一回的记录是否已结束
        if (mainList.size() > 0) {
            searchUuid = mainList.get(mainList.size() - 1);
            if (searchUuid.getFirstTimestamp() != null && !searchUuid.getTypeId().equals("12"))
                monitorMain.setFirstTimestamp(searchUuid.getFirstTimestamp());
            else
                monitorMain.setFirstTimestamp(new Date());
        } else {
            monitorMain.setFirstTimestamp(new Date());
        }
        //timestamp为空时这里会报错，所以判断一下
        if (part.getLong("timestamp") != null)
            monitorMain.setReportTimestamp(new Date(part.getLong("timestamp")));
        monitorMain.setBrowser(part.getString("browser"));
        monitorMain.setOs(part.getString("OS"));
        //ip要通过nginx获取
        monitorMain.setIp(part.getString("ip"));
        monitorMain.setProduction(part.getString("production"));
        mainService.insertFontMonitorMain(monitorMain);
        //根据type选择相应的数据库表单添加记录
        switch (type) {
            case "jsError":
                FontMonitorJsError jsError = new FontMonitorJsError();
                jsError.setMainId(monitorMain.getMainId());
                jsError.setMessage(part.getString("message"));
                jsError.setFileName(part.getString("filename"));
                jsError.setFileName(part.getString("position"));
                jsError.setStack(part.getString("stack"));
                jsError.setSelector(part.getString("selector"));
                jsErrorService.insertFontMonitorJsError(jsError);
                break;
            case "promiseError":
                FontMonitorPromiseError promiseError = new FontMonitorPromiseError();
                promiseError.setMainId(monitorMain.getMainId());
                promiseError.setMessage(part.getString("message"));
                promiseError.setStack(part.getString("stack"));
                promiseError.setSelector(part.getString("selector"));
                promiseErrorService.insertFontMonitorPromiseError(promiseError);
                break;
            case "resourceError":
                FontMonitorResourceError resourceError = new FontMonitorResourceError();
                resourceError.setMianId(monitorMain.getMainId());
                resourceError.setFilename(part.getString("filename"));
                resourceError.setTagName(part.getString("tagName"));
                resourceError.setTriggerTimestamp(part.getString("triggerTimestamp"));
                resourceError.setSelector(part.getString("selector"));
                resourceErrorService.insertFontMonitorResourceError(resourceError);
                break;
            case "resource":
                FontMonitorResource resource = new FontMonitorResource();
                resource.setMainId(monitorMain.getMainId());
                resource.setName(part.getString("name"));
                resource.setDuration(part.getDouble("duration"));
                resource.setEncodedBodySize(part.getString("encoded_body_size"));
                resource.setParseDnsTime(part.getDouble("parse_DNS_time"));
                resource.setConnectTime(part.getDouble("connectTime"));
                resource.setTtfbTime(part.getDouble("ttfbTime"));
                resource.setResponseTime(part.getDouble("responseTime"));
                resourceService.insertFontMonitorResource(resource);
                break;
            case "xhr":
                FontMonitorXhrInfo xhrInfo = new FontMonitorXhrInfo();
                xhrInfo.setMianId(monitorMain.getMainId());
                xhrInfo.setPathName(part.getString("pathname"));
                xhrInfo.setStatus(part.getString("status"));
                xhrInfo.setDuration(part.getString("duration"));
                xhrInfo.setRequestParams(part.getString("params"));
                xhrInfo.setEventType(part.getString("eventType"));
                xhrInfo.setResponseData(part.getString("response"));
                xhrInfoService.insertFontMonitorXhrInfo(xhrInfo);
                break;
            case "fetch":
                FontMonitorFetchInfo fetchInfo = new FontMonitorFetchInfo();
                fetchInfo.setMainId(monitorMain.getMainId());
                fetchInfo.setPathName(part.getString("pathName"));
                fetchInfo.setStatus(part.getString("status"));
                fetchInfo.setDuration(part.getString("duration"));
                fetchInfo.setRequestParams(part.getString("params"));
                fetchInfo.setResponseData(part.getString("response"));
                fetchInfoService.insertFontMonitorFetchInfo(fetchInfo);
                break;
            case "blank":
                FontMonitorBlankScreen blankScreen = new FontMonitorBlankScreen();
                blankScreen.setMainId(monitorMain.getMainId());
                blankScreen.setEmptyPoints(part.getString("emptyPoints"));
                blankScreen.setScreen(part.getString("screen"));
                blankScreen.setViewPoint(part.getString("viewPoint"));
                blankScreen.setSelector(part.getString("selector"));
                blankScreenService.insertFontMonitorBlankScreen(blankScreen);
                break;
            case "timing":
                //可能少字段了：parseDNSTime
                FontMonitorLoadTime loadTime = new FontMonitorLoadTime();
                loadTime.setMainId(monitorMain.getMainId());
                loadTime.setConnectTime(part.getString("connectTime"));
                loadTime.setTtfbTime(part.getString("ttfbTime"));
                loadTime.setResponseTime(part.getString("responseTime"));
                loadTime.setParseDomTime(part.getString("parseDOMTime"));
                loadTime.setDomContentLoadedTime(part.getString("domContentLoadedTime"));
                loadTime.setTimeToInteractive(part.getString("timeToInteractive"));
                loadTime.setLoadTime(part.getString("loadTime"));
                loadTimeService.insertFontMonitorLoadTime(loadTime);
                break;
            case "paint":
                FontMonitorPaintTime paintTime = new FontMonitorPaintTime();
                paintTime.setMainId(monitorMain.getMainId());
                paintTime.setFirstPaint(part.getString("firstPaint"));
                paintTime.setFirstContentPaint(part.getString("firstContentPaint"));
                paintTime.setFirstMeaningfulPaint(part.getString("firstMeaningfulPaint"));
                paintTime.setLargestContentfulPaint(part.getString("largestContentfulPaint"));
                paintTimeService.insertFontMonitorPaintTime(paintTime);
                break;
            case "firstInputDelay":
                FontMonitorFirstInputDelay firstInputDelay = new FontMonitorFirstInputDelay();
                firstInputDelay.setMainId(monitorMain.getMainId());
                firstInputDelay.setInputDelay(part.getString("inputDelay"));
                firstInputDelay.setDuration(part.getString("duration"));
                firstInputDelay.setStartTime(part.getString("startTime"));
                firstInputDelay.setSelector(part.getString("selector"));
                firstInputDelayService.insertFontMonitorFirstInputDelay(firstInputDelay);
                break;
            case "longTask":
                FontMonitorLongTask longTask = new FontMonitorLongTask();
                longTask.setMianId(monitorMain.getMainId());
                longTask.setEventType(part.getString("eventType"));
                longTask.setStartTime(part.getString("startTime"));
                longTask.setDuration(part.getString("duration"));
                longTask.setSelector(part.getString("selector"));
                longTaskService.insertFontMonitorLongTask(longTask);
                break;
            case "pv":
                FontMonitorVisitInfo visitInfo = new FontMonitorVisitInfo();
                visitInfo.setEffectiveType(part.getString("effectiveType"));
                visitInfo.setRtt(part.getString("rtt"));
                visitInfo.setScreen(part.getString("screen"));
                visitInfo.setSourceUrl(part.getString("source"));
                visitInfo.setExternalWebsiteFlag(visitInfoService.isExternalWebsite(visitInfo.getSourceUrl(), monitorMain.getUrl()));
                visitInfo.setMainId(monitorMain.getMainId());
                visitInfoService.insertFontMonitorVisitInfo(visitInfo);
                break;
            case "stayTime":
                FontMonitorStayTime stayTime = new FontMonitorStayTime();
                stayTime.setMainId(monitorMain.getMainId());
                stayTime.setStayTime(part.getLong("stayTime"));
                stayTimeService.insertFontMonitorStayTime(stayTime);
                break;
            case "hash":
                FontMonitorHash hash = new FontMonitorHash();
                hash.setMianId(monitorMain.getMainId());
                hash.setOldUrl(part.getString("oldUrl"));
                hash.setNewUrl(part.getString("newUrl"));
                hashService.insertFontMonitorHash(hash);
                break;
            case "history":
                FontMonitorHistory history = new FontMonitorHistory();
                history.setMianId(monitorMain.getMainId());
                history.setCurrent(part.getString("current"));
                history.setBack(part.getString("back"));
                history.setForward(part.getString("forward"));
                historyService.insertFontMonitorHistory(history);
                break;
            case "customMessage":
                FontMonitorCustomMessage customMessage = new FontMonitorCustomMessage();
                customMessage.setMainId(monitorMain.getMainId());
                customMessage.setMessage(part.getString("message"));
            default:
                throw new RuntimeException("暂时没有这种类型");
        }
    }

    /**
     * 获取主表id
     *
     * @param index 当天第几条记录
     * @return 主表id
     */
    public String getMainId(int index) {
        Long nowTimestamp = System.currentTimeMillis() / 1000;
        String mainId = nowTimestamp.toString();
        return mainId + index;
    }
}
