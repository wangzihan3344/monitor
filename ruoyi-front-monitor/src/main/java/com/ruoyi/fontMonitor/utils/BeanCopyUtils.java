package com.ruoyi.fontMonitor.utils;

import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Bean拷贝工具类
 *
 * @author Droplet
 * @Date 2022-11-03
 */
public class BeanCopyUtils {
    private BeanCopyUtils() {

    }

    /**
     * 单个Bean拷贝
     *
     * @param source 来源
     * @param clazz  目标对象字节码
     * @return 目标对象
     */
    public static <V> V copyBean(Object source, Class<V> clazz) {
        V result = null;
        try {
            result = clazz.getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(source, result);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    /**
     * Bean List集合拷贝
     *
     * @param list  Bean List集合
     * @param clazz 目标对象字节码
     * @return 目标对象集合
     */
    public static <O, V> List<V> copyBeanList(List<O> list, Class<V> clazz) {
        return list.stream()
                .map(o -> copyBean(o, clazz))
                .collect(Collectors.toList());
    }
}
