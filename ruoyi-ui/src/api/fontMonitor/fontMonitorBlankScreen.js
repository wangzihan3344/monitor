import request from '@/utils/request'

// 查询监控白屏列表
export function listFontMonitorBlankScreen(query) {
  return request({
    url: '/fontMonitor/fontMonitorBlankScreen/list',
    method: 'get',
    params: query
  })
}

// 查询监控白屏详细
export function getFontMonitorBlankScreen(id) {
  return request({
    url: '/fontMonitor/fontMonitorBlankScreen/' + id,
    method: 'get'
  })
}

// 新增监控白屏
export function addFontMonitorBlankScreen(data) {
  return request({
    url: '/fontMonitor/fontMonitorBlankScreen',
    method: 'post',
    data: data
  })
}

// 修改监控白屏
export function updateFontMonitorBlankScreen(data) {
  return request({
    url: '/fontMonitor/fontMonitorBlankScreen',
    method: 'put',
    data: data
  })
}

// 删除监控白屏
export function delFontMonitorBlankScreen(id) {
  return request({
    url: '/fontMonitor/fontMonitorBlankScreen/' + id,
    method: 'delete'
  })
}
