import request from '@/utils/request'

// 查询custom_error 的 message 数据列表
export function listFontMonitorCustomMessage(query) {
  return request({
    url: '/fontMonitor/fontMonitorCustomMessage/list',
    method: 'get',
    params: query
  })
}

// 查询custom_error 的 message 数据详细
export function getFontMonitorCustomMessage(id) {
  return request({
    url: '/fontMonitor/fontMonitorCustomMessage/' + id,
    method: 'get'
  })
}

// 新增custom_error 的 message 数据
export function addFontMonitorCustomMessage(data) {
  return request({
    url: '/fontMonitor/fontMonitorCustomMessage',
    method: 'post',
    data: data
  })
}

// 修改custom_error 的 message 数据
export function updateFontMonitorCustomMessage(data) {
  return request({
    url: '/fontMonitor/fontMonitorCustomMessage',
    method: 'put',
    data: data
  })
}

// 删除custom_error 的 message 数据
export function delFontMonitorCustomMessage(id) {
  return request({
    url: '/fontMonitor/fontMonitorCustomMessage/' + id,
    method: 'delete'
  })
}
