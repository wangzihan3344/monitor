import request from '@/utils/request'

// 查询监控 fetch 接口列表
export function listFontMonitorFetchInfo(query) {
  return request({
    url: '/fontMonitor/fontMonitorFetchInfo/list',
    method: 'get',
    params: query
  })
}

// 查询监控 fetch 接口详细
export function getFontMonitorFetchInfo(id) {
  return request({
    url: '/fontMonitor/fontMonitorFetchInfo/' + id,
    method: 'get'
  })
}

// 新增监控 fetch 接口
export function addFontMonitorFetchInfo(data) {
  return request({
    url: '/fontMonitor/fontMonitorFetchInfo',
    method: 'post',
    data: data
  })
}

// 修改监控 fetch 接口
export function updateFontMonitorFetchInfo(data) {
  return request({
    url: '/fontMonitor/fontMonitorFetchInfo',
    method: 'put',
    data: data
  })
}

// 删除监控 fetch 接口
export function delFontMonitorFetchInfo(id) {
  return request({
    url: '/fontMonitor/fontMonitorFetchInfo/' + id,
    method: 'delete'
  })
}
