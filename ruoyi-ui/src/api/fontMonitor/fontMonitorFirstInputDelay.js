import request from '@/utils/request'

// 查询监控首次输入响应延迟列表
export function listFontMonitorFirstInputDelay(query) {
  return request({
    url: '/fontMonitor/fontMonitorFirstInputDelay/list',
    method: 'get',
    params: query
  })
}

// 查询监控首次输入响应延迟详细
export function getFontMonitorFirstInputDelay(id) {
  return request({
    url: '/fontMonitor/fontMonitorFirstInputDelay/' + id,
    method: 'get'
  })
}

// 新增监控首次输入响应延迟
export function addFontMonitorFirstInputDelay(data) {
  return request({
    url: '/fontMonitor/fontMonitorFirstInputDelay',
    method: 'post',
    data: data
  })
}

// 修改监控首次输入响应延迟
export function updateFontMonitorFirstInputDelay(data) {
  return request({
    url: '/fontMonitor/fontMonitorFirstInputDelay',
    method: 'put',
    data: data
  })
}

// 删除监控首次输入响应延迟
export function delFontMonitorFirstInputDelay(id) {
  return request({
    url: '/fontMonitor/fontMonitorFirstInputDelay/' + id,
    method: 'delete'
  })
}
