import request from '@/utils/request'

// 查询url变动列表
export function listFontMonitorHash(query) {
  return request({
    url: '/fontMonitor/fontMonitorHash/list',
    method: 'get',
    params: query
  })
}

// 查询url变动详细
export function getFontMonitorHash(id) {
  return request({
    url: '/fontMonitor/fontMonitorHash/' + id,
    method: 'get'
  })
}

// 新增url变动
export function addFontMonitorHash(data) {
  return request({
    url: '/fontMonitor/fontMonitorHash',
    method: 'post',
    data: data
  })
}

// 修改url变动
export function updateFontMonitorHash(data) {
  return request({
    url: '/fontMonitor/fontMonitorHash',
    method: 'put',
    data: data
  })
}

// 删除url变动
export function delFontMonitorHash(id) {
  return request({
    url: '/fontMonitor/fontMonitorHash/' + id,
    method: 'delete'
  })
}
