import request from '@/utils/request'

// 查询路由记录列表
export function listFontMonitorHistory(query) {
  return request({
    url: '/fontMonitor/fontMonitorHistory/list',
    method: 'get',
    params: query
  })
}

// 查询路由记录详细
export function getFontMonitorHistory(id) {
  return request({
    url: '/fontMonitor/fontMonitorHistory/' + id,
    method: 'get'
  })
}

// 新增路由记录
export function addFontMonitorHistory(data) {
  return request({
    url: '/fontMonitor/fontMonitorHistory',
    method: 'post',
    data: data
  })
}

// 修改路由记录
export function updateFontMonitorHistory(data) {
  return request({
    url: '/fontMonitor/fontMonitorHistory',
    method: 'put',
    data: data
  })
}

// 删除路由记录
export function delFontMonitorHistory(id) {
  return request({
    url: '/fontMonitor/fontMonitorHistory/' + id,
    method: 'delete'
  })
}
