import request from '@/utils/request'

// 查询监控 jsError列表
export function listFontMonitorJsError(query) {
  return request({
    url: '/fontMonitor/fontMonitorJsError/list',
    method: 'get',
    params: query
  })
}

// 查询监控 jsError详细
export function getFontMonitorJsError(id) {
  return request({
    url: '/fontMonitor/fontMonitorJsError/' + id,
    method: 'get'
  })
}

// 新增监控 jsError
export function addFontMonitorJsError(data) {
  return request({
    url: '/fontMonitor/fontMonitorJsError',
    method: 'post',
    data: data
  })
}

// 修改监控 jsError
export function updateFontMonitorJsError(data) {
  return request({
    url: '/fontMonitor/fontMonitorJsError',
    method: 'put',
    data: data
  })
}

// 删除监控 jsError
export function delFontMonitorJsError(id) {
  return request({
    url: '/fontMonitor/fontMonitorJsError/' + id,
    method: 'delete'
  })
}
