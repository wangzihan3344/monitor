import request from '@/utils/request'

// 查询监控加载时间列表
export function listFontMonitorLoadTime(query) {
  return request({
    url: '/fontMonitor/fontMonitorLoadTime/list',
    method: 'get',
    params: query
  })
}

// 查询监控加载时间详细
export function getFontMonitorLoadTime(id) {
  return request({
    url: '/fontMonitor/fontMonitorLoadTime/' + id,
    method: 'get'
  })
}

// 新增监控加载时间
export function addFontMonitorLoadTime(data) {
  return request({
    url: '/fontMonitor/fontMonitorLoadTime',
    method: 'post',
    data: data
  })
}

// 修改监控加载时间
export function updateFontMonitorLoadTime(data) {
  return request({
    url: '/fontMonitor/fontMonitorLoadTime',
    method: 'put',
    data: data
  })
}

// 删除监控加载时间
export function delFontMonitorLoadTime(id) {
  return request({
    url: '/fontMonitor/fontMonitorLoadTime/' + id,
    method: 'delete'
  })
}
