import request from '@/utils/request'

// 查询监控卡顿列表
export function listFontMonitorLongTask(query) {
  return request({
    url: '/fontMonitor/fontMonitorLongTask/list',
    method: 'get',
    params: query
  })
}

// 查询监控卡顿详细
export function getFontMonitorLongTask(id) {
  return request({
    url: '/fontMonitor/fontMonitorLongTask/' + id,
    method: 'get'
  })
}

// 新增监控卡顿
export function addFontMonitorLongTask(data) {
  return request({
    url: '/fontMonitor/fontMonitorLongTask',
    method: 'post',
    data: data
  })
}

// 修改监控卡顿
export function updateFontMonitorLongTask(data) {
  return request({
    url: '/fontMonitor/fontMonitorLongTask',
    method: 'put',
    data: data
  })
}

// 删除监控卡顿
export function delFontMonitorLongTask(id) {
  return request({
    url: '/fontMonitor/fontMonitorLongTask/' + id,
    method: 'delete'
  })
}
