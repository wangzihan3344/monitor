import request from '@/utils/request'

// 查询公共字段列表
export function listFontMonitorMain(query) {
  return request({
    url: '/fontMonitor/fontMonitorMain/list',
    method: 'get',
    params: query
  })
}

// 查询公共字段详细
export function getFontMonitorMain(id) {
  return request({
    url: '/fontMonitor/fontMonitorMain/' + id,
    method: 'get'
  })
}

// 新增公共字段
export function addFontMonitorMain(data) {
  return request({
    url: '/fontMonitor/fontMonitorMain',
    method: 'post',
    data: data
  })
}

// 修改公共字段
export function updateFontMonitorMain(data) {
  return request({
    url: '/fontMonitor/fontMonitorMain',
    method: 'put',
    data: data
  })
}

// 删除公共字段
export function delFontMonitorMain(id) {
  return request({
    url: '/fontMonitor/fontMonitorMain/' + id,
    method: 'delete'
  })
}
