import request from '@/utils/request'

// 查询用户信息列表
export function listFontMonitorMessage(query) {
  return request({
    url: '/fontMonitor/fontMonitorMessage/list',
    method: 'get',
    params: query
  })
}

// 查询用户信息详细
export function getFontMonitorMessage(id) {
  return request({
    url: '/fontMonitor/fontMonitorMessage/' + id,
    method: 'get'
  })
}

// 新增用户信息
export function addFontMonitorMessage(data) {
  return request({
    url: '/fontMonitor/fontMonitorMessage',
    method: 'post',
    data: data
  })
}

// 修改用户信息
export function updateFontMonitorMessage(data) {
  return request({
    url: '/fontMonitor/fontMonitorMessage',
    method: 'put',
    data: data
  })
}

// 删除用户信息
export function delFontMonitorMessage(id) {
  return request({
    url: '/fontMonitor/fontMonitorMessage/' + id,
    method: 'delete'
  })
}
