import request from '@/utils/request'

// 查询监控渲染时间列表
export function listFontMonitorPaintTime(query) {
  return request({
    url: '/fontMonitor/fontMonitorPaintTime/list',
    method: 'get',
    params: query
  })
}

// 查询监控渲染时间详细
export function getFontMonitorPaintTime(id) {
  return request({
    url: '/fontMonitor/fontMonitorPaintTime/' + id,
    method: 'get'
  })
}

// 新增监控渲染时间
export function addFontMonitorPaintTime(data) {
  return request({
    url: '/fontMonitor/fontMonitorPaintTime',
    method: 'post',
    data: data
  })
}

// 修改监控渲染时间
export function updateFontMonitorPaintTime(data) {
  return request({
    url: '/fontMonitor/fontMonitorPaintTime',
    method: 'put',
    data: data
  })
}

// 删除监控渲染时间
export function delFontMonitorPaintTime(id) {
  return request({
    url: '/fontMonitor/fontMonitorPaintTime/' + id,
    method: 'delete'
  })
}
