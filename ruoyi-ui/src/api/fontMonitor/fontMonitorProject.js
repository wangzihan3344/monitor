import request from '@/utils/request'

// 查询团队负责的项目列表
export function listFontMonitorProject(query) {
  return request({
    url: '/fontMonitor/fontMonitorProject/list',
    method: 'get',
    params: query
  })
}

// 查询团队负责的项目详细
export function getFontMonitorProject(id) {
  return request({
    url: '/fontMonitor/fontMonitorProject/' + id,
    method: 'get'
  })
}

// 新增团队负责的项目
export function addFontMonitorProject(data) {
  return request({
    url: '/fontMonitor/fontMonitorProject',
    method: 'post',
    data: data
  })
}

// 修改团队负责的项目
export function updateFontMonitorProject(data) {
  return request({
    url: '/fontMonitor/fontMonitorProject',
    method: 'put',
    data: data
  })
}

// 删除团队负责的项目
export function delFontMonitorProject(id) {
  return request({
    url: '/fontMonitor/fontMonitorProject/' + id,
    method: 'delete'
  })
}
