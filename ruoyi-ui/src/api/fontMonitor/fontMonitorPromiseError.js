import request from '@/utils/request'

// 查询监控 promiseError列表
export function listFontMonitorPromiseError(query) {
  return request({
    url: '/fontMonitor/fontMonitorPromiseError/list',
    method: 'get',
    params: query
  })
}

// 查询监控 promiseError详细
export function getFontMonitorPromiseError(id) {
  return request({
    url: '/fontMonitor/fontMonitorPromiseError/' + id,
    method: 'get'
  })
}

// 新增监控 promiseError
export function addFontMonitorPromiseError(data) {
  return request({
    url: '/fontMonitor/fontMonitorPromiseError',
    method: 'post',
    data: data
  })
}

// 修改监控 promiseError
export function updateFontMonitorPromiseError(data) {
  return request({
    url: '/fontMonitor/fontMonitorPromiseError',
    method: 'put',
    data: data
  })
}

// 删除监控 promiseError
export function delFontMonitorPromiseError(id) {
  return request({
    url: '/fontMonitor/fontMonitorPromiseError/' + id,
    method: 'delete'
  })
}
