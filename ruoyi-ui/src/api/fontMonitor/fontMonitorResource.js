import request from '@/utils/request'

// 查询资源监听列表
export function listFontMonitorResource(query) {
  return request({
    url: '/fontMonitor/fontMonitorResource/list',
    method: 'get',
    params: query
  })
}

// 查询资源监听详细
export function getFontMonitorResource(id) {
  return request({
    url: '/fontMonitor/fontMonitorResource/' + id,
    method: 'get'
  })
}

// 新增资源监听
export function addFontMonitorResource(data) {
  return request({
    url: '/fontMonitor/fontMonitorResource',
    method: 'post',
    data: data
  })
}

// 修改资源监听
export function updateFontMonitorResource(data) {
  return request({
    url: '/fontMonitor/fontMonitorResource',
    method: 'put',
    data: data
  })
}

// 删除资源监听
export function delFontMonitorResource(id) {
  return request({
    url: '/fontMonitor/fontMonitorResource/' + id,
    method: 'delete'
  })
}
