import request from '@/utils/request'

// 查询监控 resourceError列表
export function listFontMonitorResourceError(query) {
  return request({
    url: '/fontMonitor/fontMonitorResourceError/list',
    method: 'get',
    params: query
  })
}

// 查询监控 resourceError详细
export function getFontMonitorResourceError(id) {
  return request({
    url: '/fontMonitor/fontMonitorResourceError/' + id,
    method: 'get'
  })
}

// 新增监控 resourceError
export function addFontMonitorResourceError(data) {
  return request({
    url: '/fontMonitor/fontMonitorResourceError',
    method: 'post',
    data: data
  })
}

// 修改监控 resourceError
export function updateFontMonitorResourceError(data) {
  return request({
    url: '/fontMonitor/fontMonitorResourceError',
    method: 'put',
    data: data
  })
}

// 删除监控 resourceError
export function delFontMonitorResourceError(id) {
  return request({
    url: '/fontMonitor/fontMonitorResourceError/' + id,
    method: 'delete'
  })
}
