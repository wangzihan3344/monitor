import request from '@/utils/request'

// 查询记录停留时间列表
export function listFontMonitorStayTime(query) {
  return request({
    url: '/fontMonitor/fontMonitorStayTime/list',
    method: 'get',
    params: query
  })
}

// 查询记录停留时间详细
export function getFontMonitorStayTime(id) {
  return request({
    url: '/fontMonitor/fontMonitorStayTime/' + id,
    method: 'get'
  })
}

// 新增记录停留时间
export function addFontMonitorStayTime(data) {
  return request({
    url: '/fontMonitor/fontMonitorStayTime',
    method: 'post',
    data: data
  })
}

// 修改记录停留时间
export function updateFontMonitorStayTime(data) {
  return request({
    url: '/fontMonitor/fontMonitorStayTime',
    method: 'put',
    data: data
  })
}

// 删除记录停留时间
export function delFontMonitorStayTime(id) {
  return request({
    url: '/fontMonitor/fontMonitorStayTime/' + id,
    method: 'delete'
  })
}
