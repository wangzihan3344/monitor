import request from '@/utils/request'

// 查询用户和团队的关系列表
export function listFontMonitorUserConnectTeam(query) {
  return request({
    url: '/fontMonitor/fontMonitorUserConnectTeam/list',
    method: 'get',
    params: query
  })
}

// 查询用户和团队的关系详细
export function getFontMonitorUserConnectTeam(id) {
  return request({
    url: '/fontMonitor/fontMonitorUserConnectTeam/' + id,
    method: 'get'
  })
}

// 新增用户和团队的关系
export function addFontMonitorUserConnectTeam(data) {
  return request({
    url: '/fontMonitor/fontMonitorUserConnectTeam',
    method: 'post',
    data: data
  })
}

// 修改用户和团队的关系
export function updateFontMonitorUserConnectTeam(data) {
  return request({
    url: '/fontMonitor/fontMonitorUserConnectTeam',
    method: 'put',
    data: data
  })
}

// 删除用户和团队的关系
export function delFontMonitorUserConnectTeam(id) {
  return request({
    url: '/fontMonitor/fontMonitorUserConnectTeam/' + id,
    method: 'delete'
  })
}
