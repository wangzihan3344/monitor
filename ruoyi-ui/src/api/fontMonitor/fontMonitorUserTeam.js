import request from '@/utils/request'

// 查询团队信息列表
export function listFontMonitorUserTeam(query) {
  return request({
    url: '/fontMonitor/fontMonitorUserTeam/list',
    method: 'get',
    params: query
  })
}

// 查询团队信息详细
export function getFontMonitorUserTeam(id) {
  return request({
    url: '/fontMonitor/fontMonitorUserTeam/' + id,
    method: 'get'
  })
}

// 新增团队信息
export function addFontMonitorUserTeam(data) {
  return request({
    url: '/fontMonitor/fontMonitorUserTeam',
    method: 'post',
    data: data
  })
}

// 修改团队信息
export function updateFontMonitorUserTeam(data) {
  return request({
    url: '/fontMonitor/fontMonitorUserTeam',
    method: 'put',
    data: data
  })
}

// 删除团队信息
export function delFontMonitorUserTeam(id) {
  return request({
    url: '/fontMonitor/fontMonitorUserTeam/' + id,
    method: 'delete'
  })
}
