import request from '@/utils/request'

// 查询记录访问信息列表
export function listFontMonitorVisitInfo(query) {
  return request({
    url: '/fontMonitor/fontMonitorVisitInfo/list',
    method: 'get',
    params: query
  })
}

// 查询记录访问信息详细
export function getFontMonitorVisitInfo(id) {
  return request({
    url: '/fontMonitor/fontMonitorVisitInfo/' + id,
    method: 'get'
  })
}

// 新增记录访问信息
export function addFontMonitorVisitInfo(data) {
  return request({
    url: '/fontMonitor/fontMonitorVisitInfo',
    method: 'post',
    data: data
  })
}

// 修改记录访问信息
export function updateFontMonitorVisitInfo(data) {
  return request({
    url: '/fontMonitor/fontMonitorVisitInfo',
    method: 'put',
    data: data
  })
}

// 删除记录访问信息
export function delFontMonitorVisitInfo(id) {
  return request({
    url: '/fontMonitor/fontMonitorVisitInfo/' + id,
    method: 'delete'
  })
}
