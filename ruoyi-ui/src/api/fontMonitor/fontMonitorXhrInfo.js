import request from '@/utils/request'

// 查询监控 xhr 接口列表
export function listFontMonitorXhrInfo(query) {
  return request({
    url: '/fontMonitor/fontMonitorXhrInfo/list',
    method: 'get',
    params: query
  })
}

// 查询监控 xhr 接口详细
export function getFontMonitorXhrInfo(id) {
  return request({
    url: '/fontMonitor/fontMonitorXhrInfo/' + id,
    method: 'get'
  })
}

// 新增监控 xhr 接口
export function addFontMonitorXhrInfo(data) {
  return request({
    url: '/fontMonitor/fontMonitorXhrInfo',
    method: 'post',
    data: data
  })
}

// 修改监控 xhr 接口
export function updateFontMonitorXhrInfo(data) {
  return request({
    url: '/fontMonitor/fontMonitorXhrInfo',
    method: 'put',
    data: data
  })
}

// 删除监控 xhr 接口
export function delFontMonitorXhrInfo(id) {
  return request({
    url: '/fontMonitor/fontMonitorXhrInfo/' + id,
    method: 'delete'
  })
}
