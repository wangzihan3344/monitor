## public

```json
{
    "pid":"aewfliaflbaw;rg",//项目id
    "uuid":"aefkafbkabflagbwfaufwg",//用户id
    "title": "前端监控系统", //网页标题
    "url": "http://localhost:8080/",//网页地址
    "timestamp": "1590815288710",//时间戳
    "browser": "Chrome",//访问浏览器
    "OS": "Windows",//访问系统
    "type":"jsError",//上报类型
    "production":"dev",//项目环境：'dev'：生产环境；'sit'：测试环境；'stag'：预发布环境；'prod'：生产环境
}
```



## jsError

```json
{
    '...':'...',//公共字段
    "type": "jsError", 
    "message": "Uncaught TypeError: Cannot set property 'error' of undefined", 
    "filename": "http://localhost:8080/", 
    "position": "0:0",
    "stack": "btnClick (http://localhost:8080/:20:39)^HTMLInputElement.onclick (http://localhost:8080/:14:72)",
    "selector": "HTML BODY #container .content INPUT"
}
```

## promiseError

```json
{
    '...':'...',//公共字段
    'type':'promiseError',
    "message": "someVar is not defined",//报错信息
    "stack": "http://localhost:8080/:24:29^new Promise (<anonymous>)^btnPromiseClick (http://localhost:8080/:23:13)^HTMLInputElement.onclick (http://localhost:8080/:15:86)",//堆栈信息
    "selector": "HTML BODY #container .content INPUT"//最后操作元素
}
```

## resourceError

```json
{
    '...':'...',//公共字段
    "type": "resourceError",
    "filename": "http://localhost:8080/error.js",
    "tagName": "SCRIPT",
    "triggerTimestamp": "76",
    "selector": "HTML BODY #container .content img"
}
```

## resource

```json
{
    '...':'...',//公共字段
    "type": "resource",
    "name": "http://localhost:8080/error.js",//资源名称(地址)
    "duration": "314134",//请求耗时
    "encoded_body_size":"199",//资源大小
    "parse_DNS_time":"123",//域名解析耗时
    "connectTime": "0",//链接耗时
  	"ttfbTime": "1",//首字节到达耗时
  	"responseTime": "1",//响应耗时
}
```

## xhr

```json
{
  '...':'...',//公共字段
  "type": "xhr",
  "eventType": "load", //事件类型
  "pathname": "/success", //请求路径
  "status": "200-OK",//响应状态
  "duration": "7",//请求耗时
  "response": "{\"id\":1}",//响应数据
  "params": "" //请求参数
}
```
## fetch

```js
{
  '...':'...',//公共字段
  "type": "fetch",
  "pathname": "/success", //请求路径
  "status": "200-OK",//响应状态
  "duration": "7",//请求耗时
  "response": "{\"id\":1}",//响应数据
  "params": "" //请求参数
}
```

## 白屏监控

```js
{
  '...':'...',//公共字段
  "emptyPoints": "0", //白屏点数
  "screen": "2049x1152", //显示器尺寸
  "viewPoint": "2048x994", //浏览器显示尺寸
  "selector": "HTML BODY #container" //选择元素
}
```

## 加载时间

```js
{
  '...':'...',//公共字段
  "type": "timing",
  "parseDNSTime"："23",//域名解析时间
  "connectTime": "0",//链接耗时
  "ttfbTime": "1",//请求开始到首字节到达耗时
  "responseTime": "1",//响应耗时
  "parseDOMTime": "80",//dom解析耗时
  "domContentLoadedTime": "0",//dom文档解析完成到所有的资源都加载完毕耗时
  "timeToInteractive": "88",//TTI（从页面加载开始到页面处于完全可交互状态所花费的时间）
  "loadTime": "89"//完整的加载时间
}
```

## 渲染

```json
{
  '...':'...',//公共字段
  "type": "paint",
  "firstPaint": "102",//FP
  "firstContentPaint": "2130",//FCP
  "firstMeaningfulPaint": "2130",//FMP
  "largestContentfulPaint": "2130"//LCP
}
```

## 首次输入响应延迟

```json
{
  '...':'...',//公共字段
  "type": "firstInputDelay",
  "inputDelay": "3",//输入延迟时间
  "duration": "8",//处理耗时
  "startTime": "344999983907",//处理开始时间戳
  "selector": "HTML BODY #container .content H1"//事件目标元素
}
```

## 卡顿

```js
{
  '...':'...',//公共字段
  "type": "longTask",
  "eventType": "mouseover",//事件类型
  "startTime": "9331",//延迟开始时间
  "duration": "200",//延迟持续时间
  "selector": "HTML BODY #container .content"//事件目标元素
}
```

## pv-uv

```js
{
    '...':'...',//公共字段
    "type": "pv",
    "effectiveType": "4g",//网络类型
    "rtt": "200",//网络延迟
    "screen": "1920x1080"//设备分辨率
}
```

## 停留时间

```js
{
  '...':'...',//公共字段
  "type": "stayTime",
  "stayTime":"000"//页面停留时间
}
```

## 路由监听（hash模式）

```js
{
  '...':'...',//公共字段
  "type": "hash",
  "oldUrl"："http://127.0.0.1:8080#/index"
  "newUrl"："http://127.0.0.1:8080#/settings"
}
```

## 路由监听（history模式）

```js
{
  '...':'...',//公共字段
  "type": "history"
  "current": "/index",
  "back": null,
  "forward": "/settings",
}
```

